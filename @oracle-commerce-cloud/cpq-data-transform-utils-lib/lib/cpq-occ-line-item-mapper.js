// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.

'use strict';

/**
 * This module provides functions to map CPQ attributes to OCC cart item properties.
 */
class CpqOccLineItemMapper {

  /**
   * Map from a CPQ line item to an OCC line item.
   *
   * @param {Object} cpqItem
   *   The CPQ line item to be mapped to an OCC line item.
   *
   * @returns {Object}
   *   The mapped OCC line item.
   */
  static mapCpqToOcc (cpqItem) {
    const attributes = cpqItem.attributes || [];
    let childItems = cpqItem.childItems || [];
    const rootBomItem = cpqItem.bomItems && cpqItem.bomItems[0] ? cpqItem.bomItems[0] : {};
    const fields = cpqItem.fields || rootBomItem.fields || {};
    let children = cpqItem.children || rootBomItem.children || [];
    const recurringCharge = cpqItem.recurringCharge || rootBomItem.recurringCharge || {};

    // Map price.
    const price = fields._price_unit_price_each || cpqItem.price;

    // Map actionCode
    const actionCode = fields.oRCL_ABO_ActionCode_l || cpqItem.actionCode;

    // Map attributes.
    let externalData = [];

    if (Array.isArray(attributes)) {
      externalData = attributes.map(this._mapCpqAttributeToOcc);
    }

    // Map child item properties.
    childItems = childItems.map(function (child) {
      return module.exports.mapCpqToOcc(child);
    });
    children = children.map(function (child) {
      return module.exports.mapCpqToOcc(child);
    });

    // Merge recommended items with BOM items.
    childItems = childItems.concat(children);

    // Get the service id of current item
    const serviceId = fields.itemInstanceName_l || cpqItem.serviceId;

    // Get the asset key of current item.
    const assetKey = fields.itemInstanceId_l || cpqItem.assetKey;

    // Determine whether the item is an asset or not.
    const isAsset = cpqItem.assetId ? true : false;

    // Map current item properties.
    return {
      // Add additional payload properties here.
      catalogRefId: cpqItem.catalogRefId,
      quantity: cpqItem.quantity,
      amount: cpqItem.amount,
      externalPrice: price,
      externalData,
      actionCode,
      childItems,
      messageType: cpqItem.messageType,
      configuratorId: cpqItem.configurationId,
      externalRecurringCharge: recurringCharge.amount,
      externalRecurringChargeFrequency: recurringCharge.frequency,
      externalRecurringChargeDuration: recurringCharge.duration,
      assetId: cpqItem.assetId,
      assetKey,
      serviceId,
      customerAccountId: cpqItem.customerAccountId,
      billingAccountId: cpqItem.billingAccountId,
      billingProfileId: cpqItem.billingProfileId,
      serviceAccountId: cpqItem.serviceAccountId,
      activationDate: cpqItem.activationDate,
      deactivationDate: cpqItem.deactivationDate,
      asset: isAsset
    };
  }

  /**
   * Map from a CPQ line item attributes to an OCC line item attributes (i.e. externalData).
   *
   * @private
   *
   * @param {Object} cpqAttributes
   *   The CPQ line item attributes.
   *
   * @returns {Object}
   *   The (mapped) OCC line item attributes (externalData).
   */
  static _mapCpqAttributeToOcc (cpqAttributes) {
    return {
      name: cpqAttributes.variableName,
      values: Object.assign(cpqAttributes, {
        name: cpqAttributes.variableName,
        label: cpqAttributes.variableName,
        displayValue: cpqAttributes.value
      })
    };
  }

}

module.exports = CpqOccLineItemMapper;
