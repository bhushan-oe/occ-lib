## Introduction
This is a server-side extension library that provides utilities to map data 
from CPQ to data the can be consumed by OCC.

## Prerequisites
* Oracle Commerce Cloud
* Oracle CPQ

## Install
This library should be copied to the node_modules directory of a server side extension.

# Usage
The cpq-occ-line-item-mapper module provides a static function 'mapCpqToOcc' that accepts
a CPQ line item (which itself may include child line items) and returns a corresponding 
OCC line item.
