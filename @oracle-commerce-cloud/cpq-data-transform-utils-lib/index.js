// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.

'use strict';

const CpqOccLineItemMapper = require('./lib/cpq-occ-line-item-mapper');

module.exports = {
  CpqOccLineItemMapper
};
