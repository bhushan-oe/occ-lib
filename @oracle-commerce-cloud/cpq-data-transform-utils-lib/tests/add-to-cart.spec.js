// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.
/* eslint-disable */

const sinon = require('sinon');
const CpqOccLineItemMapper = require('../lib/cpq-occ-line-item-mapper');


// Mock data response files.
const cpqAddToCartData = require('./data/cpq-add-to-cart-data.json');
const occAddToCartData = require('./data/occ-add-to-cart-data.json');

// Using a sinon sandbox allows stubs etc. to be restored after each test.
let sandbox;


//------------------------------------------------------------------------------------
/**
 * Tests for the configure.
 */
describe('add to cart handler', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();
  });

  afterEach(function() {
    sandbox.restore();
  });

  //--------------------------------------------------
  it('when CPQ add to cart data is passed to mapper, a corresponding OCC payload is returned.', async () => {

    // Remove \n\t characters from integrationPayload so that JSON response is valid.
    const validIntegrationPayload = cpqAddToCartData.integrationPayload.replace('\n\t', '');

    // Map the CPQ attributes to the OCC cart format.
    let result = CpqOccLineItemMapper.mapCpqToOcc(JSON.parse(validIntegrationPayload));

    expect(JSON.stringify(result, null, 2)).toEqual(
      JSON.stringify(occAddToCartData.integrationPayload, null, 2)
    );
  });
});
