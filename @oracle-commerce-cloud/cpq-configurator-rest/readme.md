# cpq-configurator-rest
> A server side extension for extending Oracle Commerce Cloud.

## Introduction
cpq-configurator-rest

## Prerequisites
* Oracle Commerce Cloud (Minimum version: 19B)

## Install
1. Run `npm install` in the cpq-configurator-rest directory.
2. Zip the extension, the package.json file should be at the top level of the zip file.

The zip should be deployed to extension server as a server side extension. Please refer the "Extending Oracle Commerce Cloud" documentation for more details on server side extensions.

## Extending cpq-configurator-rest
Classes exported from `cpq-configurator-rest` can be overwritten in an application layer to provide the behavior required. `cpq-configurator-rest` should not be  modified directly.