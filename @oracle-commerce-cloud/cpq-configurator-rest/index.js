// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.

'use strict';

const ConfiguratorRestHandler = require('./lib/configurator-rest-handler');

module.exports = {
  ConfiguratorRestHandler
};
