const constants = require('./constants');
const url = require('url');

/**
 * A Requestor component used to send requests to an external system.
 *
 * Handles the application/json media type only.
 *
 * @class Requestor
 */
class Requestor {

  /**
   * Creates an instance of Requestor.
   *
   * @param {Object} configuration
   * @param {string} configuration.useHTTPS Use https protocol for outbound requests.
   * @param {string} configuration.hostname Outbound CRM server hostname.
   * @param {string} configuration.port Outbound CRM server port.
   * @param {string} configuration.timeout Request timeout in ms.
   * @param {string} configuration.username Environment variable name for basic auth username.
   * @param {string} configuration.password Environment variable name for basic auth password.
   * @param {Object} configuration.logger Logger object.
   * @memberof Requestor
   */
  constructor(configuration) {
    if (!configuration) {
      throw new Error ('The configuration object is null.');
    }

    if(configuration['useHTTPS'] === false) {
      this.http = require('http');
      this.externalUrl = url.parse('http://' + configuration['hostname'] + ':' + configuration['port'], true, true);
    }
    else {
      this.http = require('https');
      this.externalUrl = url.parse('https://' + configuration['hostname'] + ':'
        + configuration['port'], true, true);
    }

    this.logger = configuration.logger;

    this.username = configuration.username;

    this.password = configuration.password;

    if(configuration.timeout) {
      this.timeout = configuration.timeout;
    }
    else{
      this.timeout = 5000;
    }
  }

  /**
   * Handles get requests. Invokes a callback with the response from the
   * remote server.
   *
   * @param {string} uri URI to request to.
   * @param {string} query Query string to send in the request.
   * @param {function(Object, string)} callback callback that should be invoked when this function completes.
   * @memberof Requestor
   */
  handleGET(uri, queryString, callback) {
    const self = this;
    const uriWithQuery = uri + queryString;
    const options = self.getHttpRequestOptions(constants.HTTP_METHOD_GET, uriWithQuery, null);

    self.logger.debug('Sending GET request ' + JSON.stringify(options, null, 2));

    const req = self.http.request(options, function (res) {
      let body = '';
      res.setEncoding('utf8');

      res.on('data', function (chunk) {
        body += chunk;
      });

      res.on('end', function(){
        // Only try and parse the body if we have one.
        if(body) {
          let responseData = {};

          try {
            responseData = JSON.parse(body);
          }
          catch(error) {
            self.logger.error(error);
            self.logger.error(body);
            callback(unparsableResponse(), 500);
            return;
          }

          callback(null, res.statusCode, responseData);
        }
        else {
          if (res.statusCode >= 200 && res.statusCode < 300) {
            self.logger.warning(res.statusCode + ': empty response returned from request to \n' +
              JSON.stringify(options, null, 2));
          }
          else {
            self.logger.error(res.statusCode + ': ' + res.statusMessage + ' - returned from request to \n' +
              JSON.stringify(options, null, 2));
          }
          callback(null, res.statusCode);
        }
      });
    });

    req.on('error', function (error) {
      self.logger.error(error);
      callback(errorResponse(), 500);
    });

    req.on('timeout', function () {
      callback(timeoutResponse(), 408);
    });

    req.setTimeout(self.timeout);
    req.end();
  }

  /**
   * Handles post requests. Invokes a callback with the response from the
   * remote server.
   *
   * @param {string} uri URI to request to.
   * @param {string} payload Payload to send in the request.
   * @param {function(Object, string)} callback callback that should be invoked when this function completes.
   * @memberof Requestor
   */
  handlePOST(uri, payload, callback) {
    const self = this;
    const postData = JSON.stringify(payload);
    const options = self.getHttpRequestOptions(constants.HTTP_METHOD_POST, uri, postData);

    console.log('Sending POST request ' + JSON.stringify(options, null, 2) + '\n'
      + 'POST Body:\n ' + JSON.stringify(payload, null, 2));

    const req = self.http.request(options, function (res) {
      let body = '';
      res.setEncoding('utf8');

      res.on('data', function (chunk) {
        body += chunk;
      });

      res.on('end', function () {
        // Only try and parse the body if we have one.
        if(body) {
          let responseData = {};

          try {
            responseData = JSON.parse(body);
          }
          catch(error) {
            self.logger.error(error);
            self.logger.error(body);
            callback(unparsableResponse(), 500);
            return;
          }

          callback(null, res.statusCode, responseData);
        }
        else {
          if (res.statusCode >= 200 && res.statusCode < 300) {
            self.logger.warning(res.statusCode + ': empty response returned from request to \n' +
              JSON.stringify(options, null, 2));
          }
          else {
            self.logger.error(res.statusCode + ': ' + res.statusMessage + ' - returned from request to \n' +
              JSON.stringify(options, null, 2));
          }
          callback(null, res.statusCode);
        }
      });
    });

    req.on('error', function (e) {
      self.logger.error('Error ' + e);
      callback(errorResponse(), 500);
    });

    req.on('timeout', function () {
      req.abort();
      callback(timeoutResponse(), 408);
    });

    req.setTimeout(self.timeout);
    req.write(postData);
    req.end();
  }

  /**
   * Handles put requests. Invokes a callback with the response from the
   * remote server.
   *
   * @param {string} uri URI to request to.
   * @param {string} payload Payload to send in the request.
   * @param {function(Object, string)} callback callback that should be invoked when this function completes.
   * @memberof Requestor
   */
  handlePUT(uri, payload, callback) {
    const self = this;
    const putData = JSON.stringify(payload);
    const options = self.getHttpRequestOptions(constants.HTTP_METHOD_PUT, uri, putData);

    self.logger.debug('Sending PUT request ' + JSON.stringify(options, null, 2) + '\n'
      + 'PUT Body:\n ' + JSON.stringify(payload, null, 2));

    const req = self.http.request(options, function (res) {
      let body = '';
      res.setEncoding('utf8');

      res.on('data', function (chunk) {
        body += chunk;
      });

      res.on('end', function () {
        // Only try and parse the body if we have one.
        if(body) {
          let responseData = {};

          try {
            responseData = JSON.parse(body);
          }
          catch(error) {
            self.logger.error(error);
            self.logger.error(body);
            callback(unparsableResponse(), 500);
            return;
          }

          callback(null, res.statusCode, responseData);
        }
        else {
          if (res.statusCode >= 200 && res.statusCode < 300) {
            self.logger.warning(res.statusCode + ': empty response returned from request to \n' +
              JSON.stringify(options, null, 2));
          }
          else {
            self.logger.error(res.statusCode + ': ' + res.statusMessage + ' - returned from request to \n' +
              JSON.stringify(options, null, 2));
          }
          callback(null, res.statusCode);
        }
      });
    });

    req.on('error', function (e) {
      self.logger.error('Error ' + e);
      callback(errorResponse(), 500);
    });

    req.on('timeout', function () {
      req.abort();
      callback(timeoutResponse(), 408);
    });

    req.setTimeout(self.timeout);
    req.write(putData);
    req.end();
  }

  /**
   * Generates request headers that can be used to send a request to an external
   * system.
   *
   * @param httpMethod The request method, e.g GET, POST etc...
   * @param uri Request path, e.g /v1/test
   * @param data Data that will be sent in the request.
   * @returns Http request options object.
   * @memberof Requestor
   */
  getHttpRequestOptions(httpMethod, uri, data) {
    let headerValues = {'Accept': constants.APPLICATION_JSON};

    if(constants.HTTP_METHOD_GET != httpMethod) {
      headerValues[constants.CONTENT_TYPE] = constants.APPLICATION_JSON;
    }

    let user = process.env[this.username];
    let pass = process.env[this.password];

    if(user != null && pass != null) {
      headerValues[constants.AUTHORIZATION] =
        'Basic ' + Buffer.from(user + ':' + pass).toString('base64');
    }

    if(data != null) {
      headerValues[constants.CONTENT_LENGTH] = Buffer.byteLength(data)
    }

    const httpReqOptions = {
      hostname: this.externalUrl.hostname,
      path: uri,
      port: this.externalUrl.port == undefined ? 443 : this.externalUrl.port,
      method: httpMethod,
      headers: headerValues
    };

    console.log(JSON.stringify(httpReqOptions, null, 2));

    return httpReqOptions;
  }

}

/**
 * @returns An error response.
 */
function errorResponse() {
  return new Error('The request to the server resulted in an error.');
}

/**
 * @returns A timeout response.
 */
function timeoutResponse() {
  return new Error('Request to the server timed out.');
}

/**
 * @returns An error response.
 */
function unparsableResponse() {
  return new Error('Could not parse response from server.');
}

module.exports = Requestor;
