module.exports = {
  // HTTP Methods
  HTTP_METHOD_GET: 'GET',
  HTTP_METHOD_POST: 'POST',
  HTTP_METHOD_PUT: 'PUT',
  // HTTP Header fields
  APPLICATION_JSON:'application/json',
  CONTENT_LENGTH: 'Content-Length',
  CONTENT_TYPE: 'Content-Type',
  ACCEPT: 'Accept',
  AUTHORIZATION:'authorization'
};