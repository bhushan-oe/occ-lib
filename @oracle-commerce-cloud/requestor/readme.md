# requestor
> Utility module to send http(s) requests.

## Introduction
Provides a set of utility functions to send http(s) requests.

## Prerequisites
The prerequisites required are
* Oracle Commerce Cloud (Minimum version: 18C)

## Install
The library should be copied to the node_modules directory of the server side extension.

## Usage
```javascript
const Requestor = require('@oracle-commerce-cloud/requestor');
let requestor = new Requestor(config);
requestor.handlePOST(uri, payload, function(err, code, data) {
  ...
}
```
