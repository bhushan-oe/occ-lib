const Requestor = require('../lib/Requestor');
const sinon = require('sinon');
const http = require('http');
const PassThrough = require('stream').PassThrough;

var logger = {
  debug: function(msg) {
    console.log(msg);
  }
}

var requestorInst = new Requestor({
  'useHTTPS': false,
  'hostname': 'localhost',
  'port': '8080',
  'timeout': 5000,
  'logger': logger
});

var expected = {
  "contacts": {
    "C1": {
      "id": "C1",
      "name": "Sally Admin",
      "email": 'kim@example.com',
      "accounts": {
         "AC01": ["admin", "buyer", "user"]
      },
     "primaryCustomerAccount": "AC01"
    }
  },
  "accounts": {
    "AC01": {
      "id": "AC01",
      "type": "customer",
      "addresses": ["AD01"],
      "defaultShippingAddress": "AD01",
      "defaultBillingAddress": "AD01",
      "billingProfiles": ["BP01", "BP02"],
      "parentAccount": null,
      "childAccounts": []
    }
  },
  "billingProfiles": {
    "BP01": {
      "id": "BP01",
      "address": "AD01"
    }
  },
  "addresses": {
    "AD01": {
      "id": "AD01",
      "line1": "Oak St",
      "city": "Oaksville",
      "zip": "12345",
      "country": "USA"
    }
  }
};

describe('requestor', function () {

  beforeEach(function() {
    this.request = sinon.stub(http, 'request');
  });
 
  afterEach(function() {
    http.request.restore();
  });
   
   /**
    * Test the interface.
    */
   it('has expected interface', function() {
     expect(requestorInst.handleGET).toBeDefined();
     expect(requestorInst.handlePOST).toBeDefined();
     expect(requestorInst.handlePUT).toBeDefined();
   });

   /**
    * Test the handleGET function.
    */
   it('handle get account', function(done) {
     var response = new PassThrough();
     response.statusCode = 200;
     response.write(JSON.stringify(expected));
     response.end();
   
     var request = new PassThrough();
     request.setTimeout = function() {};
     request.end = function() {};

     this.request.callsArgWith(1, response).returns(request);
     
     requestorInst.handleGET('/v1/test', {}, function(err, code, responseData) {
       // Need to check we get a valid response then done
       expect(responseData.contacts).toBeDefined();
       expect(responseData.contacts.C1).toBeDefined();
       expect(responseData.contacts.C1.email).toBe('kim@example.com');

       done();
     });
     
   });

   /**
    * Test the handlePOST function.
    */
   it('handle post account', function(done) {
    var response = new PassThrough();
    response.statusCode = 200;
    response.write(JSON.stringify(expected));
    response.end();
  
    var request = new PassThrough();
    request.setTimeout = function() {};
    request.end = function() {};

    this.request.callsArgWith(1, response).returns(request);
    
    requestorInst.handlePOST('/v1/test', {}, function(err, code, responseData) {
      // Need to check we get a valid response then done
      expect(responseData.contacts).toBeDefined();
      expect(responseData.contacts.C1).toBeDefined();
      expect(responseData.contacts.C1.email).toBe('kim@example.com');

      done();
    });
    
  });

  /**
   * Test the handlePOST function.
   */
  it('handle put account', function(done) {
    var response = new PassThrough();
    response.statusCode = 200;
    response.write(JSON.stringify(expected));
    response.end();
  
    var request = new PassThrough();
    request.setTimeout = function() {};
    request.end = function() {};

    this.request.callsArgWith(1, response).returns(request);
    
    requestorInst.handlePUT('/v1/test', {}, function(err, code, responseData) {
      // Need to check we get a valid response then done
      expect(responseData.contacts).toBeDefined();
      expect(responseData.contacts.C1).toBeDefined();
      expect(responseData.contacts.C1.email).toBe('kim@example.com');

      done();
    });
    
  });

  it('builds http options - PUT', function() {
    var options = requestorInst.getHttpRequestOptions('GET', '/test', null);

    expect(options.port).toBe('8080');
    expect(options.path).toBe('/test');
    expect(options.method).toBe('GET');
    expect(options.headers['Content-Type']).toBe(undefined);
    expect(options.headers['Accept']).toBe('application/json');
  });

  it('builds http options - PUT', function() {
    var options = requestorInst.getHttpRequestOptions('PUT', '/test', null);

    expect(options.port).toBe('8080');
    expect(options.path).toBe('/test');
    expect(options.method).toBe('PUT');
    expect(options.headers['Content-Type']).toBe('application/json');
    expect(options.headers['Accept']).toBe('application/json');
  });

  it('builds http options - POST', function() {
    var options = requestorInst.getHttpRequestOptions('POST', '/test', null);

    expect(options.port).toBe('8080');
    expect(options.path).toBe('/test');
    expect(options.method).toBe('POST');
    expect(options.headers['Content-Type']).toBe('application/json');
    expect(options.headers['Accept']).toBe('application/json');
  });

});