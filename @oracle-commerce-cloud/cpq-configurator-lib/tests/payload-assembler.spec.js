// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.
/* eslint-disable */

const PassThrough = require('stream').PassThrough;
const sinon = require('sinon');

const PayloadAssembler = require('../lib/payload-assembler');
let payloadAssembler;

const constants = require('../lib/constants');
const utils = require('../lib/utils');

const registry = require('../registry/registry');

const config = {
  "useHTTPS": true,
  "hostname": "cpq-server",
  "port": "",
  "useHTTPSforPreview": true,
  "previewHostname": "",
  "previewPort": "",
  "timeout": 60000,
  "username_env_var": "CPQ_USERNAME",
  "password_env_var": "CPQ_PASSWORD",
  "CONFIG_BASE_URI": "/rest/v8/config",
  "LAYOUT_BASE_URI": "/rest/v8/layoutCache",
  "PAGE_TEMPLATES_BASE_URI": "/rest/v8/pageTemplates",
  "CONFIG_UI_SETTINGS_BASE_URI": "/rest/v8/configUISettings",
  "CONFIGURATOR_SSE_BASE_CONTEXT_URI": "/v1/configurations",
  "registry": registry
}

let request;

//------------------------------------------------------------------------------------
/**
 * Tests for the next.
 */
describe('assemble payload', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();

    payloadAssembler = new PayloadAssembler(config);

    request = new PassThrough();
    response = new PassThrough();

    request.params = {};
    request.headers = {};
    request.body = {};
    request.originalUrl = '/ccstorex/custom/v1/configurations';
  });

  afterEach(function() {
    sandbox.restore();
    request = null;
  });

  // =================================================
  // Configure payload
  // =================================================

  //--------------------------------------------------
  /**
   * Valid configure payload.
   */
  it('when configure action is passed, configure payload is returned', () => {
    let payload = payloadAssembler.assemblePayloadForAction(request, constants.CONFIGURE_ACTION);

    expect(payload.cacheInstanceId).toBe('-1');
    expect(payload.fromPartner).toBe(true);
    expect(payload.legacyMode).toBe(true);
    expect(payload.fromPunchin).toBe(true);
    expect(payload.criteria.state).toBe(true);
  });

  //--------------------------------------------------
  /**
   * Valid configure payload.
   */
  it('when configure action is passed with additional childDefs, configure payload is returned with that childDefs', () => {
    request.body.criteria = {
      childDefs: [{ name: 'myNewChildDef' }]
    };

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.CONFIGURE_ACTION);

    expect(payload.cacheInstanceId).toBe('-1');
    expect(payload.fromPartner).toBe(true);
    expect(payload.legacyMode).toBe(true);
    expect(payload.fromPunchin).toBe(true);
    expect(payload.criteria.state).toBe(true);

    let newChildDef = payload.criteria.childDefs.find(item => {
      return item.name === 'myNewChildDef';
    });

    expect(newChildDef.name).toBe('myNewChildDef');
  });

  //--------------------------------------------------
  /**
   * Valid configure payload.
   */
  it('when configure action is passed with modelPunchin, configure payload is returned with that modelPunchin', () => {
    request.body.modelPunchin = {
      "flowInputs": {
      },
      "parameters": {
        "_variable_name_punchin": "true",
        "processor": "sku40004"
      }
    };

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.CONFIGURE_ACTION);

    expect(payload.cacheInstanceId).toBe('-1');
    expect(payload.fromPartner).toBe(true);
    expect(payload.legacyMode).toBe(true);
    expect(payload.fromPunchin).toBe(true);
    expect(payload.modelPunchin).toEqual(request.body.modelPunchin);
  });

  //--------------------------------------------------
  /**
   * Valid configure payload.
   */
  it('when configure action is passed with locale, configure payload is returned with a modelPunchin', () => {
    request.body = {
      'locale': 'en_GB',
    };

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.CONFIGURE_ACTION);

    expect(payload.cacheInstanceId).toBe('-1');
    expect(payload.fromPartner).toBe(true);
    expect(payload.legacyMode).toBe(true);
    expect(payload.fromPunchin).toBe(true);
    expect(payload.modelPunchin.parameters._bm_session_locale).toBe(request.body.locale);
  });

  //--------------------------------------------------
  /**
   * Valid configure payload.
   */
  it('when configure action is passed with locale and currency, configure payload is returned with a modelPunchin', () => {
    request.body = {
      'locale': 'en_GB',
      'currency': 'USD'
    };

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.CONFIGURE_ACTION);

    expect(payload.cacheInstanceId).toBe('-1');
    expect(payload.fromPartner).toBe(true);
    expect(payload.legacyMode).toBe(true);
    expect(payload.fromPunchin).toBe(true);
    expect(payload.modelPunchin.parameters._bm_session_locale).toBe(request.body.locale);
    expect(payload.modelPunchin.parameters._bm_session_currency).toBe(request.body.currency);
  });

  //--------------------------------------------------
  /**
   * Valid configure payload.
   */
  it('when configure action is passed configurationMetadata, configure payload is returned with a modelPunchin', () => {
    request.body = {
      'locale': 'en_GB',
      'currency': 'USD',
      'configurationMetadata': {
        'processor': 'sku40004',
        'antivirus': true
      }
    };

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.CONFIGURE_ACTION);

    expect(payload.cacheInstanceId).toBe('-1');
    expect(payload.fromPartner).toBe(true);
    expect(payload.legacyMode).toBe(true);
    expect(payload.fromPunchin).toBe(true);
    expect(payload.modelPunchin.parameters.processor).toBe("sku40004");
    expect(payload.modelPunchin.parameters.antivirus).toBe(true);
    expect(payload.modelPunchin.parameters._variable_name_punchin).toBe("true");
    expect(payload.modelPunchin.parameters._bm_session_locale).toBe(request.body.locale);
    expect(payload.modelPunchin.parameters._bm_session_currency).toBe(request.body.currency);
  });

  // =================================================
  // Reconfigure payload
  // =================================================

  //--------------------------------------------------
  /**
   * Valid reconfigure payload.
   */
  it('when reconfigure action is passed, reconfigure payload is returned', () => {
    request.body.configId = 12345;

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.RECONFIGURE_ACTION);

    expect(payload.configId).toBe(12345);
    expect(payload.criteria.state).toBe(true);
  });

  //--------------------------------------------------
  /**
   * Valid reconfigure payload.
   */
  it('when reconfigure action is passed with additional childDefs, reconfigure payload is returned with that childDefs', () => {
    request.body.criteria = {
      childDefs: [{ name: 'myNewChildDef' }]
    };

    request.body.configId = 12345;

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.RECONFIGURE_ACTION);

    expect(payload.configId).toBe(12345);
    expect(payload.criteria.state).toBe(true);

    let newChildDef = payload.criteria.childDefs.find(item => {
      return item.name === 'myNewChildDef';
    });

    expect(newChildDef.name).toBe('myNewChildDef');
  });

  // =================================================
  // Update payload
  // =================================================

  //--------------------------------------------------
  /**
   * Valid update payload.
   */
  it('when update action is passed, default update payload is returned', () => {
    request.params.cacheInstanceId = '1234567890';

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.UPDATE_ACTION);

    expect(payload.cacheInstanceId).toBe('1234567890');
    expect(payload.criteria.state).toBe(true);
    expect(payload.delta).toBe(false);
    expect(payload.criteria.childDefs.length).toBe(6);
  });

  //--------------------------------------------------
  /**
   * Valid update payload.
   */
  it('when update action is passed with delta, delta is represented in payload', () => {
    request.body.delta = true;

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.UPDATE_ACTION);

    expect(payload.delta).toBe(true);
  });

  //--------------------------------------------------
  /**
   * Valid update payload.
   */
  it('when update action is passed with extra childDef, extra childDef is represented in payload', () => {
    request.body.criteria = {
      childDefs: [{ name: 'myNewChildDef' }]
    };

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.UPDATE_ACTION);

    let newChildDef = payload.criteria.childDefs.find(item => {
      return item.name === 'myNewChildDef';
    });

    expect(newChildDef.name).toBe('myNewChildDef');
  });

  //--------------------------------------------------
  /**
   * Valid update payload.
   */
  it('when update action is passed with configData, configData is represented in payload', () => {
    request.body.configData = {
      attribute123: 'test value',
      _state: {
        attributes: {
          attribute123: {
            updatable: true
          }
        }
      }
    };

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.UPDATE_ACTION);
    expect(payload.configData.attribute123).toBe('test value');
  });

  //--------------------------------------------------
  /**
   * Valid update payload.
   */
  it('when update action is passed with configData and attribute is not updatable, attribute is not in payload', () => {
    request.body.configData = {
      attribute123: 'test value',
      _state: {
        attributes: {
          attribute123: {
            updatable: false
          }
        }
      }
    };

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.UPDATE_ACTION);
    expect(payload.configData.hasOwnProperty('attribute123')).toBe(false);
  });

  // =================================================
  // Interact payload
  // =================================================

  //--------------------------------------------------
  /**
   * Valid interact payload.
   */
  it('when interact action is passed, default interact payload is returned', () => {
    request.params.cacheInstanceId = '1234567890';

    request.body.configData = {
      attribute123: 'test value',
      _state: {
        attributes: {
          attribute123: {
            updatable: true
          }
        }
      }
    };

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.INTERACT_ACTION);

    expect(payload.cacheInstanceId).toBe('1234567890');
    expect(payload.criteria.state).toBe(true);
    expect(payload.delta).toBe(true);
    expect(payload.criteria.childDefs.length).toBe(1);
    expect(payload.configData).toBe(request.body.configData);
  });

  //--------------------------------------------------
  /**
   * Valid interact payload.
   */
  it('when interact action is passed with extra childDef, extra childDef is represented in payload', () => {
    request.body.criteria = {
      childDefs: [{ name: 'myNewChildDef' }]
    };

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.INTERACT_ACTION);

    let newChildDef = payload.criteria.childDefs.find(item => {
      return item.name === 'myNewChildDef';
    });

    expect(newChildDef.name).toBe('myNewChildDef');
  });

  //--------------------------------------------------
  /**
   * Valid interact payload.
   */
  it('when interact action is passed with configData, configData is represented in payload', () => {
    request.body.configData = {
      attribute123: 'test value',
      _state: {
        attributes: {
          attribute123: {
            updatable: true
          }
        }
      }
    };

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.INTERACT_ACTION);
    expect(payload.configData.attribute123).toBe('test value');
  });

  // =================================================
  // Load data payload
  // =================================================

  //--------------------------------------------------
  /**
   * Valid load data payload.
   */
  it('when load data action is passed, default load data payload is returned', () => {
    let payload = payloadAssembler.assemblePayloadForAction(request, constants.LOAD_DATA_ACTION);

    expect(payload.criteria.state).toBe(true);
    expect(payload.criteria.childDefs.length).toBe(6);
  });

  //--------------------------------------------------
  /**
   * Valid load data payload.
   */
  it('when load data action is passed with extra childDef, extra childDef is represented in payload', () => {
    request.body.criteria = {
      childDefs: [{ name: 'myNewChildDef' }]
    };

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.LOAD_DATA_ACTION);

    let newChildDef = payload.criteria.childDefs.find(item => {
      return item.name === 'myNewChildDef';
    });

    expect(newChildDef.name).toBe('myNewChildDef');
  });

  // =================================================
  // Previous page payload
  // =================================================

  //--------------------------------------------------
  /**
   * Valid previous page payload.
   */
  it('when previous page action is passed, previous page payload is returned', () => {
    request.params.cacheInstanceId = '1234567890';

    let payload = payloadAssembler.assemblePayloadForAction(
      request,
      constants.PAGE_ACTIONS.previous,
      utils,
      constants
    );

    expect(payload.cacheInstanceId).toBe('1234567890');
    expect(payload.criteria.state).toBe(true);
    expect(payload.criteria.childDefs.length).toBe(6);
  });

  //--------------------------------------------------
  /**
   * Valid load data payload.
   */
  it('when previous page action is passed with extra childDef, extra childDef is represented in payload', () => {
    request.params.cacheInstanceId = '1234567890';

    request.body.criteria = {
      childDefs: [{ name: 'myNewChildDef' }]
    };

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.LOAD_DATA_ACTION);

    let newChildDef = payload.criteria.childDefs.find(item => {
      return item.name === 'myNewChildDef';
    });

    expect(newChildDef.name).toBe('myNewChildDef');
  });

  // =================================================
  // Next page payload
  // =================================================

  //--------------------------------------------------
  /**
   * Valid next page payload.
   */
  it('when next page action is passed, next page payload is returned', () => {
    request.params.cacheInstanceId = '1234567890';

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.PAGE_ACTIONS.next);

    expect(payload.cacheInstanceId).toBe('1234567890');
    expect(payload.criteria.state).toBe(true);
    expect(payload.criteria.childDefs.length).toBe(6);
  });

  //--------------------------------------------------
  /**
   * Valid next page payload.
   */
  it('when next page action is passed with extra childDef, extra childDef is represented in payload', () => {
    request.params.cacheInstanceId = '1234567890';

    request.body.criteria = {
      childDefs: [{ name: 'myNewChildDef' }]
    };

    let payload = payloadAssembler.assemblePayloadForAction(request, constants.PAGE_ACTIONS.next);

    let newChildDef = payload.criteria.childDefs.find(item => {
      return item.name === 'myNewChildDef';
    });

    expect(newChildDef.name).toBe('myNewChildDef');
  });

});
