// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.
/* eslint-disable */

const PassThrough = require('stream').PassThrough;
const sinon = require('sinon');

const ConfiguratorServiceHandler = require('../lib/configurator-service-handler');
const MockConfiguratorRestHandler = require('./mocks/mock-configurator-rest-handler');
const cpqLayoutCacheResponse = require('./data/cpq-layout-cache-response.json');

// Localized resouce messages.
const localizedResources = require('./data/resource-strings.json');

// Mock logger.
const logger = { debug: msg => {}, info: msg => {}, warning: msg => {}, error: msg => {} };

// Using a sinon sandbox allows stubs etc. to be restored after each test.
let sandbox;

// The ConfiguratorServiceHandler instance to be used in every test.
let handler;

// The request/response that will be used in each test.
let request;
let response;

describe('get layout handler', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();
    handler = new ConfiguratorServiceHandler({
      logger: logger
    });

    request = new PassThrough();
    response = new PassThrough();

    request.params = {};
    request.headers = {};
    request.query = {};
    request.body = {};
    request.originalUrl = '/ccstorex/custom/v1/configurations';

    request.t = resource => {
      return localizedResources[resource];
    };
  });

  afterEach(function() {
    sandbox.restore();
    request = null;
    response = null;
    handler = null;
  });

//--------------------------------------------------
  /**
   * productFamily value null
   */
  it('when productFamily key is present but its value is null, error is thrown.', async () => {
    request.query = {
      productFamily: null,
      productLine: 'productLine1',
      model: 'model1',
      hierLevel: '1'
    };

    request.params = {
      flow: 'myFlow'
    }

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetLayout(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * productFamily missing
   */
  it('when productFamily key is missing, error is thrown.', async () => {
    request.query = {
      productLine: 'productLine1',
      model: 'model1',
      hierLevel: '1'
    };

    request.params = {
      flow: 'myFlow'
    }

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetLayout(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * productLine value is null
   */
  it('when productLine key is present but its value is null, error is thrown.', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: '',
      model: 'model1',
      hierLevel: '1'
    };

    request.params = {
      flow: 'myFlow'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetLayout(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * productLine missing
   */
  it('when productLine key is missing, error is thrown.', async () => {
    request.query = {
      productFamily: 'productFamily1',
      model: 'model1',
      hierLevel: '1'
    };

    request.params = {
      flow: 'myFlow'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetLayout(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * model value is null
   */
  it('when model key is present but its value is null, error is thrown.', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: '',
      hierLevel: '1'
    };

    request.params = {
      flow: 'myFlow'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetLayout(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   *  model is missing
   */
  it('when model key is missing, error is thrown.', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      hierLevel: '1'
    };

    request.params = {
      flow: 'myFlow'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetLayout(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * hierLevel value is null
   */
  it('when hierLevel key is present but its value is null, error is thrown.', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1',
      hierLevel: ''
    };

    request.params = {
      flow: 'myFlow'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetLayout(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('hierLevel must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   *  hierLevel is missing
   */
  it('when hierLevel key is missing, error is thrown.', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1'
    };

    request.params = {
      flow: 'myFlow'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetLayout(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('hierLevel must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * CPQ error
   */
  it('when get layout fails in CPQ, error is thrown.', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1',
      hierLevel: '1'
    };

    request.params = {
      flow: 'myFlow'
    };

    const expectedResultStatusCode = 404;
    const expectedResultMessage = 'layoutCache could not be found.';

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockGetResponse: {
        promiseResult: 'reject',
        responseCode: expectedResultStatusCode,
        responseData: expectedResultMessage
      }
    });

    try {
      await handler.handleGetLayout(options);
    }
    catch (error) {
      expect(error.status).toBe(expectedResultStatusCode);
      expect(error.message).toBe(expectedResultMessage);
    }
  });

  //--------------------------------------------------
  /**
   * Valid request
   */
  it('when layout request is valid, success response is returned.', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1',
      hierLevel: '1'
    };

    request.params = {
      flow: 'myFlow'
    };

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockGetResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqLayoutCacheResponse
      }
    });

    let result = await handler.handleGetLayout(options);

    expect(result.responseCode).toBe(200);
    expect(result.responseData.contents.productFamily).toBeDefined();
    expect(result.responseData.contents.productLine).toBeDefined();
    expect(result.responseData.contents.model).toBeDefined();
    expect(result.responseData.contents.attributes).toBeDefined();
    expect(result.responseData.contents.arraysets).toBeDefined();
    expect(result.responseData.contents.layout).toBeDefined();
    expect(result.responseData.contents.actions).toBeDefined();
  });  

});