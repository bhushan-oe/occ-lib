// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.
/* eslint-disable */

const PassThrough = require('stream').PassThrough;
const sinon = require('sinon');
const validators = require('../lib/validators');
const constants = require('../lib/constants');
const localizedResources = require('./data/resource-strings.json');

//------------------------------------------------------------------------------------
/**
 * Tests for the next.
 */
describe('assemble payload', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();

    request = new PassThrough();
    response = new PassThrough();

    request.params = {};
    request.headers = {};
    request.body = {};
    request.query = {};
    request.headers = {};

    request.t = resource => {
      return localizedResources[resource];
    };
  });

  afterEach(function() {
    sandbox.restore();
    request = null;
    response = null;
    handler = null;
  });

  afterEach(function() {
    sandbox.restore();
  });

  //--------------------------------------------------
  it('when productFamily, productLine and model exist on request body, validation passes', () => {
    request.body = {
      'productFamily': 'productFamily1',
      'productLine': 'productLine1',
      'model': 'model1'
    }
    
    expect(validators.validateProductInRequest(request)).toBe(true);
  });

   //--------------------------------------------------
   it('when only productLine and model exist on request body, validation fails', () => {
    request.body = {
      'productLine': 'productLine1',
      'model': 'model1'
    }
    
    expect(validators.validateProductInRequest(request)).toBe(false);
  });

   //--------------------------------------------------
   it('when only productFamily and model exist on request body, validation fails', () => {
    request.body = {
      'productFamily': 'productFamily1',
      'model': 'model1'
    }
    
    expect(validators.validateProductInRequest(request)).toBe(false);
  });

   //--------------------------------------------------
   it('when only productFamily and productLine exist on request body, validation fails', () => {
    request.body = {
      'productFamily': 'productFamily1',
      'productLine': 'productLine1',
    }
    
    expect(validators.validateProductInRequest(request)).toBe(false);
  });

   //--------------------------------------------------
   it('when productFamily, productLine and model do not exist on request body, validation fails', () => {
    request.body = {
    }
    
    expect(validators.validateProductInRequest(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when productFamily, productLine and model exist on request query, validation passes', () => {
    request.query = {
      'productFamily': 'productFamily1',
      'productLine': 'productLine1',
      'model': 'model1'
    }
    
    expect(validators.validateProductInQuery(request)).toBe(true);
  });

  //--------------------------------------------------
  it('when only productLine and model exist on request query, validation fails', () => {
    request.query = {
      'productLine': 'productLine1',
      'model': 'model1'
    }
    
    expect(validators.validateProductInQuery(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when only productFamily and model exist on request query, validation fails', () => {
    request.query = {
      'productFamily': 'productFamily1',
      'model': 'model1'
    }
    
    expect(validators.validateProductInQuery(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when only productFamily and productLine exist on request query, validation fails', () => {
    request.query = {
      'productFamily': 'productFamily1',
      'productLine': 'productLine1',
    }
    
    expect(validators.validateProductInQuery(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when productFamily, productLine and model do not exist on request query, validation fails', () => {
    request.query = {
    }
    
    expect(validators.validateProductInQuery(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when request has page operator but no cacheInstanceId, validation fails', () => {
    request.body.op = 'next';
  
    expect(validators.validateCacheInstanceIdForPageAction(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when request has cacheInstanceId but no page operator , validation fails', () => {
    request.params.cacheInstanceId = '1234567890';
    
    expect(validators.validatePageOperatorExistsForPageAction(request)).toBe(false);
  });
  
  //--------------------------------------------------
  it('when request has cacheInstanceId, page operator and the operator exists in pageActions, validation passes', () => {
    request.params.cacheInstanceId = '1234567890';
    request.body.op = 'next';

    expect(validators.validatePageOperatorForPageAction(request, constants.PAGE_ACTIONS)).toBe(true);
  });

  //--------------------------------------------------
  it('when request has cacheInstanceId, validation passes', () => {
    request.params.cacheInstanceId = '1234567890';
    
    expect(validators.validateCacheInstanceIdInRequest(request)).toBe(true);
  });

  //--------------------------------------------------
  it('when request has no cacheInstanceId, validation fails', () => {    
    expect(validators.validateCacheInstanceIdInRequest(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when request has an arraySetVarName in body, validation passes', () => {    
    request.body.arraySetVarName = 'testArraySetVarName';
    expect(validators.validateArraySetVarNameInRequest(request)).toBe(true);
  });

  //--------------------------------------------------
  it('when request has no arraySetVarName in body, validation fails', () => {    
    expect(validators.validateArraySetVarNameInRequest(request)).toBe(false);
  });
  
  //--------------------------------------------------
  it('when request has an arraySetVarName in query, validation passes', () => {    
    request.query.arraySetVarName = 'testArraySetVarName';
    expect(validators.validateArraySetVarNameInQuery(request)).toBe(true);
  });

  //--------------------------------------------------
  it('when request has no arraySetVarName in query, validation fails', () => {    
    expect(validators.validateArraySetVarNameInQuery(request)).toBe(false);
  });
  
  //--------------------------------------------------
  it('when request body has no configData, validation fails', () => {    
    expect(validators.validateStateAttributesInRequest(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when request body has no configData._state, validation fails', () => {
    request.body.configData = {};  
    expect(validators.validateStateAttributesInRequest(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when request body has no configData._state.attributes, validation fails', () => {
    request.body.configData = {
      '_state': {}
    };  
    expect(validators.validateStateAttributesInRequest(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when request body has configData._state.attributes, validation passes', () => {
    request.body.configData = {
      '_state': {
        'attributes' : {

        }
      }
    };  
    expect(validators.validateStateAttributesInRequest(request)).toBe(true);
  });

  //--------------------------------------------------
  it('when actionResponse has no _flow property, validation fails', () => {  
    const actionResponse = {
      'responseData': {}
    }

    expect(validators.validateLayoutFlowInActionResponse(actionResponse)).toBe(false);
  });

  //--------------------------------------------------
  it('when actionResponse._flow has no links elements, validation fails', () => {  
    const actionResponse = {
      'responseData': {
        '_flow': {
          'links': []
        }
      }
    }

    expect(validators.validateLayoutFlowInActionResponse(actionResponse)).toBe(false);
  });
  
  //--------------------------------------------------
  it('when actionResponse._flow has links element without href, validation fails', () => {  
    const actionResponse = {
      'responseData': {
        '_flow': {
          'links': [
            {
              'blah': 'blah'
            }
          ]
        }
      }
    }

    expect(validators.validateLayoutFlowInActionResponse(actionResponse)).toBe(false);
  });

  //--------------------------------------------------
  it('when actionResponse._flow has no links elements, validation fails', () => {  
    const actionResponse = {
      'responseData': {
        '_flow': {
          'links': [
            {
              'href': 'http://link_to_somewhere'
            }
          ]
        }
      }
    }

    expect(validators.validateLayoutFlowInActionResponse(actionResponse)).toBe(true);
  });

  //--------------------------------------------------
  it('when request query has no removeIndex property, validation fails.', () => {  
    expect(validators.validateRemoveIndexInQuery(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when request query has removeIndex property but its value is less than 0, validation fails.', () => {
    request.query.removeIndex = -1;
    expect(validators.validateRemoveIndexInQuery(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when request query has removeIndex property and its value is greater than -1, validation passes.', () => {
    request.query.removeIndex = 0;
    expect(validators.validateRemoveIndexInQuery(request)).toBe(true);
  });

  //--------------------------------------------------
  it('when an unsupported expand parameter is supplied, validation fails.', () => {
    let expandParameters = ['unsupported_parameter'];

    let invalidExpandParameters =
      validators.validateExpandParameters(expandParameters, constants.SUPPORTED_EXPAND_PARAMETERS)

    expect(invalidExpandParameters.length).toBe(1);
  });

  //--------------------------------------------------
  it('when an supported expand parameters are supplied, validation passes.', () => {
    Object.values(constants.SUPPORTED_EXPAND_PARAMETERS).forEach(parameter => {
      let expandParameters = [parameter];

      let invalidExpandParameters =
        validators.validateExpandParameters(expandParameters, constants.SUPPORTED_EXPAND_PARAMETERS)

      expect(invalidExpandParameters.length).toBe(0);
    });
  });

  //--------------------------------------------------
  it('when request query has no arraySetVarNames property, validation fails.', () => {
    expect(validators.validateArraySetVarNamesInQuery(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when request query has an arraySetVarNames property, validation passes.', () => {
    request.query.arraySetVarNames = [];
    expect(validators.validateArraySetVarNamesInQuery(request)).toBe(true);
  });

  //--------------------------------------------------
  it('when request params has no flow property, validation fails.', () => {
    expect(validators.validateFlowInRequest(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when request params has a flow property, validation passes.', () => {
    request.params.flow = {};
    expect(validators.validateFlowInRequest(request)).toBe(true);
  });
  
  //--------------------------------------------------
  it('when request query has no hierLevel property, validation fails.', () => {
    expect(validators.validateHierLevelInQuery(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when request query has a hierLevel property, validation passes.', () => {
    request.query.hierLevel = 3;
    expect(validators.validateHierLevelInQuery(request)).toBe(true);
  });

  //--------------------------------------------------
  it('when request params has no pickListVarName property, validation fails.', () => {
    expect(validators.validatePickListVarNameInRequest(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when request params has a pickListVarName property, validation passes.', () => {
    request.params.pickListVarName = 'myPickListVarName';
    expect(validators.validatePickListVarNameInRequest(request)).toBe(true);
  });

  //--------------------------------------------------
  it('when request body does not contain an assetKey parameter, validation fails.', () => {
    request.body = {};
    expect(validators.validateAssetKeyInRequest(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when request body contains an assetKey parameter, validation passes.', () => {
    request.body.assetKey = "validAssetKey";
    expect(validators.validateAssetKeyInRequest(request)).toBe(true);
  });

  //--------------------------------------------------
  it('when request body contains an assetKey parameter but no op parameter, validation fails.', () => {
    request.body.assetKey = "validAssetKey";
    expect(validators.validateIsABOSupported(request, constants.SUPPORTED_ASSET_BASED_OPERATIONS)).toBe(false);
  });

  //--------------------------------------------------
  it('when request body contains an assetKey parameter and op parameter is empty, validation fails.', () => {
    request.body.assetKey = "validAssetKey";
    request.body.op = "";

    expect(validators.validateIsABOSupported(request, constants.SUPPORTED_ASSET_BASED_OPERATIONS)).toBe(false);
  });

  //--------------------------------------------------
  it('when request body contains an assetKey parameter and op parameter is not supported, validation fails.', () => {
    request.body.assetKey = "validAssetKey";
    request.body.op = "blah";

    expect(validators.validateIsABOSupported(request, constants.SUPPORTED_ASSET_BASED_OPERATIONS)).toBe(false);
  });

  //--------------------------------------------------
  it('when request body contains an assetKey parameter and op parameter is supported, validation passes.', () => {
    request.body.assetKey = "validAssetKey";
    request.body.op = "MODIFY";

    expect(validators.validateIsABOSupported(request, constants.SUPPORTED_ASSET_BASED_OPERATIONS)).toBe(true);
  });

  //--------------------------------------------------
  it('when request body does not contain an upgradeName parameter, validation fails.', () => {
    request.body = {}
    expect(validators.validateUpgradeNameInRequest(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when request body contains an empty upgradeName parameter, validation fails.', () => {
    request.body.upgradeName = "";
    expect(validators.validateUpgradeNameInRequest(request)).toBe(false);
  });

  //--------------------------------------------------
  it('when request body contains an empty upgradeName parameter, validation fails.', () => {
    request.body.upgradeName = "upgrade123";
    expect(validators.validateUpgradeNameInRequest(request)).toBe(true);
  });

});
