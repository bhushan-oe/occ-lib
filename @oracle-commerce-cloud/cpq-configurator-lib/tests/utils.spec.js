// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.
/* eslint-disable */

const PassThrough = require('stream').PassThrough;
const sinon = require('sinon');

const Utils = require('../lib/utils');
let utils;

const constants = require('../lib/constants');

const registry = require('../registry/registry');

const config = {
  "useHTTPS": true,
  "hostname": "cpq-server",
  "port": "8080",
  "useHTTPSforPreview": true,
  "previewHostname": "",
  "previewPort": "",
  "timeout": 60000,
  "username_env_var": "CPQ_USERNAME",
  "password_env_var": "CPQ_PASSWORD",
  "CONFIG_BASE_URI": "/rest/v8/config",
  "LAYOUT_BASE_URI": "/rest/v8/layoutCache",
  "PAGE_TEMPLATES_BASE_URI": "/rest/v8/pageTemplates",
  "CONFIG_UI_SETTINGS_BASE_URI": "/rest/v8/configUISettings",
  "CONFIGURATOR_SSE_BASE_CONTEXT_URI": "/v1/configurations",
  "registry": registry
}

const MockConfiguratorRestHandler = require('./mocks/mock-configurator-rest-handler');
const layoutCacheResponseWithPickLists = require('./data/utils_responses/layout-cache-with-pick-lists-response.json');
const layoutCacheResponseWithArraySets = require('./data/utils_responses/layout-cache-with-array-sets-response.json');

//------------------------------------------------------------------------------------
/**
 * Tests for the next.
 */
describe('assemble payload', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();

    utils = new Utils(config);

    request = new PassThrough();
    response = new PassThrough();

    request.params = {};
    request.headers = {};
    request.body = {};
    request.query = {};
    request.headers = {};
  });

  afterEach(function() {
    sandbox.restore();
    request = null;
    response = null;
    handler = null;
  });

  afterEach(function() {
    sandbox.restore();
  });

  //--------------------------------------------------
  it('when an unsupported action exists, it will be removed', () => {

    let configData = {
      '_state': {
        'actions': {
          '_update': {
            'enabled': true,
            'visable': true
          },
          '_next': {
            'enabled': true,
            'visable': true
          },
          '_unsupported_action': {
            'enabled': true,
            'visable': true
          }
        }
      }
    };

    utils.removeUnsupportedActions(configData, constants);
    expect(Object.keys(configData._state.actions).includes('_update')).toBe(true);
    expect(Object.keys(configData._state.actions).includes('_next')).toBe(true);
    expect(Object.keys(configData._state.actions).includes('_unsupported_action')).toBe(false);
  });

  
  //--------------------------------------------------
  it('when attributes have been set as not updatable, they will be removed.', () => {

    let configData = {
      '_state': {
        'attributes': {
          'attribute1': {
          'updatable': true
          },
          'attribute2': {
            'updatable': false
          }
        }
      },
      'attribute1': {
        'value': 'sku12345'
      },
      'attribute2': {
        'value': 'sku679890'
      },
      '_setset1': {
        "_state": {
          "defaultRowState": {
            "updatable": true,
            "attributes": {
              "arrayFloatField1": {
                "updatable": false,
                "hasError": false,
                "hasWarning": false,
                "messages": [],
                "visible": true,
                "hasConstraintViolation": false,
                "recommendationMessages": []
              },
              "arrayBoolean1": {
                "updatable": true,
                "hasError": false,
                "hasWarning": false,
                "messages": [],
                "visible": true,
                "hasConstraintViolation": false,
                "recommendationMessages": []
              }
            }
          },
          "attributes": {
            "arrayFloatField1": {
                "visible": true
            },
            "arrayBoolean1": {
                "visible": true
            }
          }
        },
        "items": [
          {
            "arrayBoolean1": true,
            "arrayFloatField1": 1.0,
            "_state": {
              "hasError": false,
              "messages": []
            }
          }
        ]
      }
    };

    let result = utils.removeNonUpdatableAttributes(configData, constants);

    // Attributes that still exist because they are updatable.
    expect(result.attribute1.value).toBe('sku12345');
    expect(result._setset1.items[0].arrayBoolean1).toBe(true);

    // Attributes that have been removed because they are not updatable.
    expect(result.attribute2).toBe(undefined);
    expect(result._setset1.items[0].arrayFloatField1).toBe(undefined);    
  });


  // --------------------------------------------------
  it('when CPQ specific links exist in JSON response, they are removed.', () => {
    let response = {
      'responseData': {
        'cacheInstanceId': '1234567890',
        'configData': {
          "links": [
            {
                "rel": "child",
                "href": "https://cpq-server.us.oracle.com/rest/v8/configlaptop.laptopConfiguration.sku50001/_bom"
            },
            {
                "rel": "child",
                "href": "https://cpq-server.us.oracle.com/rest/v8/configlaptop.laptopConfiguration.sku50001/_recmdModels"
            }
          ]
        },
        '_flow': {
          "links": [
            {
                "rel": "self",
                "href": "https://cpq-server.us.oracle.com/rest/v8/productFamilies/laptop"
            }
        ]
        }
      }
    };

    let result = utils.removeCpqSpecificProperties(response);

    expect(result.responseData.links).toBe(undefined);
    expect(result.responseData._flow).toBe(undefined);
    expect(result.responseData.cacheInstanceId).toBe('1234567890');
  });

  // --------------------------------------------------
  it('when locale is on request body, return it.', () => {
    request.body.locale = 'en_US';
    expect(utils.getLocale(request, constants)).toBe('en_US');
  });

  // --------------------------------------------------
  it('when locale is on request query parameter, return it.', () => {
    request.query.locale = 'en_US';
    expect(utils.getLocale(request, constants)).toBe('en_US');
  });

  // --------------------------------------------------
  it('when locale is on request header, return it.', () => {
    request.headers[constants.LOCALE_HEADER_NAME] = 'en_US';
    expect(utils.getLocale(request, constants)).toBe('en_US');
  });

  // --------------------------------------------------
  it('when locale is not on request, return default.', () => {
    expect(utils.getLocale(request, constants)).toBe('us');
  });

  // --------------------------------------------------
  it('when comma-separated string of expand parameters exist, return them in an array.', () => {
    request.body.expand = 'layout,test,blah';
    let result = utils.getExpandParameters(request)

    expect(result[0]).toBe('layout');
    expect(result[1]).toBe('test');
    expect(result[2]).toBe('blah');
  });

  // --------------------------------------------------
  it('when comma-separated string of expand parameters exist with extra white space, return them in an array with white space removed.', () => {
    request.body.expand = '    layout  ,  test  ,     blah ';
    let result = utils.getExpandParameters(request)
    
    expect(result[0]).toBe('layout');
    expect(result[1]).toBe('test');
    expect(result[2]).toBe('blah');
  });

  // --------------------------------------------------
  it('when no expand parameters exist, null is returned.', () => {
    let result = utils.getExpandParameters(request)
    
    expect(result).toBe(null);
  });

  // --------------------------------------------------
  it('when multiple arrays of attributes exist, they are reduced into a single array.', () => {
    let layoutCache = {
      'responseData': {
        'contents': {
          'attributes': [
            {
              'items': [
                {
                  'id': 12345,
                  'name': 'attribute1'
                }
              ]
            },
            {
              'items': [
                {
                  'id': 67890,
                  'name': 'attribute2'
                }
              ]
            },
            {
              'items': [
                {
                  'id': 54321,
                  'name': 'attribute3'
                }
              ]
            },
          ]
        }
      }
    };

    let result = utils.reduceLayoutCacheContents(layoutCache, 'attributes');

    expect(result[0].id).toBe(12345);
    expect(result[1].id).toBe(67890);
    expect(result[2].id).toBe(54321);
  });

  // --------------------------------------------------
  it('when laptop flow exists at third level of hierarchy, return the flow details.', () => {
    let actionResponse = {
      'responseData': {
        '_flow': {
          'links': [
            {
              'href': 'http://myserver:8080/rest/v8/productFamilies/laptop/productLines/laptopConfiguration/models/sku50001/layouts/laptopFlow'
            }
          ]
        }
      }
    };

    let result = utils.getLayoutFlowDetails(actionResponse);

    expect(result.flow).toBe('laptopFlow');
    expect(result.hierLevel).toBe(3);
  });

  // --------------------------------------------------
  it('when laptop flow exists at third level of hierarchy, return the flow details.', () => {
    let actionResponse = {
      'responseData': {
        '_flow': {
          'links': [
            {
              'href': 'http://myserver:8080/rest/v8/productFamilies/laptop/productLines/laptopConfiguration/layouts/laptopFlow'
            }
          ]
        }
      }
    };

    let result = utils.getLayoutFlowDetails(actionResponse);

    expect(result.flow).toBe('laptopFlow');
    expect(result.hierLevel).toBe(2);
  });

  // --------------------------------------------------
  it('when laptop flow exists at third level of hierarchy, return the flow details.', () => {
    let actionResponse = {
      'responseData': {
        '_flow': {
          'links': [
            {
              'href': 'http://myserver:8080/rest/v8/productFamilies/laptop/layouts/laptopFlow'
            }
          ]
        }
      }
    };

    let result = utils.getLayoutFlowDetails(actionResponse);

    expect(result.flow).toBe('laptopFlow');
    expect(result.hierLevel).toBe(1);
  });

  // --------------------------------------------------
  it('when pick lists exist in a layout cache, their resourceAttributeVarNames are returned in an array.', () => {

    let configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: layoutCacheResponseWithPickLists
      }
    }); 

    let layoutCacheResponse = configuratorRestHandler.mockPostResponse;
    
    let result = utils.getArraySetAndPickListIds(
      layoutCacheResponse.responseData.contents.layout.items.components.items);

    expect(result.pickListResourceAttributeVarNames[0]).toBe('textPicklist');
    expect(result.pickListResourceAttributeVarNames[1]).toBe('floatPicklist');
    expect(result.pickListResourceAttributeVarNames[2]).toBe('intPicklist');
  });

  // --------------------------------------------------
  it('when array sets exist in a layout cache, their variable names are returned in an array.', () => {

    let configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: layoutCacheResponseWithArraySets
      }
    }); 

    let layoutCacheResponse = configuratorRestHandler.mockPostResponse;
    
    let result = utils.getArraySetAndPickListIds(
      layoutCacheResponse.responseData.contents.layout.items.components.items);
    
    expect(result.arraySetVariableNames[0]).toBe('set1');
    expect(result.arraySetVariableNames[1]).toBe('set2');
  });



  // --------------------------------------------------
  it('when ', () => {

    let configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: layoutCacheResponseWithArraySets
      }
    }); 

    let layoutCacheResponse = configuratorRestHandler.mockPostResponse;
    let layoutCacheArraySets = utils.reduceLayoutCacheContents(layoutCacheResponse, 'arraysets');
    let arraySetVariableNames = ['set1', 'set2'];
    
    let result = utils.getArraySetData(layoutCacheArraySets, arraySetVariableNames);
    
    expect(result.set1.sizeAttrVarName).toBe('arrayControl1');
    expect(result.set2.sizeAttrVarName).toBe('arrayControl2');
  });

  // --------------------------------------------------
  it('when array sets exist, append them to the layout cache attributes', () => {

    let configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: layoutCacheResponseWithArraySets
      }
    }); 

    let layoutCacheResponse = configuratorRestHandler.mockPostResponse;
    let layoutCacheAttributes = utils.reduceLayoutCacheContents(layoutCacheResponse, 'attributes');
    let layoutCacheArraySets = utils.reduceLayoutCacheContents(layoutCacheResponse, 'arraysets');
    let arraySetVariableNames = ['set1', 'set2'];
    
    let beforeCount = 0

    layoutCacheAttributes.forEach(item => {
      if (item.array) {
        ++afterCount;
      }
    });

    expect(beforeCount).toBe(0);

    layoutCacheAttributes = utils.addArraySetsToLayoutCacheAttributes(
      layoutCacheAttributes, layoutCacheArraySets, arraySetVariableNames);
    

    let afterCount = 0;

    layoutCacheAttributes.forEach(item => {
      if (item.array) {
        ++afterCount;
      }
    });

    expect(afterCount).toBe(10);
  });

   // --------------------------------------------------
   it('when array sets exist, add them the the configuration actionResponse', () => {

    let configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: layoutCacheResponseWithArraySets
      }
    });

    let layoutCacheResponse = configuratorRestHandler.mockPostResponse;
    let layoutCacheArraySets = utils.reduceLayoutCacheContents(layoutCacheResponse, 'arraysets');
    let arraySetVariableNames = ['set1', 'set2'];
    
    let arraySetsData = utils.getArraySetData(layoutCacheArraySets, arraySetVariableNames);

    let actionResponse = {
      responseData: {}
    };

    utils.addArraySetsVariableNamesToConfiguration(arraySetsData, actionResponse);

    expect(actionResponse.responseData.arraySetVariableNames).toEqual(["_setset1", "_setset2"]);
  });

  // --------------------------------------------------
  it('when pick list options exist for a pick list item, the options are returned.', async () => {

    request.body.populatePickListItems = true;
    let pickListResourceAttributeVarNames = ['floatPicklist'];

    let dispatcher = {
      doPickListGetOptions: (request, response, cacheInstanceId) => {
        let options = {
          'responseData': {
            "cacheInstanceId": "1234567890",
            "variableName": "floatPicklist",
            "items": [
              {
                  "value": 4.4,
                  "displayValue": "4.4"
              },
              {
                  "value": 1.1,
                  "displayValue": "1.1"
              }
            ],
            "links": [
              {
                "rel": "related",
                "href": "https://cpq-server.us.oracle.com/rest/v8/configtest.productLine.model/_pickLists/floatPicklist"
              }
            ]
          }
        };

        return options;
      }
    };
    
    let result = await utils.getPickListOptions(
        request, dispatcher, '1234567890', pickListResourceAttributeVarNames);
      
    expect(result[0].value).toBe(4.4);
    expect(result[1].value).toBe(1.1);
  });

  // --------------------------------------------------
  it('when populatePickListItems is false on request, no options are returned.', async () => {

    request.body.populatePickListItems = false;
    let pickListResourceAttributeVarNames = ['floatPicklist'];

    let dispatcher = {
      doPickListGetOptions: (request, response, cacheInstanceId) => {
        let options = {
          'responseData': {
            "cacheInstanceId": "1234567890",
            "variableName": "floatPicklist",
            "items": [
              {
                  "value": 4.4,
                  "displayValue": "4.4"
              },
              {
                  "value": 1.1,
                  "displayValue": "1.1"
              }
            ],
            "links": [
              {
                "rel": "related",
                "href": "https://cpq-server.us.oracle.com/rest/v8/configtest.productLine.model/_pickLists/floatPicklist"
              }
            ]
          }
        };

        return options;
      }
    };
    
    let result = await utils.getPickListOptions(
        request, response, dispatcher, '1234567890', pickListResourceAttributeVarNames);
      
    expect(result).toEqual([]);
  });

  // --------------------------------------------------
  it('when pickListResourceAttributeVarNames is empty on request, no options are returned.', async () => {

    request.body.populatePickListItems = true;
    let pickListResourceAttributeVarNames = [];

    let dispatcher = {
      doPickListGetOptions: (request, response, cacheInstanceId) => {
        let options = {
          'responseData': {
            "cacheInstanceId": "1234567890",
            "variableName": "floatPicklist",
            "items": [
              {
                  "value": 4.4,
                  "displayValue": "4.4"
              },
              {
                  "value": 1.1,
                  "displayValue": "1.1"
              }
            ],
            "links": [
              {
                "rel": "related",
                "href": "https://cpq-server.us.oracle.com/rest/v8/configtest.productLine.model/_pickLists/floatPicklist"
              }
            ]
          }
        };

        return options;
      }
    };
    
    let result = await utils.getPickListOptions(
        request, dispatcher, '1234567890', pickListResourceAttributeVarNames);
      
    expect(result).toEqual([]);
  });

  // --------------------------------------------------
  it('when mergeDataToLayoutComponents is invoked, merging of arraysets, picklists, cpq server details, column widths and component sorting is comleted successfully.', () => {
    let layoutCacheResponse = {
      responseData: {
        contents: {
          layout: {
            items: {
              components: {
                items: [
                  {
                    sequence: 1,
                    columnWidths: "25~25~50",
                    resourceAttributeVarName: 'testAttribute1'
                  },
                  {
                    sequence: 2,
                    resourceAttributeVarName: 'testAttribute2',
                    components: {
                      items: [
                        {
                          sequence: 2,
                          resourceAttributeVarName: 'testAttribute3'
                        },
                        {
                          sequence: 4,
                          resourceAttributeVarName: 'testArraySetAttribute1',
                          resourceVariableName: {
                            variableName: 'set1'
                          }
                        },
                        {
                          sequence: 3,
                          resourceAttributeVarName: 'testPickList1'
                        },
                        {
                          sequence: 1,
                          resourceAttributeVarName: 'testAttribute4'
                        }
                      ]
                    }
                  }
                ]
              }
            }
          }
        }
      }
    };

    let allAttributesList = [
      {
        variableName: 'testAttribute1',
        menuItems: {
          items: [
            {
              imageUrl: '/path/to/image1.jpg'
            },
            {
              imageUrl: '/path/to/image2.jpg'
            }
          ]
        }
      },
      {
        variableName: 'testAttribute2',
        menuItems: {
          items: [
            {
              imageUrl: '/path/to/image3.jpg'
            },
            {
              imageUrl: '/path/to/image4.jpg'
            }
          ]
        }
      },
      {
        variableName: 'testAttribute3',
        menuItems: {
          items: [
            {
              imageUrl: '/path/to/image5.jpg'
            },
            {
              imageUrl: '/path/to/image6.jpg'
            }
          ]
        }
      },
      {
        variableName: 'testAttribute4',
        menuItems: {
          items: [
            {
              imageUrl: '/path/to/image7.jpg'
            },
            {
              imageUrl: '/path/to/image8.jpg'
            }
          ]
        }
      },
      {
        variableName: 'testPickList1',
      },
      {
        variableName: 'testArraySetAttribute1'
      }
    ];
    
    let pickListOptions = [
      {
        variableName: 'testPickList1',
        items: [
          {
            value: 'red',
            displayValue: 'Red' 
          },
          {
            value: 'blue',
            displayValue: 'Blue' 
          }
        ]
    }];

    let arraySetsData = {
      set1: {
        sizeAttrVarName: 'arrayControl1'
      }
    };

    utils.mergeDataToLayoutComponents(
      layoutCacheResponse,
      allAttributesList,
      arraySetsData,
      pickListOptions);

    expect(layoutCacheResponse).toEqual({
      "responseData": {
        "contents": {
          "layout": {
            "items": {
              "components": {
                "items": [
                  {
                    "sequence": 1,
                    "columnWidths": "25~25~50",
                    "resourceAttributeVarName": "testAttribute1",
                    "resourceAttributeData": {
                      "variableName": "testAttribute1",
                      "menuItems": {
                        "items": [
                          {
                            "imageUrl": "https://cpq-server:8080/path/to/image1.jpg"
                          },
                          {
                            "imageUrl": "https://cpq-server:8080/path/to/image2.jpg"
                          }
                        ]
                      }
                    },
                    "columnWidthsList": [
                      {
                        "width": "25",
                        "colsFor12ColGridSystem": 3
                      },
                      {
                        "width": "25",
                        "colsFor12ColGridSystem": 3
                      },
                      {
                        "width": "50",
                        "colsFor12ColGridSystem": 6
                      }
                    ]
                  },
                  {
                    "sequence": 2,
                    "resourceAttributeVarName": "testAttribute2",
                    "components": {
                      "items": [
                        {
                          "sequence": 1,
                          "resourceAttributeVarName": "testAttribute4",
                          "resourceAttributeData": {
                            "variableName": "testAttribute4",
                            "menuItems": {
                              "items": [
                                {
                                  "imageUrl": "https://cpq-server:8080/path/to/image7.jpg"
                                },
                                {
                                  "imageUrl": "https://cpq-server:8080/path/to/image8.jpg"
                                }
                              ]
                            }
                          }
                        },
                        {
                          "sequence": 2,
                          "resourceAttributeVarName": "testAttribute3",
                          "resourceAttributeData": {
                            "variableName": "testAttribute3",
                            "menuItems": {
                              "items": [
                                {
                                  "imageUrl": "https://cpq-server:8080/path/to/image5.jpg"
                                },
                                {
                                  "imageUrl": "https://cpq-server:8080/path/to/image6.jpg"
                                }
                              ]
                            }
                          }
                        },
                        {
                          "sequence": 3,
                          "resourceAttributeVarName": "testPickList1",
                          "resourceAttributeData": {
                            "variableName": "testPickList1",
                            "pickListItems": [
                              {
                                "value": "red",
                                "displayValue": "Red"
                              },
                              {
                                "value": "blue",
                                "displayValue": "Blue"
                              }
                            ]
                          }
                        },
                        {
                          "sequence": 4,
                          "resourceAttributeVarName": "testArraySetAttribute1",
                          "resourceVariableName": {
                            "variableName": "set1"
                          },
                          "resourceAttributeData": {
                            "variableName": "testArraySetAttribute1"
                          },
                          "sizeAttrVarName": "arrayControl1"
                        }
                      ]
                    },
                    "resourceAttributeData": {
                      "variableName": "testAttribute2",
                      "menuItems": {
                        "items": [
                          {
                            "imageUrl": "https://cpq-server:8080/path/to/image3.jpg"
                          },
                          {
                            "imageUrl": "https://cpq-server:8080/path/to/image4.jpg"
                          }
                        ]
                      }
                    }
                  }
                ]
              }
            }
          }
        }
      }
    });

  });
  

});
