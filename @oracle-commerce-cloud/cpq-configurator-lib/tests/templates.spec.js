// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.
/* eslint-disable */

const PassThrough = require('stream').PassThrough;
const sinon = require('sinon');

const ConfiguratorServiceHandler = require('../lib/configurator-service-handler');
const MockConfiguratorRestHandler = require('./mocks/mock-configurator-rest-handler');
const cpqGetTemplatesResponse = require('./data/template_responses/cpq-get-templates-response.json');

// Localized resouce messages.
const localizedResources = require('./data/resource-strings.json');

// Mock logger.
const logger = { debug: msg => {}, info: msg => {}, warning: msg => {}, error: msg => {} };

// Using a sinon sandbox allows stubs etc. to be restored after each test.
let sandbox;

// The ConfiguratorServiceHandler instance to be used in every test.
let handler;

// The request/response that will be used in each test.
let request;
let response;

describe('get templates handler', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();
    handler = new ConfiguratorServiceHandler({
      logger: logger
    });

    request = new PassThrough();
    response = new PassThrough();

    request.params = {};
    request.headers = {};
    request.query = {};
    request.body = {};
    request.originalUrl = '/ccstorex/custom/v1/configurations';

    request.t = resource => {
      return localizedResources[resource];
    };
  });

  afterEach(function() {
    sandbox.restore();
    request = null;
    response = null;
    handler = null;
  });

  //--------------------------------------------------
  /**
   * CPQ error
   */
  it('when get templates fails in CPQ, error is thrown.', async () => {

    const expectedResultStatusCode = 404;
    const expectedResultMessage = 'templates could not be found.';

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockGetResponse: {
        promiseResult: 'reject',
        responseCode: expectedResultStatusCode,
        responseData: expectedResultMessage
      }
    });

    try {
      await handler.handleGetTemplates(options);
    }
    catch (error) {
      expect(error.status).toBe(expectedResultStatusCode);
      expect(error.message).toBe(expectedResultMessage);
    }
  });

  //--------------------------------------------------
  /**
   * Valid request
   */
  it('when templates request is valid, success response is returned.', async () => {
    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockGetResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqGetTemplatesResponse
      }
    });

    let result = await handler.handleGetTemplates(options);

    expect(result.responseCode).toBe(200);
    expect(result.responseData.modelTemplate).toBeDefined();
    expect(result.responseData.partsTemplate).toBeDefined();
  });  

});