// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.
/* eslint-disable */

const PassThrough = require('stream').PassThrough;
const sinon = require('sinon');

const ConfiguratorServiceHandler = require('../lib/configurator-service-handler');
const MockConfiguratorRestHandler = require('./mocks/mock-configurator-rest-handler');

// Localized resouce messages.
const localizedResources = require('./data/resource-strings.json');

// Mock logger.
const logger = { debug: msg => {}, info: msg => {}, warning: msg => {}, error: msg => {} };

// Using a sinon sandbox allows stubs etc. to be restored after each test.
let sandbox;

// The ConfiguratorServiceHandler instance to be used in every test.
let handler;

// The request/response that will be used in each test.
let request;
let response;

//------------------------------------------------------------------------------------
/**
 * Tests for the add array set row.
 */
describe('array set add row handler', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();
    handler = new ConfiguratorServiceHandler({
      logger: logger
    });

    request = new PassThrough();
    response = new PassThrough();

    request.params = {};
    request.headers = {};
    request.originalUrl = '/ccstorex/custom/v1/configurations';

    request.t = resource => {
      return localizedResources[resource];
    };
  });

  afterEach(function() {
    sandbox.restore();
    request = null;
    response = null;
    handler = null;
  });

  //--------------------------------------------------
  /**
   * Invalid productFamily.
   */
  it('when productFamily key is present but its value is null, error is thrown. (add row)', async () => {
    request.body = {
      productFamily: null,
      productLine: 'productLine1',
      model: 'model1',
      arraySetVarName: 'myArraySet'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleAddArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productFamily.
   */
  it('when productFamily key is missing, error is thrown. (add row)', async () => {
    request.body = {
      productLine: 'productLine1',
      model: 'model1',
      arraySetVarName: 'myArraySet'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleAddArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productLine.
   */
  it('when productLine key is present but its value is null, error is thrown. (add row)', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: '',
      model: 'model1',
      arraySetVarName: 'myArraySet'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleAddArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productLine.
   */
  it('when productLine key is missing, error is thrown. (add row)', async () => {
    request.body = {
      productFamily: 'productFamily1',
      model: 'model1',
      arraySetVarName: 'myArraySet'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleAddArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid model.
   */
  it('when model key is present but its value is null, error is thrown. (add row)', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: '',
      model: '',
      arraySetVarName: 'myArraySet'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleAddArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid model.
   */
  it('when model key is missing, error is thrown. (add row)', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      arraySetVarName: 'myArraySet'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleAddArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid arraySetVarName.
   */
  it('when arraySetVarName key is missing, error is thrown. (add row)', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1'
    };

    request.params = {
      cacheInstanceId: '1234567890'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleAddArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('arraySetVarName must be supplied.');
    }
  });

  it('when add array set row request valid, success response is returned. (add row)', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1',
      arraySetVarName: 'myArraySet'
    };

    request.params = {
      cacheInstanceId: '1234567890'
    };

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: {
          result: 'success'
        }
      }
    });

    let result = await handler.handleAddArraySetRow(options);

    expect(result.responseCode).toBe(200);
    expect(result.responseData.result).toBe('success');
  });

  it('when adding an array set fails in CPQ, error is thrown. (add row)', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1',
      arraySetVarName: 'myArraySet'
    };

    request.params = {
      cacheInstanceId: '1234567890'
    };

    const expectedResultStatusCode = 400;
    const expectedResultMessage = 'Array set row creation failed in CPQ.';

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'reject',
        responseCode: 400,
        responseData: {
          error: expectedResultMessage
        }
      }
    });

    try {
      await handler.handleAddArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(expectedResultStatusCode);
      expect(error.message).toBe(expectedResultMessage);
    }
  });
});

//------------------------------------------------------------------------------------
/**
 * Tests for the delete array set row.
 */
describe('array set delete row handler', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();
    handler = new ConfiguratorServiceHandler({
      logger: logger
    });

    request = new PassThrough();
    response = new PassThrough();

    request.params = {};
    request.headers = {};
    request.query = {};

    request.t = resource => {
      return localizedResources[resource];
    };
  });

  afterEach(function() {
    sandbox.restore();
    request = null;
    response = null;
    handler = null;
  });

  //--------------------------------------------------
  /**
   * Invalid productFamily.
   */
  it('when productFamily key is present but its value is null, error is thrown. (delete row)', async () => {
    request.query = {
      productFamily: null,
      productLine: 'productLine1',
      model: 'model1',
      arraySetVarName: 'myArraySet',
      removeIndex: 0
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleDeleteArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productFamily.
   */
  it('when productFamily key is missing, error is thrown. (delete row)', async () => {
    request.query = {
      productLine: 'productLine1',
      model: 'model1',
      arraySetVarName: 'myArraySet',
      removeIndex: 0
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleDeleteArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productLine.
   */
  it('when productLine key is present but its value is null, error is thrown. (delete row)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: '',
      model: 'model1',
      arraySetVarName: 'myArraySet',
      removeIndex: 0
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleDeleteArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productLine.
   */
  it('when productLine key is missing, error is thrown. (delete row)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      model: 'model1',
      arraySetVarName: 'myArraySet',
      removeIndex: 0
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleDeleteArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid model.
   */
  it('when model key is present but its value is null, error is thrown. (delete row)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: '',
      arraySetVarName: 'myArraySet',
      removeIndex: 0
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleDeleteArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid model.
   */
  it('when model key is missing, error is thrown. (delete row)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      arraySetVarName: 'myArraySet',
      removeIndex: 0
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleDeleteArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid arraySetVarName.
   */
  it('when arraySetVarName key is missing, error is thrown. (delete row)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1',
      removeIndex: 0
    };

    request.params = {
      cacheInstanceId: '1234567890'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleDeleteArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('arraySetVarName must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid removeIndex.
   */
  it('when removeIndex key is missing, error is thrown. (delete row)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1',
      arraySetVarName: 'myArraySet'
    };

    request.params = {
      cacheInstanceId: '1234567890'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleDeleteArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('removeIndex must be supplied.');
    }
  });

  /**
   *
   */
  it('when add array set row request valid, success response is returned. (delete row)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1',
      arraySetVarName: 'myArraySet',
      removeIndex: 1
    };

    request.params = {
      cacheInstanceId: '1234567890'
    };

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: {
          result: 'success'
        }
      }
    });

    let result = await handler.handleDeleteArraySetRow(options);

    expect(result.responseCode).toBe(200);
    expect(result.responseData.result).toBe('success');
  });

  it('when adding an array set fails in CPQ, error is thrown. (delete row)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1',
      arraySetVarName: 'myArraySet',
      removeIndex: 1
    };

    request.params = {
      cacheInstanceId: '1234567890'
    };

    const expectedResultStatusCode = 400;
    const expectedResultMessage = 'Array set row creation failed in CPQ.';

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'reject',
        responseCode: 400,
        responseData: {
          error: expectedResultMessage
        }
      }
    });

    try {
      await handler.handleDeleteArraySetRow(options);
    }
    catch (error) {
      expect(error.status).toBe(expectedResultStatusCode);
      expect(error.message).toBe(expectedResultMessage);
    }
  });
});

describe('array set get config data handler', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();
    handler = new ConfiguratorServiceHandler({
      logger: logger
    });

    request = new PassThrough();
    response = new PassThrough();

    request.params = {};
    request.headers = {};
    request.query = {};
    request.body = {};

    request.t = resource => {
      return localizedResources[resource];
    };
  });

  afterEach(function() {
    sandbox.restore();
    request = null;
    response = null;
    handler = null;
  });

  //--------------------------------------------------
  /**
   * productFamily value null
   */
  it('when productFamily key is present but its value is null, error is thrown. (get configData)', async () => {
    request.query = {
      productFamily: null,
      productLine: 'productLine1',
      model: 'model1',
      arraySetVarNames: 'myArraySet1,myArraySet2',
    };

    request.params = {
      cacheInstanceId: '123456'
    }

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetArraySetConfigData(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * productFamily missing
   */
  it('when productFamily key is missing, error is thrown. (get configData)', async () => {
    request.query = {
      productLine: 'productLine1',
      model: 'model1',
      arraySetVarNames: 'myArraySet1,myArraySet2',
    };

    request.params = {
      cacheInstanceId: '123456'
    }

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetArraySetConfigData(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * productLine value is null
   */
  it('when productLine key is present but its value is null, error is thrown. (get configData)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: '',
      model: 'model1',
      arraySetVarNames: 'myArraySet1,myArraySet2',
    };

    request.params = {
      cacheInstanceId: '123456'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetArraySetConfigData(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * productLine missing
   */
  it('when productLine key is missing, error is thrown. (get configData)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      model: 'model1',
      arraySetVarNames: 'myArraySet1,myArraySet2',
    };

    request.params = {
      cacheInstanceId: '123456'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetArraySetConfigData(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

    //--------------------------------------------------
  /**
   * model value is null
   */
  it('when model key is present but its value is null, error is thrown. (get configData)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: '',
      model: '',
      arraySetVarNames: 'myArraySet1,myArraySet2',
    };

    request.params = {
      cacheInstanceId: '123456'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetArraySetConfigData(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   *  model is missing
   */
  it('when model key is missing, error is thrown. (get configData)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      arraySetVarNames: 'myArraySet1,myArraySet2'
    };

    request.params = {
      cacheInstanceId: '123456'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetArraySetConfigData(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * arraySetVarNames missing
   */
  it('when arraySetVarNames key is missing, error is thrown. (get configData)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1'
    };

    request.params = {
      cacheInstanceId: '123456'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetArraySetConfigData(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('arraySetVarNames must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * CPQ error
   */
  it('when get array set configData fails in CPQ, error is thrown. (get configData)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1',
      arraySetVarNames: 'myArraySet1,myArraySet2'
    };

    request.params = {
      cacheInstanceId: '123456'
    };

    const expectedResultStatusCode = 400;
    const expectedResultMessage = 'Array set get configData failed in CPQ.';

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'reject',
        responseCode: 400,
        responseData: {
          error: expectedResultMessage
        }
      }
    });

    try {
      await handler.handleGetArraySetConfigData(options);
    }
    catch (error) {
      expect(error.status).toBe(expectedResultStatusCode);
      expect(error.message).toBe(expectedResultMessage);
    }
  });

  //--------------------------------------------------
  /**
   * Valid request
   */
  it('when get array set configData request is valid, success response is returned. (get configData)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1',
      arraySetVarNames: 'myArraySet1,myArraySet2'
    };

    request.params = {
      cacheInstanceId: '1234567890'
    };

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: {
          result: 'success'
        }
      }
    });

    let result = await handler.handleGetArraySetConfigData(options);

    expect(result.responseCode).toBe(200);
    expect(result.responseData.result).toBe('success');
  });  

});
