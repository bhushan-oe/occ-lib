// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.
/* eslint-disable */

const PassThrough = require('stream').PassThrough;
const sinon = require('sinon');

const ConfiguratorServiceHandler = require('../lib/configurator-service-handler');
const MockConfiguratorRestHandler = require('./mocks/mock-configurator-rest-handler');

// Mock data response files.
const cpqPreviousResponse = require('./data/previous_responses/cpq-previous-response.json');
const cpqNextResponse = require('./data/next_responses/cpq-next-response.json');
const cpqLayoutCacheResponse = require('./data/cpq-layout-cache-response.json');

// Localized resouce messages.
const localizedResources = require('./data/resource-strings.json');

// Mock logger.
const logger = { debug: msg => {}, info: msg => {}, warning: msg => {}, error: msg => {} };

// Using a sinon sandbox allows stubs etc. to be restored after each test.
let sandbox;

// The ConfiguratorServiceHandler instance to be used in every test.
let handler;

// The request/response that will be used in each test.
let request;
let response;

//------------------------------------------------------------------------------------
/**
 * Tests for the next.
 */
describe('next handler', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();
    handler = new ConfiguratorServiceHandler({
      logger: logger
    });

    request = new PassThrough();
    response = new PassThrough();

    request.params = {};
    request.headers = {};
    request.body = {};
    request.query = {};
    request.originalUrl = '/ccstorex/custom/v1/configurations';

    request.t = resource => {
      return localizedResources[resource];
    };
  });

  afterEach(function() {
    sandbox.restore();
    request = null;
    response = null;
    handler = null;
  });

  //--------------------------------------------------
  /**
   * Valid next action.
   */
  it('when next invocation request is valid, invalid actions are removed from actions list.', async () => {
    request.body = {
      productFamily: 'laptop',
      productLine: 'laptopConfiguration',
      model: 'sku50001',
      op: 'next',
      expand: 'layout'
    };

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqNextResponse
      },
      mockGetResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqLayoutCacheResponse
      }
    });

    request.params = {
      cacheInstanceId: '1234567890'
    };

    let result = await handler.handlePage(options);

    expect(result.responseCode).toBe(200);
    expect(result.responseData.configData._state.actions._next.visible).toBe(false);
    expect(result.responseData.configData._state.actions._previous.visible).toBe(true);
    expect(result.responseData.configData._state.actions._update.visible).toBe(true);
    expect(result.responseData.configData._state.actions._integration_addToCart.visible).toBe(true);
  });
});

//------------------------------------------------------------------------------------
/**
 * Tests for the previous.
 */
describe('previous handler', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();
    handler = new ConfiguratorServiceHandler({
      logger: logger
    });

    request = new PassThrough();
    response = new PassThrough();

    request.params = {};
    request.headers = {};
    request.body = {};
    request.query = {};
    request.originalUrl = '/ccstorex/custom/v1/configurations';

    request.t = resource => {
      return localizedResources[resource];
    };
  });

  afterEach(function() {
    sandbox.restore();
    request = null;
    response = null;
    handler = null;
  });
  
    //--------------------------------------------------
  /**
   * Invalid operation.
   */
  it('when page operation and cacheInstanceId is present but page operation is invalid, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1',
      op: 'non-existent-page-operation',
      expand: 'layout'
    };

    let options = {
      request: request,
      response: response
    };

    request.params = {
      cacheInstanceId: '1234567890'
    };

    try {
      await handler.handlePage(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('Page operation is not valid.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid operation.
   */
  it('when cacheInstanceId is present but next/previous is not, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1',
      expand: 'layout'
    };

    let options = {
      request: request,
      response: response
    };

    request.params = {
      cacheInstanceId: '1234567890'
    };

    try {
      await handler.handlePage(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('next/previous action op is required for page action.');
    }
  });

  //--------------------------------------------------
  /**
   * Valid previous action.
   */
  it('when previous invocation request is valid, invalid actions are removed from actions list.', async () => {
    request.body = {
      productFamily: 'laptop',
      productLine: 'laptopConfiguration',
      model: 'sku50001',
      op: 'previous',
      expand: 'layout'
    };

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqPreviousResponse
      },
      mockGetResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqLayoutCacheResponse
      }
    });

    request.params = {
      cacheInstanceId: '1234567890'
    };

    let result = await handler.handlePage(options);

    expect(result.responseCode).toBe(200);
    expect(result.responseData.configData._state.actions._next.visible).toBe(true);
    expect(result.responseData.configData._state.actions._previous.visible).toBe(false);
    expect(result.responseData.configData._state.actions._update.visible).toBe(true);
    expect(result.responseData.configData._state.actions._integration_addToCart.visible).toBe(false);
  });

  //--------------------------------------------------
  /**
   * Invalid operation.
   */
  it('when page operation is present but cacheInstanceId is not, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1',
      op: 'previous',
      expand: 'layout'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handlePage(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('cacheInstanceId is required for next/previous action.');
    }
  });
});

 