// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.
/* eslint-disable */
const ConfiguratorRestHandler = require('@oracle-commerce-cloud/cpq-configurator-rest').ConfiguratorRestHandler;

/**
 * Class that mocks the ConfiguratorRestHandler.
 *
 * The constructor for this class accepts a config object that should contain
 * a 'mockPostResponse' object and/or a 'mockGetResponse' object that contains
 * the type of promise, the responseCode and responseData that should be returned.
 * For example:
 *
 * let configuratorRestHandler = new MockConfiguratorRestHandler({
 *   mockPostResponse: {
 *     promiseResult: 'resolve',
 *     responseCode: 200,
 *     responseData: cpqConfigureResponse
 *   },
 *   mockGetResponse: {
 *     promiseResult: 'reject',
 *     responseCode: 400,
 *     responseData: cpqLayoutResponse
 *   }
 * })
 *
 */
class MockConfiguratorRestHandler extends ConfiguratorRestHandler {
  constructor(config) {
    super(config);
    this.mockPostResponse = config.mockPostResponse;
    this.mockGetResponse = config.mockGetResponse;
  }

  /**
   * A mock implementation of the ConfiguratorRestHandler.post method.
   *
   * @param {Object} serviceRequest
   *   The request that contains the configurator action URI and payload.
   *
   * @returns {Promise}
   */
  post(serviceRequest) {
    let response = {
      responseCode: this.mockPostResponse.responseCode,
      responseData: this.mockPostResponse.responseData
    };

    if (this.mockPostResponse.promiseResult === 'resolve') {
      return Promise.resolve(response);
    } 
    else if (this.mockPostResponse.promiseResult === 'reject') {
      return Promise.reject(response);
    }
  }

  /**
   * A mock implementation of the ConfiguratorRestHandler.get method.
   *
   * @param {Object} request
   *   The request object passed to the the rest handler get function.
   *
   * @returns {Promise}
   */
  get(request) {
    let response = {
      responseCode: this.mockGetResponse.responseCode,
      responseData: this.mockGetResponse.responseData
    };

    if (this.mockGetResponse.promiseResult === 'resolve') {
      return Promise.resolve(response);
    } 
    else if (this.mockGetResponse.promiseResult === 'reject') {
      return Promise.reject(response);
    }
  }
}

module.exports = MockConfiguratorRestHandler;
