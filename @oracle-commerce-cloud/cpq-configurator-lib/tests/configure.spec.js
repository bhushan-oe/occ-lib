// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.
/* eslint-disable */

const PassThrough = require('stream').PassThrough;
const sinon = require('sinon');

const ConfiguratorServiceHandler = require('../lib/configurator-service-handler');
const MockConfiguratorRestHandler = require('./mocks/mock-configurator-rest-handler');

const constants = require('../lib/constants');

// Mock data response files.
const cpqConfigureResponse = require('./data/configure_responses/cpq-configure-response.json');
const cpqConfigureResponseNoLayoutFlow = require('./data/configure_responses/cpq-configure-response-no-layout-flow.json');
const cpqLayoutCacheResponse = require('./data/cpq-layout-cache-response.json');
const cpqUiSettingsResponse = require('./data/configure_responses/cpq-uiSettings-response.json');
const cpqPageTemplatesResponse = require('./data/configure_responses/cpq-page-templates-response.json');

// Localized resouce messages.
const localizedResources = require('./data/resource-strings.json');

// Mock logger.
const logger = { debug: msg => {}, info: msg => {}, warning: msg => {}, error: msg => {} };

// Using a sinon sandbox allows stubs etc. to be restored after each test.
let sandbox;

// The ConfiguratorServiceHandler instance to be used in every test.
let handler;

// The request/response that will be used in each test.
let request;
let response;

//------------------------------------------------------------------------------------
/**
 * Tests for the configure.
 */
describe('configure handler', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();
    handler = new ConfiguratorServiceHandler({
      logger: logger
    });

    request = new PassThrough();
    response = new PassThrough();
    request.params = {};
    request.headers = {};
    request.body = {};
    request.query = {};
    request.originalUrl = '/ccstorex/custom/v1/configurations';

    request.t = resource => {
      return localizedResources[resource];
    };
  });

  afterEach(function() {
    sandbox.restore();
    request = null;
    response = null;
    handler = null;
  });

  //--------------------------------------------------
  /**
   * Invalid handler initialization.
   */
  it('when ConfiguratorServiceHandler is initialized with no configuration, error is thrown.', () => {
    expect(() => {
      new ConfiguratorServiceHandler();
    }).toThrow(new Error('ConfiguratorServiceHandler requires config to be instantiated'));
  });

  //--------------------------------------------------
  /**
   * Valid handler initialization.
   */
  it('when ConfiguratorServiceHandler is initialized with no configuration, valid handler is created.', () => {
    expect(handler).not.toBe(null);
  });

  //--------------------------------------------------
  /**
   * Invalid productFamily.
   */
  it('when productFamily key is present but its value is null, error is thrown.', async () => {
    request.body = {
      productFamily: null,
      productLine: 'productLine1',
      model: 'model1'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleConfigure(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.resourceKey).toBe(constants.INVALID_PRODUCT_INPUT);
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productFamily.
   */
  it('when productFamily key is missing, error is thrown.', async () => {
    request.body = {
      productLine: 'productLine1',
      model: 'model1'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleConfigure(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.resourceKey).toBe(constants.INVALID_PRODUCT_INPUT);
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productFamily.
   */
  it('when productFamily value is not found on external system, error is thrown.', async () => {
    request.body = {
      productFamily: 'invalidProductFamily',
      productLine: 'productLine1',
      model: 'model1'
    };

    const expectedResultStatusCode = 400;
    const expectedResultMessage = 'Product Family not found for the label invalidProductFamily.';

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'reject',
        responseCode: expectedResultStatusCode,
        responseData: {
          error: expectedResultMessage
        }
      }
    });

    try {
      await handler.handleConfigure(options);
    }
    catch (error) {
      expect(error.status).toBe(expectedResultStatusCode);
      expect(error.message).toBe(expectedResultMessage);
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productLine.
   */
  it('when productLine key is present but its value is null, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: '',
      model: 'model1'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleConfigure(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.resourceKey).toBe(constants.INVALID_PRODUCT_INPUT);
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productLine.
   */
  it('when productLine key is missing, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      model: 'model1'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleConfigure(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.resourceKey).toBe(constants.INVALID_PRODUCT_INPUT);
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productLine.
   */
  it('when productLine value is not found on external system, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'invalidProductLine',
      model: 'model1'
    };

    const expectedResultStatusCode = 400;
    const expectedResultMessage =
      'The model model1 belonging to the product line invalidProductLine under the segment productFamily1 could not be found.';

    let options = {
      request: request,
      response: response
    };
    
    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'reject',
        responseCode: expectedResultStatusCode,
        responseData: {
          'o:errorDetails': [
            {
              title: expectedResultMessage
            }
          ]
        }
      }
    });

    try {
      await handler.handleConfigure(options);
    }
    catch (error) {
      expect(error.status).toBe(expectedResultStatusCode);
      expect(error.message).toBe(expectedResultMessage);
    }
  });

  //--------------------------------------------------
  /**
   * Invalid model.
   */
  it('when model key is present but its value is null, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: '',
      model: ''
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleConfigure(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.resourceKey).toBe(constants.INVALID_PRODUCT_INPUT);
    }
  });

  //--------------------------------------------------
  /**
   * Invalid model.
   */
  it('when model key is missing, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'productLine1'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleConfigure(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.resourceKey).toBe(constants.INVALID_PRODUCT_INPUT);
    }
  });

  //--------------------------------------------------
  /**
   * Invalid model.
   */
  it('when model value is not found on external system, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'invalidModel'
    };

    const expectedResultStatusCode = 400;
    const expectedResultMessage =
      'The model invalidModel belonging to the product line productLine1 under the segment productFamily1 could not be found.';

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'reject',
        responseCode: expectedResultStatusCode,
        responseData: {
          title: expectedResultMessage
        }
      }
    });

    try {
      await handler.handleConfigure(options);
    }
    catch (error) {
      expect(error.status).toBe(expectedResultStatusCode);
      expect(error.message).toBe(expectedResultMessage);
    }
  });

  //--------------------------------------------------
  /**
   * Invalid layoutCache request
   */
  it('when invalid layoutCache URI is invoked, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1',
      expand: 'layout'
    };

    const expectedResultStatusCode = 404;
    const expectedResultMessage = 'layoutCache could not be found.';

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqConfigureResponse
      },
      mockGetResponse: {
        promiseResult: 'reject',
        responseCode: expectedResultStatusCode,
        responseData: expectedResultMessage
      }
    });

    try {
      await handler.handleConfigure(options);
    }
    catch (error) {
      expect(error.status).toBe(expectedResultStatusCode);
      expect(error.message).toBe(expectedResultMessage);
    }
  });

  //--------------------------------------------------
  /**
   * Invalid layoutCache request
   */
  it('when no layout flow href is found, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1',
      expand: 'layout'
    };

    const expectedResultStatusCode = 400;
    const expectedResultMessage = 'layoutCache could not be found.';

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqConfigureResponseNoLayoutFlow
      }
    });

    try {
      await handler.handleConfigure(options);
    }
    catch (error) {
      expect(error.status).toBe(expectedResultStatusCode);
      expect(error.message).toBe(expectedResultMessage);
    }
  });

  //--------------------------------------------------
  /**
   * Valid configure action.
   */
  it('when configure invocation request is valid, full valid response is returned.', async () => {
    request.body = {
      productFamily: 'laptop',
      productLine: 'laptopConfiguration',
      model: 'sku50001',
      expand: 'layout'
    };

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqConfigureResponse
      },
      mockGetResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqLayoutCacheResponse
      }
    });

    let result = await handler.handleConfigure(options);

    expect(result.responseCode).toBe(200);
    expect(result.responseData.configData._state.attributes).toBeDefined();
    expect(result.responseData.attributes).not.toBeDefined();
    expect(result.responseData.layout).toBeDefined();
  });

  //--------------------------------------------------
  /**
   * Valid configure action.
   */
  it('when configure invocation request is valid, attributes will be merged into layout correctly.', async () => {
    request.body = {
      productFamily: 'laptop',
      productLine: 'laptopConfiguration',
      model: 'sku50001',
      expand: 'layout'
    };

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqConfigureResponse
      },
      mockGetResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqLayoutCacheResponse
      }
    });

    let result = await handler.handleConfigure(options);

    expect(result.responseCode).toBe(200);
    expect(result.responseData.attributes).not.toBeDefined();
    expect(result.responseData.configData._state.attributes).toBeDefined();

    // Attribute data gets correctly merged to layout.
    expect(
      result.responseData.layout.items.components.items[0].components.items[0]
        .components.items[0].components.items[0].components.items[0].components.items[0]
        .resourceAttributeData.name
    ).toEqual('currencyCode');
  });

  //--------------------------------------------------
  /**
   * Valid configure action.
   */
  it('when configure invocation request is valid, CPQ specific properties will be removed from response.', async () => {
    request.body = {
      productFamily: 'laptop',
      productLine: 'laptopConfiguration',
      model: 'sku50001',
      expand: 'layout'
    };

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqConfigureResponse
      },
      mockGetResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqLayoutCacheResponse
      }
    });

    let result = await handler.handleConfigure(options);

    expect(result.responseCode).toBe(200);
    expect(result.responseData.configData.links).not.toBeDefined();
    expect(result.responseData._flow).not.toBeDefined();
  });

  //--------------------------------------------------
  /**
   * Valid configure action. Layout sequence correct.
   */
  it('when configure layout components sequence out of order, they are sorted in response.', async () => {
    request.body = {
      productFamily: 'laptop',
      productLine: 'laptopConfiguration',
      model: 'sku50001',
      expand: 'layout',
      configData: {
        processor: {
          value: 'sku12345',
          displayValue: 'i7'
        }
      }
    };

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqConfigureResponse
      },
      mockGetResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqLayoutCacheResponse
      }
    });

    let result = await handler.handleConfigure(options);

    expect(result.responseCode).toBe(200);

    // Components should be in sequence.
    expect(
      result.responseData.layout.items.components.items[0].components.items[0]
        .components.items[0].components.items[0].components.items[0].components.items[0]
        .sequence
    ).toEqual(1);

    expect(
      result.responseData.layout.items.components.items[0].components.items[0]
        .components.items[0].components.items[0].components.items[0].components.items[1]
        .sequence
    ).toEqual(2);

    expect(
      result.responseData.layout.items.components.items[0].components.items[0]
        .components.items[0].components.items[0].components.items[0].components.items[2]
        .sequence
    ).toEqual(3);

    expect(
      result.responseData.layout.items.components.items[0].components.items[0]
        .components.items[0].components.items[0].components.items[0].components.items[3]
        .sequence
    ).toEqual(4);

    expect(
      result.responseData.layout.items.components.items[0].components.items[0]
        .components.items[0].components.items[0].components.items[0].components.items[4]
        .sequence
    ).toEqual(5);

    expect(
      result.responseData.layout.items.components.items[0].components.items[0]
        .components.items[0].components.items[0].components.items[0].components.items[5]
        .sequence
    ).toEqual(6);

    expect(
      result.responseData.layout.items.components.items[0].components.items[0]
        .components.items[0].components.items[0].components.items[0].components.items[6]
        .sequence
    ).toEqual(7);

    expect(
      result.responseData.layout.items.components.items[0].components.items[0]
        .components.items[0].components.items[0].components.items[0].components.items[7]
        .sequence
    ).toEqual(8);
  });

  //--------------------------------------------------
  /**
   * Valid configure action. 'attributes' will be included (configData._state.attributes will still always be returned).
   */
  it('when configure PANEL contains columnWidths, 12 column grid system property is calculated correctly and returned', async () => {
    request.body = {
      productFamily: 'laptop',
      productLine: 'laptopConfiguration',
      model: 'sku50001',
      expand: 'layout'
    };

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqConfigureResponse
      },
      mockGetResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqLayoutCacheResponse
      }
    });

    let result = await handler.handleConfigure(options);

    expect(result.responseCode).toBe(200);

    // There will be 3 columns. First column is 25%, second is 25% and the third is 50%.
    expect(
      result.responseData.layout.items.components.items[0].components.items[0]
        .components.items[0].columnWidthsList[0].width
    ).toBe(Number(25.0).toFixed(1));
    expect(
      result.responseData.layout.items.components.items[0].components.items[0]
        .components.items[0].columnWidthsList[0].colsFor12ColGridSystem
    ).toBe(3);

    expect(
      result.responseData.layout.items.components.items[0].components.items[0]
        .components.items[0].columnWidthsList[1].width
    ).toBe(Number(25.0).toFixed(1));
    expect(
      result.responseData.layout.items.components.items[0].components.items[0]
        .components.items[0].columnWidthsList[1].colsFor12ColGridSystem
    ).toBe(3);

    expect(
      result.responseData.layout.items.components.items[0].components.items[0]
        .components.items[0].columnWidthsList[2].width
    ).toBe(Number(50.0).toFixed(1));
    expect(
      result.responseData.layout.items.components.items[0].components.items[0]
        .components.items[0].columnWidthsList[2].colsFor12ColGridSystem
    ).toBe(6);
  });

  //--------------------------------------------------
  /**
   * Valid configure action.
   */
  it('when handlePickListOptions is invoked, return valid options.', async () => {
    
    request.params = {
      cacheInstanceId: '1234567890',
      pickListVarName: "myPickListVarName"
    };

    request.query = {
      productFamily: 'laptop',
      productLine: 'laptopConfiguration',
      model: 'sku50001'
    };

    const pickListOptionsResponse = {
      "cacheInstanceId": "TkdTBck2EIDdO0n1jqwRQ4DZlKhdcX9d2gFa5HtshmaOzkyM5DEKmWy2PoKE6nx6",
      "variableName": "myPickListVarName",
      "items": [
          {
              "value": "",
              "displayValue": ""
          },
          {
              "value": 4.4,
              "displayValue": "4.4"
          }
        ]
      };

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: pickListOptionsResponse
      }
    });

    let result = await handler.handleGetPickListOptions(options);

    expect(result.responseCode).toBe(200);
    expect(result.responseData.items[1].displayValue).toBe("4.4");
  });

  //--------------------------------------------------
  /**
   * Valid configure action.
   */
  it('when ui settings expand parameter set, uiSettings request is invoked.', async () => {
    request.body = {
      productFamily: 'laptop',
      productLine: 'laptopConfiguration',
      model: 'sku50001',
      expand: 'uiSettings'
    };

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqConfigureResponse
      },
      mockGetResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqUiSettingsResponse
      }
    });

    let result = await handler.handleConfigure(options);

    expect(result.responseCode).toBe(200);
    expect(result.responseData.uiSettings).toBeDefined();
  });

  //--------------------------------------------------
  /**
   * Valid configure action.
   */
  it('when templates expand parameter set, pageTemplates request is invoked.', async () => {
    request.body = {
      productFamily: 'laptop',
      productLine: 'laptopConfiguration',
      model: 'sku50001',
      expand: 'templates'
    };

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqConfigureResponse
      },
      mockGetResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqPageTemplatesResponse
      }
    });

    let result = await handler.handleConfigure(options);

    expect(result.responseCode).toBe(200);
    expect(result.responseData.templates).toBeDefined();
  });
});
