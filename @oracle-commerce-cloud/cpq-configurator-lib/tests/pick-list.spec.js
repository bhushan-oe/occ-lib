// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.
/* eslint-disable */

const PassThrough = require('stream').PassThrough;
const sinon = require('sinon');

const ConfiguratorServiceHandler = require('../lib/configurator-service-handler');
const MockConfiguratorRestHandler = require('./mocks/mock-configurator-rest-handler');

// Localized resouce messages.
const localizedResources = require('./data/resource-strings.json');

// Mock logger.
const logger = { debug: msg => {}, info: msg => {}, warning: msg => {}, error: msg => {} };

// Using a sinon sandbox allows stubs etc. to be restored after each test.
let sandbox;

// The ConfiguratorServiceHandler instance to be used in every test.
let handler;

// The request/response that will be used in each test.
let request;
let response;

describe('pick list get options handler', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();
    handler = new ConfiguratorServiceHandler({
      logger: logger
    });

    request = new PassThrough();
    response = new PassThrough();

    request.params = {};
    request.headers = {};
    request.query = {};
    request.body = {};
    request.originalUrl = '/ccstorex/custom/v1/configurations';

    request.t = resource => {
      return localizedResources[resource];
    };
  });

  afterEach(function() {
    sandbox.restore();
    request = null;
    response = null;
    handler = null;
  });

  //--------------------------------------------------
  /**
   * productFamily value null
   */
  it('when productFamily key is present but its value is null, error is thrown. (get options)', async () => {
    request.query = {
      productFamily: null,
      productLine: 'productLine1',
      model: 'model1'
    };

    request.params = {
      cacheInstanceId: '123456',
      pickListVarName: 'myPickListVarName'
    }

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetPickListOptions(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    };
  });

  //--------------------------------------------------
  /**
   * productFamily missing
   */
  it('when productFamily key is missing, error is thrown. (get options)', async () => {
    request.query = {
      productLine: 'productLine1',
      model: 'model1'
    };

    request.params = {
      cacheInstanceId: '123456',
      pickListVarName: 'myPickListVarName'
    }

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetPickListOptions(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    };
  });

  //--------------------------------------------------
  /**
   * productLine value is null
   */
  it('when productLine key is present but its value is null, error is thrown. (get options)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: '',
      model: 'model1'
    };

    request.params = {
      cacheInstanceId: '123456',
      pickListVarName: 'myPickListVarName'
    }

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetPickListOptions(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    };
  });

  //--------------------------------------------------
  /**
   * productLine missing
   */
  it('when productLine key is missing, error is thrown. (get options)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      model: 'model1',
    };

    request.params = {
      cacheInstanceId: '123456',
      pickListVarName: 'myPickListVarName'
    }

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetPickListOptions(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    };
  });

    //--------------------------------------------------
  /**
   * model value is null
   */
  it('when model key is present but its value is null, error is thrown. (get options)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: '',
      model: ''
    };

    request.params = {
      cacheInstanceId: '123456',
      pickListVarName: 'myPickListVarName'
    }

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetPickListOptions(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   *  model is missing
   */
  it('when model key is missing, error is thrown. (get options)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1'
    };

    request.params = {
      cacheInstanceId: '123456',
      pickListVarName: 'myPickListVarName'
    }

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleGetPickListOptions(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * CPQ error
   */
  it('when get pick list options fails in CPQ, error is thrown. (get options)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1'
    };

    request.params = {
      cacheInstanceId: '123456',
      pickListVarName: 'myPickListVarName'
    }

    const expectedResultStatusCode = 400;
    const expectedResultMessage = 'Pick list get options failed in CPQ.';

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'reject',
        responseCode: 400,
        responseData: {
          error: expectedResultMessage
        }
      }
    });

    try {
      await handler.handleGetPickListOptions(options);
    }
    catch (error) {
      expect(error.status).toBe(expectedResultStatusCode);
      expect(error.message).toBe(expectedResultMessage);
    }
  });

  //--------------------------------------------------
  /**
   * Valid request
   */
  it('when pick list get options request is valid, success response is returned. (get options)', async () => {
    request.query = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'model1'
    };

    request.params = {
      cacheInstanceId: '123456',
      pickListVarName: 'myPickListVarName'
    }

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: {
          result: 'success'
        }
      }
    });

    let result = await handler.handleGetPickListOptions(options);

    expect(result.responseCode).toBe(200);
    expect(result.responseData.result).toBe('success');
  });

});