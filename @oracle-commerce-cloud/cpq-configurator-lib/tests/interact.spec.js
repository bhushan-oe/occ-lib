// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.
/* eslint-disable */

const PassThrough = require('stream').PassThrough;
const sinon = require('sinon');

const ConfiguratorServiceHandler = require('../lib/configurator-service-handler');
const MockConfiguratorRestHandler = require('./mocks/mock-configurator-rest-handler');

// Mock data response files.
const cpqInteractResponse = require('./data/interact_responses/cpq-interact-response.json');

// Localized resouce messages.
const localizedResources = require('./data/resource-strings.json');

// Mock logger.
const logger = { debug: msg => {}, info: msg => {}, warning: msg => {}, error: msg => {} };

// Using a sinon sandbox allows stubs etc. to be restored after each test.
let sandbox;

// The ConfiguratorServiceHandler instance to be used in every test.
let handler;

// The request/response that will be used in each test.
let request;
let response;

//------------------------------------------------------------------------------------
/**
 * Tests for the configure.
 */
describe('interact handler', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();
    handler = new ConfiguratorServiceHandler({
      logger: logger
    });

    request = new PassThrough();
    response = new PassThrough();

    request.params = {};
    request.headers = {};
    request.originalUrl = '/ccstorex/custom/v1/configurations';

    request.t = resource => {
      return localizedResources[resource];
    };
  });

  afterEach(function() {
    sandbox.restore();
    request = null;
    response = null;
    handler = null;
  });

  //--------------------------------------------------
  /**
   * Invalid handler initialization.
   */
  it('when ConfiguratorServiceHandler is initialized with no configuration, error is thrown.', () => {
    expect(() => {
      new ConfiguratorServiceHandler();
    }).toThrow(new Error('ConfiguratorServiceHandler requires config to be instantiated'));
  });

  //--------------------------------------------------
  /**
   * Valid handler initialization.
   */
  it('when ConfiguratorServiceHandler is initialized with no configuration, valid handler is created.', () => {
    expect(handler).not.toBe(null);
  });

  //--------------------------------------------------
  /**
   * Invalid cacheInstanceId.
   */
  it('when cacheInstanceId is not present in URI, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'productLine',
      model: 'model',
      configData: {
        attribute123: {
          value: 'sku40002',
          displayValue: 'Intel Pentium i7'
        }
      }
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleInteract(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('cacheInstanceId is required for action.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid cacheInstanceId.
   */
  it('when cacheInstanceId value is not found on external system, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'invalidModel',
      configData: {
        attribute123: {
          value: 'sku40002',
          displayValue: 'Intel Pentium i7'
        }
      }
    };

    request.params = {
      cacheInstanceId: 'juFWNcCAzMYynYGJgVwxE8flSE8x8XNapP3hI3qIsTpjHPoGRbEeVvucON5oFnEa'
    };

    const expectedResultStatusCode = 400;
    const expectedResultMessage =
      'Valid cacheInstanceId is required for action _update to run in current mode.';

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'reject',
        responseCode: expectedResultStatusCode,
        responseData: {
          error: expectedResultMessage
        }
      }
    });

    try {
      await handler.handleInteract(options);
    }
    catch (error) {
      expect(error.status).toBe(expectedResultStatusCode);
      expect(error.message).toBe(expectedResultMessage);
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productFamily.
   */
  it('when productFamily key is present but its value is null, error is thrown.', async () => {
    request.body = {
      productFamily: null,
      productLine: 'productLine1',
      model: 'model1',
      configData: {
        attribute123: {
          value: 'sku40002',
          displayValue: 'Intel Pentium i7'
        }
      }
    };

    request.params = {
      cacheInstanceId: 'juFWNcCAzMYynYGJgVwxE8flSE8x8XNapP3hI3qIsTpjHPoGRbEeVvucON5oFnEa'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleInteract(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productFamily.
   */
  it('when productFamily key is missing, error is thrown.', async () => {
    request.body = {
      productLine: 'productLine1',
      model: 'model1',
      configData: {
        attribute123: {
          value: 'sku40002',
          displayValue: 'Intel Pentium i7'
        }
      }
    };

    request.params = {
      cacheInstanceId: 'juFWNcCAzMYynYGJgVwxE8flSE8x8XNapP3hI3qIsTpjHPoGRbEeVvucON5oFnEa'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleInteract(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productFamily.
   */
  it('when productFamily value is not found on external system, error is thrown.', async () => {
    request.body = {
      productFamily: 'invalidProductFamily',
      productLine: 'productLine1',
      model: 'model1',
      configData: {
        attribute123: {
          value: 'sku40002',
          displayValue: 'Intel Pentium i7'
        }
      }
    };

    request.params = {
      cacheInstanceId: 'juFWNcCAzMYynYGJgVwxE8flSE8x8XNapP3hI3qIsTpjHPoGRbEeVvucON5oFnEa'
    };

    const expectedResultStatusCode = 400;
    const expectedResultMessage = 'Product Family not found for the label invalidProductFamily.';

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'reject',
        responseCode: expectedResultStatusCode,
        responseData: {
          error: expectedResultMessage
        }
      }
    });

    try {
      await handler.handleInteract(options);
    }
    catch (error) {
      expect(error.status).toBe(expectedResultStatusCode);
      expect(error.message).toBe(expectedResultMessage);
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productLine.
   */
  it('when productLine key is present but its value is null, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: '',
      model: 'model1',
      configData: {
        attribute123: {
          value: 'sku40002',
          displayValue: 'Intel Pentium i7'
        }
      }
    };

    request.params = {
      cacheInstanceId: 'juFWNcCAzMYynYGJgVwxE8flSE8x8XNapP3hI3qIsTpjHPoGRbEeVvucON5oFnEa'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleInteract(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productLine.
   */
  it('when productLine key is missing, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      model: 'model1',
      configData: {
        attribute123: {
          value: 'sku40002',
          displayValue: 'Intel Pentium i7'
        }
      }
    };

    request.params = {
      cacheInstanceId: 'juFWNcCAzMYynYGJgVwxE8flSE8x8XNapP3hI3qIsTpjHPoGRbEeVvucON5oFnEa'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleInteract(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid productLine.
   */
  it('when productLine value is not found on external system, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'invalidProductLine',
      model: 'model1',
      configData: {
        attribute123: {
          value: 'sku40002',
          displayValue: 'Intel Pentium i7'
        }
      }
    };

    request.params = {
      cacheInstanceId: 'juFWNcCAzMYynYGJgVwxE8flSE8x8XNapP3hI3qIsTpjHPoGRbEeVvucON5oFnEa'
    };

    const expectedResultStatusCode = 400;
    const expectedResultMessage =
      'The model model1 belonging to the product line invalidProductLine under the segment productFamily1 could not be found.';

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'reject',
        responseCode: expectedResultStatusCode,
        responseData: {
          error: expectedResultMessage
        }
      }
    });

    try {
      await handler.handleInteract(options);
    }
    catch (error) {
      expect(error.status).toBe(expectedResultStatusCode);
      expect(error.message).toBe(expectedResultMessage);
    }
  });

  //--------------------------------------------------
  /**
   * Invalid model.
   */
  it('when model key is present but its value is null, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: '',
      model: '',
      configData: {
        attribute123: {
          value: 'sku40002',
          displayValue: 'Intel Pentium i7'
        }
      }
    };

    request.params = {
      cacheInstanceId: 'juFWNcCAzMYynYGJgVwxE8flSE8x8XNapP3hI3qIsTpjHPoGRbEeVvucON5oFnEa'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleInteract(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid model.
   */
  it('when model key is missing, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      configData: {
        attribute123: {
          value: 'sku40002',
          displayValue: 'Intel Pentium i7'
        }
      }
    };

    request.params = {
      cacheInstanceId: 'juFWNcCAzMYynYGJgVwxE8flSE8x8XNapP3hI3qIsTpjHPoGRbEeVvucON5oFnEa'
    };

    let options = {
      request: request,
      response: response
    };

    try {
      await handler.handleInteract(options);
    }
    catch (error) {
      expect(error.status).toBe(400);
      expect(error.message).toBe('productFamily, productLine and model must be supplied.');
    }
  });

  //--------------------------------------------------
  /**
   * Invalid model.
   */
  it('when model value is not found on external system, error is thrown.', async () => {
    request.body = {
      productFamily: 'productFamily1',
      productLine: 'productLine1',
      model: 'invalidModel',
      configData: {
        attribute123: {
          value: 'sku40002',
          displayValue: 'Intel Pentium i7'
        }
      }
    };

    request.params = {
      cacheInstanceId: 'juFWNcCAzMYynYGJgVwxE8flSE8x8XNapP3hI3qIsTpjHPoGRbEeVvucON5oFnEa'
    };

    const expectedResultStatusCode = 400;
    const expectedResultMessage =
      'The model invalidModel belonging to the product line productLine1 under the segment productFamily1 could not be found.';

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'reject',
        responseCode: expectedResultStatusCode,
        responseData: {
          error: expectedResultMessage
        }
      }
    });

    try {
      await handler.handleInteract(options);
    }
    catch (error) {
      expect(error.status).toBe(expectedResultStatusCode);
      expect(error.message).toBe(expectedResultMessage);
    }
  });

  //--------------------------------------------------
  /**
   * Valid update action.
   */
  it('when interact invocation request is valid, full valid response is returned.', async () => {
    request.body = {
      productFamily: 'laptop',
      productLine: 'laptopConfiguration',
      model: 'sku50001',
      configData: {
        textField1: 'test'
      }
    };

    request.params = {
      cacheInstanceId: 'AtdtPIgTpw54fARGZZATBNcyvRluMFClC1B1mXsbtbmPHUE2zBvEw5PQTALluraU'
    };

    let options = {
      request: request,
      response: response
    };

    handler.requestDispatcher.configuratorRestHandler = new MockConfiguratorRestHandler({
      mockPostResponse: {
        promiseResult: 'resolve',
        responseCode: 200,
        responseData: cpqInteractResponse
      }
    });

    let result = await handler.handleInteract(options);

    expect(result.responseCode).toBe(200);
    expect(result.responseData.configData.textField1).toBe('test');
    expect(result.responseData.configData.links).not.toBeDefined();
    expect(result.responseData.configData._state.actions._update.visible).toBe(true);
    expect(result.responseData.configData._state.actions._integration_addToCart.visible).toBe(false);
    expect(result.responseData.configData._state.actions._next.visible).toBe(true);
    expect(result.responseData.configData._state.actions._previous.visible).toBe(false);
  });

});
