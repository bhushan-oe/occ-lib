// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.
/* eslint-disable */

const PassThrough = require('stream').PassThrough;
const sinon = require('sinon');
const localizedResources = require('./data/resource-strings.json');

// Mock data response files.
const cpqPicklistConfigureResponse = require('./data/link_builder_responses/cpq-pick-list-configure-response.json');
const cpqPicklistLayoutResponse = require('./data/link_builder_responses/cpq-pick-list-layout-response.json');

const LinkBuilder = require('../lib/link-builder');
let linkBuilder;

const constants = require('../lib/constants');
const registry = require('../registry/registry');

// Mock logger.
const logger = { debug: msg => {}, info: msg => {}, warning: msg => {}, error: msg => {} };

const config = {
  'useHTTPS': true,
  'hostname': 'cpq-server',
  'port': '',
  'useHTTPSforPreview': true,
  'previewHostname': '',
  'previewPort': '',
  'timeout': 60000,
  'username_env_var': 'CPQ_USERNAME',
  'password_env_var': 'CPQ_PASSWORD',
  'CONFIG_BASE_URI': '/rest/v8/config',
  'LAYOUT_BASE_URI': '/rest/v8/layoutCache',
  'PAGE_TEMPLATES_BASE_URI': '/rest/v8/pageTemplates',
  'CONFIG_UI_SETTINGS_BASE_URI': '/rest/v8/configUISettings',
  'CONFIGURATOR_SSE_BASE_CONTEXT_URI': '/v1/configurations',
  'registry': registry,
  'logger': logger
}

let request;

//------------------------------------------------------------------------------------
/**
 * Tests for the layout link bulder.
 */
describe('layout link builder', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();

    linkBuilder = new LinkBuilder(config);

    request = new PassThrough();
    response = new PassThrough();

    request.params = {};
    request.headers = {};
    request.body = {};
    request.originalUrl = '/ccstorex/custom/v1/configurations';
    request.t = resource => {
      return localizedResources[resource];
    };
  });

  afterEach(function() {
    sandbox.restore();
    request = null;
  });

  //--------------------------------------------------
  /**
   * Invalid layout.
   */
  it('when layout link is not in the given actionResponse, an exception is thrown.', () => {

    let actionResponse = {
      responseData: {}
    };

    try {
      linkBuilder.buildLayoutLink(request, actionResponse);
    }
    catch (error) {
      expect(error.status).toBe(404);
      expect(error.resourceKey).toBe(constants.FLOW_REQUIRED);
    }
  });

  //--------------------------------------------------
  /**
   * Valid layout.
   */
  it('when layout link is built when no layout exists in the given actionResponse.', () => {

    request.body.productFamily = 'laptop';
    request.body.productLine = 'laptopConfiguration';
    request.body.model = 'sku50001';
    request.body.locale = 'en_US';

    request.originalUrl = '/ccstorex/custom/v1/configurations'

    let actionResponse = {
      responseData: {
        _flow: {
          links: [
            {
              href: 'https://cpq-server/rest/v8/productFamilies/laptop/productLines/laptopConfiguration/models/sku50001/layouts/laptopFlow'
            }
          ]
        }
      }
    };

    linkBuilder.buildLayoutLink(request, actionResponse);

    // 'undefined' in the expected URL would be the http://server:port value for storefront outside the context of unit tests.
    expect('layout').toBe(actionResponse.responseData.links[0].rel);
    expect(actionResponse.responseData.links[0].href)
      .toBe('undefined/ccstorex/custom/v1/configurations/layouts/laptopFlow?productFamily=laptop&productLine=laptopConfiguration&model=sku50001&locale=en_US&hierLevel=3');
  });

  //--------------------------------------------------
  /**
   * Valid layout.
   */
  it('layout and self layout links are built when layout exists in the given actionResponse.', () => {

    request.body.productFamily = 'laptop';
    request.body.productLine = 'laptopConfiguration';
    request.body.model = 'sku50001';
    request.body.locale = 'en_US';

    request.originalUrl = '/ccstorex/custom/v1/configurations'

    let actionResponse = {
      responseData: {
        _flow: {
          links: [
            {
              href: 'https://cpq-server/rest/v8/productFamilies/laptop/productLines/laptopConfiguration/models/sku50001/layouts/laptopFlow'
            }
          ]
        },
        layout: {

        }
      }
    };

    linkBuilder.buildLayoutLink(request, actionResponse);

    // 'undefined' in the expected URL would be the http://server:port value for storefront outside the context of unit tests.
    expect(actionResponse.responseData.links[0].rel).toBe('layout');
    expect(actionResponse.responseData.links[0].href)
      .toBe('undefined/ccstorex/custom/v1/configurations/layouts/laptopFlow?productFamily=laptop&productLine=laptopConfiguration&model=sku50001&locale=en_US&hierLevel=3');
    
    expect(actionResponse.responseData.layout.links[0].rel).toBe('layout');
    expect(actionResponse.responseData.layout.links[0].href)
      .toBe('undefined/ccstorex/custom/v1/configurations/layouts/laptopFlow?productFamily=laptop&productLine=laptopConfiguration&model=sku50001&locale=en_US&hierLevel=3');
  });

});

//------------------------------------------------------------------------------------
/**
 * Tests for the pick list link bulder.
 */
describe('pick list options link builder', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();

    request = new PassThrough();
    response = new PassThrough();

    request.params = {};
    request.headers = {};
    request.body = {};
    request.t = resource => {
      return localizedResources[resource];
    };
  });

  afterEach(function() {
    sandbox.restore();
    request = null;
  });

  //--------------------------------------------------
  /**
   * Valid.
   */
  it('when layout contains picklist components, links are build for those components.', () => {

    request.body.productFamily = 'test1';
    request.body.productLine = 'productLine1';
    request.body.model = 'model1';

    request.originalUrl = '/ccstorex/custom/v1/configurations'

    let actionResponse = {
      responseData: {
        cacheInstanceId: cpqPicklistConfigureResponse.cacheInstanceId,
        configData: cpqPicklistConfigureResponse.configData,
        layout: cpqPicklistLayoutResponse.contents.layout
      }
    };

    linkBuilder.buildPickListOptionsLinks(request, actionResponse);

    // 'undefined' in the expected URL would be the http://server:port value for storefront outside the context of unit tests.
    expect(
      actionResponse.responseData.layout.items.components.items[0].components.items[0].components.items[0].components.items[0].components.items[0].components.items[0].resourceAttributeData.links[0].rel)
      .toBe('pickListItems');
      
    expect(
      actionResponse.responseData.layout.items.components.items[0].components.items[0].components.items[0].components.items[0].components.items[0].components.items[0].resourceAttributeData.links[0].href)
      .toBe('undefined/ccstorex/custom/v1/configurations/DCtMG95PNVdB4A5ikGFm3uIfUSQmF8euO0ESmvDyx0Oz9VQibvI7h8T30EUwtX4p/pick-lists/textPicklist?productFamily=test1&productLine=productLine1&model=model1');
    
    expect(actionResponse.responseData.configData.floatPicklist.links[0].rel).toBe('self');
    expect(actionResponse.responseData.configData.floatPicklist.links[0].href).toBe('undefined/ccstorex/custom/v1/configurations/DCtMG95PNVdB4A5ikGFm3uIfUSQmF8euO0ESmvDyx0Oz9VQibvI7h8T30EUwtX4p/pick-lists/floatPicklist?productFamily=test1&productLine=productLine1&model=model1');
  });

  //--------------------------------------------------
  /**
   * Valid.
   */
  it('when the request is a previous/next page action, the link is created without page in the URL.', () => {

    request.body.productFamily = 'test1';
    request.body.productLine = 'productLine1';
    request.body.model = 'model1';

    request.originalUrl = '/ccstorex/custom/v1/configurations/DCtMG95PNVdB4A5ikGFm3uIfUSQmF8euO0ESmvDyx0Oz9VQibvI7h8T30EUwtX4p/page'

    let actionResponse = {
      responseData: {
        cacheInstanceId: cpqPicklistConfigureResponse.cacheInstanceId,
        configData: cpqPicklistConfigureResponse.configData,
        layout: cpqPicklistLayoutResponse.contents.layout
      }
    };

    linkBuilder.buildPickListOptionsLinks(request, actionResponse);

    // 'undefined' in the expected URL would be the http://server:port value for storefront outside the context of unit tests.
    expect(
      actionResponse.responseData.layout.items.components.items[0].components.items[0].components.items[0].components.items[0].components.items[0].components.items[0].resourceAttributeData.links[0].rel)
      .toBe('pickListItems');
      
    expect(
      actionResponse.responseData.layout.items.components.items[0].components.items[0].components.items[0].components.items[0].components.items[0].components.items[0].resourceAttributeData.links[0].href)
      .toBe('undefined/ccstorex/custom/v1/configurations/DCtMG95PNVdB4A5ikGFm3uIfUSQmF8euO0ESmvDyx0Oz9VQibvI7h8T30EUwtX4p/pick-lists/textPicklist?productFamily=test1&productLine=productLine1&model=model1');
    
    expect(actionResponse.responseData.configData.floatPicklist.links[0].rel).toBe('self');
    expect(actionResponse.responseData.configData.floatPicklist.links[0].href).toBe('undefined/ccstorex/custom/v1/configurations/DCtMG95PNVdB4A5ikGFm3uIfUSQmF8euO0ESmvDyx0Oz9VQibvI7h8T30EUwtX4p/pick-lists/floatPicklist?productFamily=test1&productLine=productLine1&model=model1');
  });

    //--------------------------------------------------
  /**
   * Valid.
   */
  it('when the request URL contains a cacheInstanceId, configData isnt retrieved from configData. This test ensures a duplicate cacheInstanceId is not appended to the URL.', () => {

    request.body.productFamily = 'test1';
    request.body.productLine = 'productLine1';
    request.body.model = 'model1';

    request.originalUrl = '/ccstorex/custom/v1/configurations/DCtMG95PNVdB4A5ikGFm3uIfUSQmF8euO0ESmvDyx0Oz9VQibvI7h8T30EUwtX4p/page'

    let actionResponse = {
      responseData: {
        cacheInstanceId: cpqPicklistConfigureResponse.cacheInstanceId,
        configData: cpqPicklistConfigureResponse.configData,
        layout: cpqPicklistLayoutResponse.contents.layout
      }
    };

    linkBuilder.buildPickListOptionsLinks(request, actionResponse);

    // 'undefined' in the expected URL would be the http://server:port value for storefront outside the context of unit tests.
    expect(
      actionResponse.responseData.layout.items.components.items[0].components.items[0].components.items[0].components.items[0].components.items[0].components.items[0].resourceAttributeData.links[0].rel)
      .toBe('pickListItems');
      
    expect(
      actionResponse.responseData.layout.items.components.items[0].components.items[0].components.items[0].components.items[0].components.items[0].components.items[0].resourceAttributeData.links[0].href)
      .toBe('undefined/ccstorex/custom/v1/configurations/DCtMG95PNVdB4A5ikGFm3uIfUSQmF8euO0ESmvDyx0Oz9VQibvI7h8T30EUwtX4p/pick-lists/textPicklist?productFamily=test1&productLine=productLine1&model=model1');
    
    expect(actionResponse.responseData.configData.floatPicklist.links[0].rel).toBe('self');
    expect(actionResponse.responseData.configData.floatPicklist.links[0].href).toBe('undefined/ccstorex/custom/v1/configurations/DCtMG95PNVdB4A5ikGFm3uIfUSQmF8euO0ESmvDyx0Oz9VQibvI7h8T30EUwtX4p/pick-lists/floatPicklist?productFamily=test1&productLine=productLine1&model=model1');
  });

});
