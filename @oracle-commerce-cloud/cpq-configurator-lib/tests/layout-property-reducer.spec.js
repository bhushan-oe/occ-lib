// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.
/* eslint-disable */

const PassThrough = require('stream').PassThrough;
const sinon = require('sinon');
const propertyReducer = require('../lib/layout-property-reducer');

//------------------------------------------------------------------------------------
/**
 * Tests for the next.
 */
describe('assemble payload', function() {
  beforeEach(function() {
    sandbox = sinon.createSandbox();

    request = new PassThrough();
    response = new PassThrough();

    request.body = {};
    request.query = {};
    request.originalUrl = '/ccstorex/custom/v1/configurations';
  });

  afterEach(function() {
    sandbox.restore();
    request = null;
  });

  //--------------------------------------------------
  it('when exclude properties list is supplied, those properties are removed from layout items.', () => {
    
    let layoutCache = {
      responseData: {
        contents: {
          layout: {
            "varName": "flow1",
            "items": {
              "id": 38549146,
              "dateModified": null,
              "language": null,
              "components": {
                "items": [
                  {
                    'one': 1,
                    'two': 2,
                    'three': 3,
                    'four': 4,
                    'five': 5
                  },
                  {
                    'one': 1,
                    'two': 2,
                    'three': 3,
                    'four': 4,
                    'five': 5,
                    'components': {
                      'items': [
                        {
                          'one': 1,
                          'two': 2,
                          'three': 3,
                          'four': 4,
                          'five': 5
                        }
                      ]
                    }
                  }
                ]
              }
            }
          }
        }
      }
    };

    request.body.excludeLayoutProperties = ['one', 'three', 'five'];
    propertyReducer.reduceLayoutProperties(request, layoutCache);

    expect(layoutCache.responseData.contents.layout.items.components.items[0]).toEqual({ 'two': 2, 'four': 4 });
    expect(layoutCache.responseData.contents.layout.items.components.items[1]).toEqual({
      'two': 2, 
      'four': 4, 
      'components': {
        'items': [
          {
            'two': 2,
            'four': 4,
          }
        ]
      }
    });
  });

  //--------------------------------------------------
  it('when include properties list is supplied, only those properties are included layout items.', () => {
    
    let layoutCache = {
      responseData: {
        contents: {
          layout: {
            "varName": "flow1",
            "items": {
              "id": 38549146,
              "dateModified": null,
              "language": null,
              "components": {
                "items": [
                  {
                    'one': 1,
                    'two': 2,
                    'three': 3,
                    'four': 4,
                    'five': 5
                  },
                  {
                    'one': 1,
                    'two': 2,
                    'three': 3,
                    'four': 4,
                    'five': 5,
                    'components': {
                      'items': [
                        {
                          'one': 1,
                          'two': 2,
                          'three': 3,
                          'four': 4,
                          'five': 5
                        }
                      ]
                    }
                  }
                ]
              }
            }
          }
        }
      }
    };

    request.body.includeLayoutProperties = ['one', 'three', 'five', 'components'];
    propertyReducer.reduceLayoutProperties(request, layoutCache);

    expect(layoutCache.responseData.contents.layout.items.components.items[0])
      .toEqual({ 'one': 1, 'three': 3, 'five': 5 });
    
    expect(layoutCache.responseData.contents.layout.items.components.items[1]).toEqual(
      {
        'one': 1,
        'three': 3,
        'five': 5,
        'components': {
          'items': [
            {
              'one': 1,
              'three': 3, 
              'five': 5
            }
          ]
        }
      }
    );

  });

  //--------------------------------------------------
  it('when null properties list is supplied, layout item properties are not affected.', () => {
    
    let layoutCache = {
      responseData: {
        contents: {
          layout: {
            "varName": "flow1",
            "items": {
              "id": 38549146,
              "dateModified": null,
              "language": null,
              "components": {
                "items": [
                  {
                    'one': 1,
                    'two': 2,
                    'three': 3,
                    'four': 4,
                    'five': 5
                  }
                ]
              }
            }
          }
        }
      }
    };

    propertyReducer.reduceLayoutProperties(request, layoutCache);

    expect(layoutCache.responseData.contents.layout.items.components.items[0])
      .toEqual({ 'one': 1, 'two': 2, 'three': 3, 'four': 4, 'five': 5 });
  });

});
