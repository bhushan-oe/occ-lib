// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.

'use strict';

// OCC Package constants.
const CPQ_CONFIGURATOR_LIB_PACKAGE = 'cpq-configurator-lib';
const CPQ_CONFIGURATOR_REST_PACKAGE = 'cpq-configurator-rest';
const CPQ_DATA_TRANSFORM_UTILS_LIB_PACKAGE = 'cpq-data-transform-utils-lib';

// cpq-configurator-lib module constants
const CPQ_CONFIGURATOR_LIB_MODULE_CONSTANTS = 'constants';
const CPQ_CONFIGURATOR_LIB_MODULE_UTILS = 'utils';
const CPQ_CONFIGURATOR_LIB_MODULE_VALIDATORS = 'validators';
const CPQ_CONFIGURATOR_LIB_MODULE_PAYLOAD_ASSEMBLER = 'payloadAssembler';
const CPQ_CONFIGURATOR_LIB_MODULE_CONFIGURATOR_SERVICE_HANDLER = 'configuratorServiceHandler';
const CPQ_CONFIGURATOR_LIB_MODULE_CPQ_REQUEST_DISPATCHER = 'cpqRequestDispatcher';
const CPQ_CONFIGURATOR_LIB_MODULE_LINK_BUILDER = 'linkBuilder';
const CPQ_CONFIGURATOR_LIB_MODULE_LAYOUT_PROPERTY_REDUCER = 'layoutPropertyReducer';

// cpq-configurator-rest module constants.
const CPQ_CONFIGURATOR_REST_MODULE_CONFIGURATOR_REST_HANDLER = 'configuratorRestHandler';

// cpq-occ-attribute-mapper-lib module constants.
const CPQ_OCC_TRANSFORM_UTILS_LIB_MODULE_CPQ_OCC_LINE_ITEM_MAPPER = 'cpqOccLineItemMapper';

/**
 * This class defines the default modules that will be used throughout the
 * cpq-configurator-lib.
 *
 * When any configurator modules have been extended, this file must also be extended
 * to reference the extended module(s) locations.
 */
class Registry {

  // ------------------------------------------------------------
  // CONSTANTS
  // ------------------------------------------------------------

  // Packages
  static get CPQ_CONFIGURATOR_LIB_PACKAGE () {
    return CPQ_CONFIGURATOR_LIB_PACKAGE;
  }
  static get CPQ_CONFIGURATOR_REST_PACKAGE () {
    return CPQ_CONFIGURATOR_REST_PACKAGE;
  }
  static get CPQ_DATA_TRANSFORM_UTILS_LIB_PACKAGE () {
    return CPQ_DATA_TRANSFORM_UTILS_LIB_PACKAGE;
  }

  // cpq-configurator-lib modules
  static get CPQ_CONFIGURATOR_LIB_MODULE_CONSTANTS () {
    return CPQ_CONFIGURATOR_LIB_MODULE_CONSTANTS;
  }
  static get CPQ_CONFIGURATOR_LIB_MODULE_UTILS () {
    return CPQ_CONFIGURATOR_LIB_MODULE_UTILS;
  }
  static get CPQ_CONFIGURATOR_LIB_MODULE_VALIDATORS () {
    return CPQ_CONFIGURATOR_LIB_MODULE_VALIDATORS;
  }
  static get CPQ_CONFIGURATOR_LIB_MODULE_PAYLOAD_ASSEMBLER () {
    return CPQ_CONFIGURATOR_LIB_MODULE_PAYLOAD_ASSEMBLER;
  }
  static get CPQ_CONFIGURATOR_LIB_MODULE_CONFIGURATOR_SERVICE_HANDLER () {
    return CPQ_CONFIGURATOR_LIB_MODULE_CONFIGURATOR_SERVICE_HANDLER;
  }
  static get CPQ_CONFIGURATOR_LIB_MODULE_CPQ_REQUEST_DISPATCHER () {
    return CPQ_CONFIGURATOR_LIB_MODULE_CPQ_REQUEST_DISPATCHER;
  }
  static get CPQ_CONFIGURATOR_LIB_MODULE_LINK_BUILDER () {
    return CPQ_CONFIGURATOR_LIB_MODULE_LINK_BUILDER;
  }
  static get CPQ_CONFIGURATOR_LIB_MODULE_LAYOUT_PROPERTY_REDUCER () {
    return CPQ_CONFIGURATOR_LIB_MODULE_LAYOUT_PROPERTY_REDUCER;
  }

  // cpq-configurator-rest modules
  static get CPQ_CONFIGURATOR_REST_MODULE_CONFIGURATOR_REST_HANDLER () {
    return CPQ_CONFIGURATOR_REST_MODULE_CONFIGURATOR_REST_HANDLER;
  }

  // cpq-occ-attribute-mapper-lib modules
  static get CPQ_OCC_TRANSFORM_UTILS_LIB_MODULE_CPQ_OCC_LINE_ITEM_MAPPER () {
    return CPQ_OCC_TRANSFORM_UTILS_LIB_MODULE_CPQ_OCC_LINE_ITEM_MAPPER;
  }

  // ------------------------------------------------------------
  // FUNCTIONS
  // ------------------------------------------------------------

  /**
   * This function returns a required module.
   *
   * @param {String} configuratorModule
   *   The name of the configurator module to be required.
   * @param {String} configuratorPackage
   *   The name of the configurator package where the module belongs.
   */
  static load (configuratorModule, configuratorPackage) {
    /* eslint-disable global-require */
    if (configuratorPackage === CPQ_CONFIGURATOR_LIB_PACKAGE) {
      switch (configuratorModule) {
      case CPQ_CONFIGURATOR_LIB_MODULE_CONSTANTS:
        return require('../lib/constants');
      case CPQ_CONFIGURATOR_LIB_MODULE_UTILS:
        return require('../lib/utils');
      case CPQ_CONFIGURATOR_LIB_MODULE_VALIDATORS:
        return require('../lib/validators');
      case CPQ_CONFIGURATOR_LIB_MODULE_PAYLOAD_ASSEMBLER:
        return require('../lib/payload-assembler');
      case CPQ_CONFIGURATOR_LIB_MODULE_CONFIGURATOR_SERVICE_HANDLER:
        return require('../lib/configurator-service-handler');
      case CPQ_CONFIGURATOR_LIB_MODULE_CPQ_REQUEST_DISPATCHER:
        return require('../lib/cpq-request-dispatcher');
      case CPQ_CONFIGURATOR_LIB_MODULE_LINK_BUILDER:
        return require('../lib/link-builder');
      case CPQ_CONFIGURATOR_LIB_MODULE_LAYOUT_PROPERTY_REDUCER:
        return require('../lib/layout-property-reducer');
      }
    }
    else if (configuratorPackage === CPQ_CONFIGURATOR_REST_PACKAGE) {
      if (configuratorModule === CPQ_CONFIGURATOR_REST_MODULE_CONFIGURATOR_REST_HANDLER) {
        return require(`@oracle-commerce-cloud/${CPQ_CONFIGURATOR_REST_PACKAGE}`).ConfiguratorRestHandler;
      }
    }
    else if (configuratorPackage === CPQ_DATA_TRANSFORM_UTILS_LIB_PACKAGE) {
      if (configuratorModule === CPQ_OCC_TRANSFORM_UTILS_LIB_MODULE_CPQ_OCC_LINE_ITEM_MAPPER) {
        return require(`@oracle-commerce-cloud/${CPQ_DATA_TRANSFORM_UTILS_LIB_PACKAGE}`).CpqOccLineItemMapper;
      }
    }
    /* eslint-enable global-require */
  }
}

module.exports = Registry;
