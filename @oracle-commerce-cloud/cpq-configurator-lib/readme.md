## Introduction
This is a server-side extension library that contains functionality to invoke the CPQ 
Configurator REST API and return responses that can easily be consumed by an OCC client.

## Prerequisites
* Oracle Commerce Cloud (Minimum version: 19B)

## Extending @oracle-commerce-cloud/cpq-configurator-lib
Classes exported from `@oracle-commerce-cloud/cpq-configurator-lib` can be overwritten in an 
application layer to provide the behavior required. `@oracle-commerce-cloud/cpq-configurator-lib` 
should not be  modified directly.