// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.

'use strict';

const ConfiguratorServiceHandler = require('./lib/configurator-service-handler');
const CpqRequestDispatcher = require('./lib/cpq-request-dispatcher');
const constants = require('./lib/constants');
const payloadAssembler = require('./lib/payload-assembler');
const layoutPropertyReducer = require('./lib/layout-property-reducer');
const utils = require('./lib/utils');
const linkBuilder = require('./lib/link-builder');
const validators = require('./lib/validators');

const registry = require('./registry/registry');

module.exports = {
  ConfiguratorServiceHandler,
  CpqRequestDispatcher,
  constants,
  payloadAssembler,
  layoutPropertyReducer,
  utils,
  linkBuilder,
  validators,
  registry
};
