// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.

'use strict';

const defaultRegistry = require('../registry/registry');

/**
 * This class contains a number of general utility functions to be
 * used throughout the configurator library.
 */
class Utils {

  /**
   * Constructor
   *
   * @param {Object} config
   *   An object that contains the configuration settings for the application.
   * @param {Boolean} config.useHTTPS 
   *   Whether or not to use the https protocol for outbound requests.
   * @param {String} config.hostname 
   *   External CPQ Configurator server hostname.
   * @param {String} config.port 
   *   External CPQ Configurator server port.
   * @param {Boolean} config.useHTTPSforPreview 
   *   Whether or not to use the https protocol for outbound preview requests.
   * @param {String} config.previewHostname 
   *   External CPQ Configurator preview server hostname.
   * @param {String} config.previewPort 
   *   External CPQ Configurator preview server port.
   * @param {Number} config.timeout 
   *   Request timeout in ms.
   * @param {Object} config.logger 
   *   Used for logging.
   * @param {String} config.username_env_var 
   *   Environment variable name for basic auth username.
   * @param {String} config.password_env_var 
   *   Environment variable name for basic auth password.
   * @param {String} config.CONFIG_BASE_URI
   *   The CPQ 'config' base REST URI.
   * @param {String} config.LAYOUT_BASE_URI
   *   The CPQ 'layoutCache' base REST URI.
   * @param {String} config.PAGE_TEMPLATES_BASE_URI
   *   The CPQ 'pageTemplates' base REST URI.
   * @param {String} config.CONFIG_UI_SETTINGS_BASE_URI
   *   The CPQ 'configUISettings' base REST URI.
   * @param {String} config.CONFIGURATOR_SSE_BASE_CONTEXT_URI
   *   The base URI of the SSE Configurator application.
   * @param {Object} config.registry
   *   The registry that contains the module mappings for the SSE configurator. (optional)
   */
  constructor (config) {
    if (!config) {
      throw new Error('Utils requires config to be instantiated');
    }

    this.config = config;

    this.registry = config.registry ? config.registry : defaultRegistry;

    this.constants = this.registry.load(
      this.registry.CPQ_CONFIGURATOR_LIB_MODULE_CONSTANTS,
      this.registry.CPQ_CONFIGURATOR_LIB_PACKAGE);
  }

  /**
   * This function removes any _state actions that that are not supported by OCC.
   *
   * @param {Object} configData
   *   The configData object that contains the actions available for the configuration session.
   */
  removeUnsupportedActions (configData) {
    if (configData._state && configData._state.actions) {
      const validActions = {};

      Object.keys(configData._state.actions).forEach(action => {
        const isActionSupported = this.constants.SUPPORTED_ACTIONS.includes(action);

        if (isActionSupported) {
          validActions[action] = configData._state.actions[action];
        }
      });

      configData._state.actions = validActions;
    }
  }

  /**
   * This function creates a new configData object containing only 'updateable' attributes.
   * It handles direct child attributes of the configData object as well as array set attributes.
   *
   * @param {Object} configData
   *   The object that contains the _state.attributes data and the updated attributes.
   *
   * @returns {Object}
   *   A new configData object that contains only updatable attributes.
   */
  removeNonUpdatableAttributes (configData) {
    const configDataItems = this._removeNonUpdatableConfigDataAttributes(configData);
    const configDataArraySets = this._removeNonUpdatableArraySetAttributes(configData);

    return {...configDataItems, ...configDataArraySets};
  }

  /**
   * This function merges attribute data into the corresponding layout components.
   *
   * @param {Object} layoutCacheResponse
   *   The object that contains the layout attributes.
   * @param {Object} allAttributesList
   *   All attributes available for a particular product.
   * @param {Object} arraySetsData
   *   The object containing all arraySet data.
   * @param {Array} pickListOptions
   *   A list of all the available pickList options.
   */
  mergeDataToLayoutComponents (layoutCacheResponse, allAttributesList, arraySetsData, pickListOptions) {

    // Reducer function to create a lookup map object from an array of objects.
    const arrayItemsToMapReducer = (map, currentValue) => {
      map[currentValue.variableName] = currentValue;
      return map;
    };

    // Create a map to look up attributes by id.
    const attributesLookupMap = allAttributesList.reduce(arrayItemsToMapReducer, {});
    const pickListOptionsMap = pickListOptions ? pickListOptions.reduce(arrayItemsToMapReducer, {}) : null;

    const addResourceAttributeData = item => {

      this._addResourceAttributeData(item, attributesLookupMap);
      this._addPickListItemsToResourceAttributeData(item, pickListOptionsMap);
      this._addColumnWidthsForGridSystem(item);
      this._setArraySetSizeAttrVarName(item, arraySetsData);
      this._populateImageUrlsWithCpqServerDetails(item);

      // Recursive call to add resource attribute data to child components.
      if (item.components && item.components.items) {
        item.components.items.forEach(addResourceAttributeData);

        // The sequence of components in the array won't necessarily match the
        // components sequence numbers so sort the array to match.
        this._sortComponents(item.components.items);
      }

      return item;
    };

    // Add attribute data to corresponding layout control.
    layoutCacheResponse.responseData.contents.layout.items.components.items.forEach(
      addResourceAttributeData
    );
  }

  /**
   * This function will remove any CPQ specific properties it finds in the given response object.
   *
   * @param {Object} response
   *   The response object that will have CPQ specific properties removed.
   * @param {Boolean} preserveTopLevelLinks  
   *   When false, any 'links' properties will be removed from the top level of the response.
   * 
   * @returns {Object}
   *   The new response object that has CPQ specific properties removed.
   */
  removeCpqSpecificProperties (response, preserveTopLevelLinks) {
    const newResponse = {
      responseData: {},
      responseCode: response.responseCode
    };

    Object.keys(response.responseData).forEach(key => {
      if (key !== '_flow') {
        newResponse.responseData[key] = response.responseData[key];
      }
    });

    // Remove any CPQ links as they will not make sense to the client.
    if (newResponse.responseData.configData && newResponse.responseData.configData.links) {
      delete newResponse.responseData.configData.links;
    }

    if (newResponse.responseData.links && !preserveTopLevelLinks) {
      delete newResponse.responseData.links;
    }

    return newResponse;
  }

  /**
   * This function will return the locale associated to the request, otherwise a default.
   *
   * @param {Object} request
   *   The request that the locale will be retrieved from.
   *
   * @returns {String}
   *   The locale retrieved from the request, otherwise a default.
   */
  getLocale (request) {
    return request.body.locale ||
           request.query.locale ||
           request.headers[this.constants.LOCALE_HEADER_NAME] ||
           this.constants.DEFAULT_LOCALE;
  }

  /**
   * This function will extract any 'expand' values from the request and add them to an array..
   *
   * @param {Object} request
   *   The request that the 'expand' parameters will be retrieved from.
   *
   * @returns {Array}
   *   An expand parameters array, otherwise null.
   */
  getExpandParameters (request) {

    // Ensure that all whitespace is removed from expand list.
    const expand = request.body.expand ? request.body.expand.replace(/ +?/g, '') : null;
    const expandParameters = expand ? expand.split(',') : null;

    return expandParameters;
  }

  /**
   * So that the client can consume the layout contents more easily, merge all 'items' arrays from each
   * of the objects into a single array.
   *
   * @param {Object} layoutCache
   *   The layoutCache that will have its arraySets merged into a single array.
   * @param {String} contentsProperty
   *   The name of the property that will be reduced.
   *
   * @return {Array}
   *   A single array with all of the elements contained in the contentsProperty.
   */
  reduceLayoutCacheContents (layoutCache, contentsProperty) {
    return layoutCache.responseData.contents[contentsProperty].reduce((list, currentValue) => {
      return list.concat(currentValue.items);
    }, []);
  }

  /**
   * Retrieve layout flow details from a configuration action response.
   *
   * These details can then be used to retrieve the layoutCache for a particular configuration.
   *
   * @param {Object} actionResponse
   *   The configuration action response that contains the layout flow detils to be extracted.
   *
   * @returns {Object}
   *   The layout flow details from the actionResponse.
   */
  getLayoutFlowDetails (actionResponse) {
    const layoutFlowHref = actionResponse.responseData._flow.links[0].href;
    const layoutFlowUrlSplitArr = layoutFlowHref.split('/');
    const layoutFlowVariableName = layoutFlowUrlSplitArr[layoutFlowUrlSplitArr.length - 1];

    // The hierLevel value needs to be set to the level in the hierarchy that the layoutFlow was created.
    // For example, if there is only a productFamily, the value will be 1. If the there is a productFamily
    // and productLine but no model, the value will be 2. If there is a productFamily, productLine and model,
    // the value will be 3.
    let layoutCacheHierarchyLevel = 1;

    if (layoutFlowHref.includes('/productLines/')) {
      layoutCacheHierarchyLevel = layoutFlowHref.includes('/models/') ? 3 : 2;
    }

    return {
      flow: layoutFlowVariableName,
      hierLevel: layoutCacheHierarchyLevel
    };
  }

  /**
   * Search through a nested collection of layout components and return the variable name
   * of any array set and/or single pick list component
   *
   * @param {Object} items
   *   The list of layout components to be searched
   */
  getArraySetAndPickListIds (items) {
    const returnValue = {
      arraySetVariableNames: [],
      pickListResourceAttributeVarNames: []
    };

    for (let index = 0; index < items.length; index++) {
      const element = items[index];

      if (element.componentTypeCode.lookupCode === 'ARRAY_SET') {
        returnValue.arraySetVariableNames.push(element.resourceVariableName.variableName);
      }
      else if (element.componentTypeCode.lookupCode === 'FORM_CONTROL' &&
          element.inputTypeCode.lookupCode === 'SingleSelectPickList') {

        returnValue.pickListResourceAttributeVarNames.push(element.resourceAttributeVarName);
      }

      if (element.components && element.components.items) {
        const childReturnValue = this.getArraySetAndPickListIds(element.components.items);

        returnValue.arraySetVariableNames =
          returnValue.arraySetVariableNames.concat(childReturnValue.arraySetVariableNames
          );

        returnValue.pickListResourceAttributeVarNames =
          returnValue.pickListResourceAttributeVarNames.concat(childReturnValue.pickListResourceAttributeVarNames);
      }
    }

    return returnValue;
  }

  /**
   * Retrieve arraySet data associated with a layoutCache.
   *
   * @param {Array} layoutCacheAttributes
   *   Attributes that are associated with a particular layoutCache.
   * @param {Array} layoutCacheArraySets
   *   ArraySets that are associated with a particular layoutCache.
   * @param {Array} arraySetVariableNames
   *   An array of of arraySet variable names.
   * @returns {Object}
   *   An object that contains all arraySets with their corresponding data.
   */
  getArraySetData (layoutCacheArraySets, arraySetVariableNames) {
    const arraySetsData = {};

    if (arraySetVariableNames.length) {
      for (let i = 0, len = layoutCacheArraySets.length; i < len; i++) {
        const arraySetVariableName = layoutCacheArraySets[i].variableName;

        if (!arraySetVariableName.includes('array_key')) {
          arraySetsData[arraySetVariableName] = {
            sizeAttrVarName: layoutCacheArraySets[i].sizeAttrVarName
          };
        }
      }
    }

    return arraySetsData;
  }

  /**
   * When arraySets exist, append them to the given layoutCache attributes list.
   *
   * @param {Array} layoutCacheAttributes
   *   Attributes that are associated with a particular layoutCache.
   * @param {Array} layoutCacheArraySets
   *   ArraySets that are associated with a particular layoutCache.
   * @param {Array} arraySetVariableNames
   *   An array of of arraySet variable names.
   */
  addArraySetsToLayoutCacheAttributes (
    layoutCacheAttributes, layoutCacheArraySets, arraySetVariableNames) {

    if (arraySetVariableNames.length) {
      for (let i = 0, len = layoutCacheArraySets.length; i < len; i++) {
        const arraySetVariableName = layoutCacheArraySets[i].variableName;

        if (!arraySetVariableName.includes('array_key')) {
          layoutCacheAttributes = layoutCacheAttributes.concat(layoutCacheArraySets[i].attributes.items);
        }
      }
    }

    return layoutCacheAttributes;
  }

  /**
   * Add all arraySet variable names to the given actionResponse.
   *
   * @param {Object} arraySetsData
   *   All arraySet variable names will be retrieved from this object.
   * @param {Object} actionResponse
   *   The actionResponse object that the arraySet variable names will be added to.
   */
  addArraySetsVariableNamesToConfiguration (arraySetsData, actionResponse) {
    const arraySetVariableNames = [];

    Object.keys(arraySetsData).forEach(key => {
      arraySetVariableNames.push(`_set${key}`);
    });

    actionResponse.responseData.arraySetVariableNames = arraySetVariableNames;
  }

  /**
   * Retrieve pick list options when they exist for a configuration.
   *
   * @param {Object} request
   *   The request object for the action being performed.
   * @param {Object} response
   *   The response object for the action being performed.
   * @param {Object} requestDispatcher
   *   A request dispatcher that will be used to invoke the endpoint to retrieve the pick list options.
   * @param {String} cacheInstanceId
   *   The cacheInstanceId of the configuration to retrieve pick list options from.
   * @param {Object} pickListResourceAttributeVarNames
   *   Object that contains all pickListResourceAttributeVarNames.
   *
   * @returns {Array}
   *   A list of pick list options.
   */
  async getPickListOptions (request, requestDispatcher, cacheInstanceId, pickListResourceAttributeVarNames) {
    let pickListOptions = [];

    if (request.body.populatePickListItems && pickListResourceAttributeVarNames.length) {
      const pickListGetOptionsResponse = await requestDispatcher.doPickListGetOptions(request, cacheInstanceId);
      pickListOptions = pickListGetOptionsResponse.responseData.items;
    }

    return pickListOptions;
  }

  // ------------------------------------------------------------
  // PRIVATE FUNCTIONS
  // ------------------------------------------------------------

  /**
   * This module contains utility functions that are common for action update handlers.
   */

  /**
   * Sort items according to their corresponding 'sequence' property number.
   *
   * @private
   *
   * @param {Array} items
   *   The items that will be sorted.
   */
  _sortComponents (items) {
    items.sort((a, b) => a.sequence - b.sequence);
  }

  /**
   * Calculates the value for a 12 column grid system from the item.columnWidths property returned
   * from CPQ. This value is then added to a columnWidthsList property on the item.
   *
   *  Note this only supports PERCENTAGE_WIDTH.
   *
   * @private
   *
   * @param {Object} item
   *   The item that contains the columnWidths property.
   */
  _addColumnWidthsForGridSystem (item) {
    if (item.columnWidths) {
      item.columnWidthsList = item.columnWidths.split('~').map(width => {
        return {
          width,
          colsFor12ColGridSystem: Math.round((width / 100) * 12)
        };
      });
    }
  }

  /**
   * Iterates through all attributes in a given configData object and only returns
   * those attributes that are set as updateable.
   *
   * @private
   *
   * @param {Object} configData
   *   The configData object that contains the available attributes.
   *
   * @returns {Array}
   *   An array of updateable attributes.
   */
  _getUpdatableAttributes (configData) {
    const returnValue = [];

    for (const [key, value] of Object.entries(configData._state.attributes)) {
      if (value.updatable) {
        returnValue.push(key);
      }
    }

    return returnValue;
  }

  /**
   * Iterates through all arraySet specific attributes in a given configData object
   * and only returns those arraySet attributes that are set as updateable.
   *
   * @private
   *
   * @param {Object} configData
   *   The configData object that contains the available arraySet attributes.
   */
  _getAllArraySetUpdateAttributes (configData) {

    const configDataContainsArraySetAttributeData = (configDataKey, configDataValue) => {
      return (
        configDataKey.includes(this.constants.ARRAY_SET_PREFIX) &&
        configDataValue._state.defaultRowState &&
        configDataValue._state.defaultRowState.attributes
      );
    };

    const getArraySetUpdatableAttributeIds = arraySetAttributes => {
      const returnValue = [];

      for (const [key, value] of Object.entries(arraySetAttributes)) {
        if (value.updatable) {
          returnValue.push(key);
        }
      }

      return returnValue;
    };


    const returnValue = [];

    for (const [key, value] of Object.entries(configData)) {
      if (configDataContainsArraySetAttributeData(key, value)) {
        returnValue.push({
          variableName: key,
          updatableAttributes: getArraySetUpdatableAttributeIds(value._state.defaultRowState.attributes)
        });
      }
    }

    return returnValue;
  }

  /**
   * Removes any arraySet attributes that are not set as updateable.
   *
   * @private
   *
   * @param {Object} configData
   *   The configData object that contains the available arraySet attributes.
   */
  _removeNonUpdatableArraySetAttributes (configData) {

    const getArraySetItem = (arraySetConfigDataItem, arraySetUpdatableAttributes) => {
      let returnValue = {};
      returnValue._index = arraySetConfigDataItem._index;

      for (let index = 0; index < arraySetUpdatableAttributes.length; index++) {
        const attributeName = arraySetUpdatableAttributes[index];
        const currentObject = this._correctConfigDataItemFormat(
          arraySetConfigDataItem[attributeName],
          attributeName
        );
        returnValue = {...returnValue, ...currentObject};
      }

      return returnValue;
    };

    const getArraySetUpdatableConfigDataItems = (arraySetConfigDataItems, arraySetUpdatableAttributes) => {
      const returnValue = [];

      for (let index = 0; index < arraySetConfigDataItems.length; index++) {
        const newArraySetConfigDataItem =
          getArraySetItem(arraySetConfigDataItems[index], arraySetUpdatableAttributes);

        returnValue.push(newArraySetConfigDataItem);
      }
      return returnValue;
    };

    const allArraySetUpdatableAttributes = this._getAllArraySetUpdateAttributes(configData);
    const returnValue = {};

    for (let index = 0; index < allArraySetUpdatableAttributes.length; index++) {
      const arraySetVariableName = allArraySetUpdatableAttributes[index].variableName;
      const arraySetUpdatableAttributes = allArraySetUpdatableAttributes[index].updatableAttributes || [];
      const arraySetConfigDataItems = configData[arraySetVariableName].items;

      const newArraySetConfigDataItems =
        getArraySetUpdatableConfigDataItems(arraySetConfigDataItems, arraySetUpdatableAttributes);

      returnValue[arraySetVariableName] = {
        items: newArraySetConfigDataItems
      };
    }

    return returnValue;
  }

  /**
   * Ensures that all configData attributes are in the correct format to be consumed by the client.
   *
   * @private
   *
   * @param {Object} currentAttributeValue
   *   The current attribute value being processed.
   * @param {Object} attributeName
   *   The current attribute name being processed.
   *
   * @returns {Object}
   *   The correctly formatted configData item.
   */
  _correctConfigDataItemFormat (currentAttributeValue, attributeName) {
    const getCurrencyConfigData = configDataItem => {
      return {
        value: configDataItem.value,
        currency: configDataItem.currency
      };
    };

    const getStandardConfigData = configDataItem => {
      return {
        value: configDataItem.value,
        displayValue: configDataItem.value
      };
    };

    const getItemsConfigData = configDataItem => {
      return {
        items: configDataItem.items.map(item => {
          return {value: item.value};
        })
      };
    };

    const returnValue = {};

    if (currentAttributeValue && currentAttributeValue.hasOwnProperty('value')) {
      if (currentAttributeValue.hasOwnProperty('currency')) {
        returnValue[attributeName] = getCurrencyConfigData(currentAttributeValue);
      }
      else {
        returnValue[attributeName] = getStandardConfigData(currentAttributeValue);
      }
    }
    else if (currentAttributeValue && currentAttributeValue.hasOwnProperty('items')) {
      returnValue[attributeName] = getItemsConfigData(currentAttributeValue);
    }
    else {
      returnValue[attributeName] = currentAttributeValue;
    }

    return returnValue;
  }

  /**
   * Iterates through all attributes in a given configData object
   * and only returns those attributes that are set as updateable.
   *
   * @private
   *
   * @param {Object} configData
   *   The configData object that contains the available attributes.
   *
   * @returns {Object}
   *   configData object containing only updatable attributes.
   */
  _removeNonUpdatableConfigDataAttributes (configData) {
    const updatableAttributes = this._getUpdatableAttributes(configData);

    let returnValue = {};

    updatableAttributes.forEach(function (attributeName) {
      const currentObject = this._correctConfigDataItemFormat(configData[attributeName], attributeName);
      returnValue = {...returnValue, ...currentObject};
    }, this);

    return returnValue;
  }

  /**
   * Update any imageUrl properties with the CPQ server details.
   *
   * @private
   *
   * @param {Object} item
   *   The item that contains imageUrls to be updated.
   * @param {Object} config
   *   A configuration object that holds the CPQ server details.
   */
  _populateImageUrlsWithCpqServerDetails (item) {
    if (item.resourceAttributeData && item.resourceAttributeData.menuItems &&
        item.resourceAttributeData.menuItems.items && item.resourceAttributeData.menuItems.items.length > 0) {

      const httpProtocol = this.config.useHTTPS ? 'https://' : 'http://';

      const cpqServer = this.config.port 
        ? `${httpProtocol}${this.config.hostname}:${this.config.port}` 
        : `${httpProtocol}${this.config.hostname}`;

      item.resourceAttributeData.menuItems.items.forEach(menuItem => {
        if (menuItem.imageUrl) {
          menuItem.imageUrl = `${cpqServer}${menuItem.imageUrl}`;
        }
      });
    }
  }

  /**
   * Set a sizeAttrVarName property on an item if the item is an arraySet attribute.
   *
   * @param {*} item
   *   The item that will be updated with a sizeAttrVarName property.
   * @param {*} arraySetsData.
   *   List of arraySets that hold their corresponding sizeAttrVarName values.
   */
  _setArraySetSizeAttrVarName (item, arraySetsData) {
    if (item.resourceVariableName && item.resourceVariableName.variableName) {
      if (arraySetsData[item.resourceVariableName.variableName]) {
        item.sizeAttrVarName =
          arraySetsData[item.resourceVariableName.variableName].sizeAttrVarName;
      }
    }
  }

  /**
   * When a given item has a resourceAttributeVarName but doesn't have a
   * resourceAttributeData object, create one.
   *
   * @param {*} item
   *   The item that the resourceAttributeData object will be added to.
   * @param {*} attributesLookupMap
   *   The resourceAttributeData will only be created when a resourceAttributeVarName
   *   name exists in this object.
   */
  _addResourceAttributeData (item, attributesLookupMap) {
    if (item.resourceAttributeVarName && !item.resourceAttributeData) {
      item.resourceAttributeData = attributesLookupMap[item.resourceAttributeVarName];
    }
  }

  /**
   * Add a pickListItems property with all available pickList options for a particular item.
   *
   * @param {*} item
   *   The item that the pickList options will be added to.
   * @param {*} pickListOptionsMap
   *   Map containing all available pickList options.
   */
  _addPickListItemsToResourceAttributeData (item, pickListOptionsMap) {
    if (item.resourceAttributeData && pickListOptionsMap && pickListOptionsMap[item.resourceAttributeVarName]) {
      item.resourceAttributeData.pickListItems = pickListOptionsMap[item.resourceAttributeVarName].items;
    }
  }
}

module.exports = Utils;
