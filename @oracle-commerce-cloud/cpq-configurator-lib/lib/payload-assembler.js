// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.

'use strict';

const defaultRegistry = require('../registry/registry');

const priceInfoChildDef = '_priceInfo';
const recmdPartsChildDef = '_recmdParts';
const recmdModelsChildDef = '_recmdModels';
const mandatoryPartsChildDef = '_mandatoryParts';
const mandatoryModelsChildDef = '_mandatoryModels';
const bomChildDef = '_bom';

/**
 * This class acts as a helper to build the payloads for particular actions.
 */
class PayloadAssember {

  /**
   * Constructor
   *
   * @param {Object} config
   *   An object that contains the configuration settings for the application.
   * @param {Boolean} config.useHTTPS 
   *   Whether or not to use the https protocol for outbound requests.
   * @param {String} config.hostname 
   *   External CPQ Configurator server hostname.
   * @param {String} config.port 
   *   External CPQ Configurator server port.
   * @param {Boolean} config.useHTTPSforPreview 
   *   Whether or not to use the https protocol for outbound preview requests.
   * @param {String} config.previewHostname 
   *   External CPQ Configurator preview server hostname.
   * @param {String} config.previewPort 
   *   External CPQ Configurator preview server port.
   * @param {Number} config.timeout 
   *   Request timeout in ms.
   * @param {Object} config.logger 
   *   Used for logging.
   * @param {String} config.username_env_var 
   *   Environment variable name for basic auth username.
   * @param {String} config.password_env_var 
   *   Environment variable name for basic auth password.
   * @param {String} config.CONFIG_BASE_URI
   *   The CPQ 'config' base REST URI.
   * @param {String} config.LAYOUT_BASE_URI
   *   The CPQ 'layoutCache' base REST URI.
   * @param {String} config.PAGE_TEMPLATES_BASE_URI
   *   The CPQ 'pageTemplates' base REST URI.
   * @param {String} config.CONFIG_UI_SETTINGS_BASE_URI
   *   The CPQ 'configUISettings' base REST URI.
   * @param {String} config.CONFIGURATOR_SSE_BASE_CONTEXT_URI
   *   The base URI of the SSE Configurator application.
   * @param {Object} config.registry
   *   The registry that contains the module mappings for the SSE configurator. (optional)
   */
  constructor (config) {
    if (!config) {
      throw new Error('PayloadAssember requires config to be instantiated');
    }

    this.config = config;

    this.registry = config.registry ? config.registry : defaultRegistry;

    this.constants = this.registry.load(
      this.registry.CPQ_CONFIGURATOR_LIB_MODULE_CONSTANTS,
      this.registry.CPQ_CONFIGURATOR_LIB_PACKAGE);

    const Utils = this.registry.load(
      this.registry.CPQ_CONFIGURATOR_LIB_MODULE_UTILS,
      this.registry.CPQ_CONFIGURATOR_LIB_PACKAGE);
  
    this.utils = new Utils(config);
  }

  /**
   * This function takes care of invoking the appropriate payload assembler
   * for the given action.
   *
   * @param {Object} request
   *   The request that can have particular values that should be added to the payload.
   * @param {Object} action
   *   The action that the payload should be built for.
   */
  assemblePayloadForAction (request, action) {
    switch (action) {
      case this.constants.CONFIGURE_ACTION: {
        if (request.body.op === this.constants.OP_MODIFY) {
          return this._assemblePayloadForModify(request);
        }
        else if (request.body.op === this.constants.OP_UPGRADE) {
          return this._assemblePayloadForUpgrade(request);  
        }
        else {
          return this._assemblePayloadForConfigure(request);
        }
      }
      case this.constants.RECONFIGURE_ACTION:
        return this._assemblePayloadForReconfigure(request);
      case this.constants.UPDATE_ACTION:
        return this._assemblePayloadForUpdate(request);
      case this.constants.INTERACT_ACTION:
        return this._assemblePayloadForInteract(request);
      case this.constants.ADD_TO_CART_ACTION:
        return this._assemblePayloadForAddToCart(request);
      case this.constants.LOAD_DATA_ACTION:
        return this._assemblePayloadForLoadData(request);
      case this.constants.PAGE_ACTIONS.previous:
        return this._assemblePayloadForPreviousPage(request);
      case this.constants.PAGE_ACTIONS.next:
        return this._assemblePayloadForNextPage(request);
      case constants.OP_MODIFY:
        return this._assemblePayloadForModify(request);
      case constants.OP_UPGRADE:
        return this._assemblePayloadForUpgrade(request);  
    }
  }

  // ------------------------------------------------------------
  // PRIVATE FUNCTIONS
  // ------------------------------------------------------------

  /**
   * Assemble a payload for a CPQ modify action.
   * 
   * Note that an modify action is exactly the same as a _configure action
   * except that it also includes an 'assetKey', optional 'transactionDate'.
   * 
   * @private
   *
   * @param {Object} request
   *   The request that may contain extra parameters to add to the payload.
   */
  _assemblePayloadForModify (request) {
    let payload = this._assemblePayloadForConfigure(request);

    payload.assetKey = request.body.assetKey;

    if (request.body.transactionDate) {
      payload.transactionDate = request.body.transactionDate;
    }

    return payload;
  }

  /**
   * Assemble a payload for a CPQ upgrade action.
   * 
   * Note that an upgrade action is exactly the same as a _configure action
   * except that it also includes an 'assetKey', optional 'transactionDate' 
   * and 'modelPunchin' that contains the '_config_upgrade_name' parameter.
   * 
   * @private
   *
   * @param {Object} request
   *   The request that may contain extra parameters to add to the payload.
   */
  _assemblePayloadForUpgrade (request) {
    let payload = this._assemblePayloadForConfigure(request);

    payload.assetKey = request.body.assetKey;

    payload.modelPunchin = {
      parameters: {
        _config_upgrade_name: request.body.upgradeName
      }
    }
    
    if (request.body.transactionDate) {
      payload.transactionDate = request.body.transactionDate;
    }

    return payload;
  }

  /**
   * Assemble a payload for a CPQ _configure action.
   * @private
   *
   * @param {Object} request
   *   The request that may contain extra parameters to add to the payload.
   */
  _assemblePayloadForConfigure (request) {
    const payload = {
      cacheInstanceId: '-1',
      fromPartner: true,
      legacyMode: true,
      fromPunchin: true,
      criteria: {
        state: true,
        childDefs: [
          {name: priceInfoChildDef},
          {name: recmdPartsChildDef},
          {name: recmdModelsChildDef},
          {name: mandatoryPartsChildDef},
          {name: mandatoryModelsChildDef},
          {name: bomChildDef}
        ]
      }
    };

    this._addModelPunchinToPayload(request, payload);
    this._addChildDefsToPayload(request, payload);
    return payload;
  }

  /**
   * Assemble a payload for a CPQ _reconfigure action.
   *
   * @private
   *
   * @param {Object} request
   *   The request that may contain extra parameters to add to the payload.
   */
  _assemblePayloadForReconfigure (request) {
    const payload = {
      configId: parseInt(request.body.configId, 10),
      criteria: {
        state: true,
        childDefs: [
          {name: priceInfoChildDef},
          {name: recmdPartsChildDef},
          {name: recmdModelsChildDef},
          {name: mandatoryPartsChildDef},
          {name: mandatoryModelsChildDef},
          {name: bomChildDef}
        ]
      }
    };

    this._addChildDefsToPayload(request, payload);
    return payload;
  }

  /**
   * Assemble a payload for a CPQ _update action.
   *
   * @private
   *
   * @param {Object} request
   *   The request that may contain extra parameters to add to the payload.
   */
  _assemblePayloadForUpdate (request) {
    const payload = {
      cacheInstanceId: request.params.cacheInstanceId,
      delta: request.body.delta || false,
      criteria: {
        state: true,
        childDefs: [
          {name: priceInfoChildDef},
          {name: recmdPartsChildDef},
          {name: recmdModelsChildDef},
          {name: mandatoryPartsChildDef},
          {name: mandatoryModelsChildDef},
          {name: bomChildDef}
        ]
      }
    };

    this._addChildDefsToPayload(request, payload);
    this._addConfigDataToPayload(request, payload);
    return payload;
  }

  /**
   * Assemble a payload for a CPQ _integration_addToCart action.
   *
   * @private
   *
   * @param {Object} request
   *   The request that may contain extra parameters to add to the payload.
   */
  _assemblePayloadForAddToCart (request) {
    return {
      cacheInstanceId: request.params.cacheInstanceId
    };
  }

  /**
   * Assemble a payload for a CPQ _interact action.
   * 
   * @param {*} request
   *  The request that contains parameters to add to the payload.
   */
  _assemblePayloadForInteract (request) {
    const payload = {
      cacheInstanceId: request.params.cacheInstanceId,
      delta: true,
      configData: request.body.configData,
      criteria: {
        state: true,
        childDefs: [{name: priceInfoChildDef}]
      }
    };

    this._addChildDefsToPayload(request, payload);
    return payload;
  }

  /**
   * Assemble a payload for a CPQ _loadData action.
   *
   * @private
   *
   * @param {Object} request
   *   The request that may contain extra parameters to add to the payload.
   */
  _assemblePayloadForLoadData (request) {
    const payload = {
      criteria: {
        state: true,
        childDefs: [
          {name: priceInfoChildDef},
          {name: recmdPartsChildDef},
          {name: recmdModelsChildDef},
          {name: mandatoryPartsChildDef},
          {name: mandatoryModelsChildDef},
          {name: bomChildDef}
        ]
      }
    };

    this._addChildDefsToPayload(request, payload);
    return payload;
  }

  /**
   * Assemble a payload for a CPQ _previous action.
   *
   * @private
   *
   * @param {Object} request
   *   The request that may contain extra parameters to add to the payload.
   */
  _assemblePayloadForPreviousPage (request) {
    const payload = {
      cacheInstanceId: request.params.cacheInstanceId,
      criteria: {
        state: true,
        childDefs: [
          {name: priceInfoChildDef},
          {name: recmdPartsChildDef},
          {name: recmdModelsChildDef},
          {name: mandatoryPartsChildDef},
          {name: mandatoryModelsChildDef},
          {name: bomChildDef}
        ]
      }
    };

    this._addChildDefsToPayload(request, payload);
    this._addConfigDataToPayload(request, payload);

    return payload;
  }

  /**
   * Assemble a payload for a CPQ _previous action.
   *
   * @private
   *
   * @param {Object} request
   *   The request that may contain extra parameters to add to the payload.
   */
  _assemblePayloadForNextPage (request) {
    const payload = {
      cacheInstanceId: request.params.cacheInstanceId,
      criteria: {
        state: true,
        childDefs: [
          {name: priceInfoChildDef},
          {name: recmdPartsChildDef},
          {name: recmdModelsChildDef},
          {name: mandatoryPartsChildDef},
          {name: mandatoryModelsChildDef},
          {name: bomChildDef}
        ]
      }
    };

    this._addChildDefsToPayload(request, payload);
    this._addConfigDataToPayload(request, payload);

    return payload;
  }

  /**
   * If the request contains childDefs elements, add them to the initial payload.
   *
   * @private
   *
   * @param {Object} request
   *   The request that contains the childDefs to add to the given payload.
   * @param {Object} payload
   *   The payload that will have childDefs appended to.
   */
  _addChildDefsToPayload (request, payload) {
    if (request.body.criteria && request.body.criteria.childDefs) {
      request.body.criteria.childDefs.forEach(element => {
        payload.criteria.childDefs.push(element);
      });
    }
  }

  /**
   * If the request contains a configData object, append it to the payload.
   *
   * @private
   *
   * @param {Object} request
   *   The request that contains the configData object to add to the given payload.
   * @param {Object} payload
   *   The payload that will have configData appended to.
   */
  _addConfigDataToPayload (request, payload) {
    if (request.body.configData) {
      Object.assign(payload, {
        configData: this.utils.removeNonUpdatableAttributes(request.body.configData)
      });
    }
  }

  /**
   * If the request contains a modelPunchin object, append it to the payload.
   *
   * If the request doesn't contain a modelPunchin object, one will be created.
   * The properties held in modelPunchin will be populated with values from the
   * request payload such as, locale, currency, configurationMetadata etc.
   *
   * More details about modelPunchin can be found at:
   *
   * https://docs.oracle.com/cloud/latest/cpq_gs/CXCPQ/op-configprodfamvarname-
   * prodlinevarname-modelvarname-actions-_configure-post.html
   *
   * @private
   *
   * @param {Object} request
   *   The request that contains the modelPunchin object to add to the given payload.
   * @param {Object} payload
   *   The payload that will have modelPunchin appended to.
   */
  _addModelPunchinToPayload (request, payload) {
    if (request.body.modelPunchin) {
      Object.assign(payload, {
        modelPunchin: request.body.modelPunchin
      });
    }
    else {
      payload.modelPunchin = {
        flowInputs: {},
        parameters: {}
      };

      if (request.body.configurationMetadata) {
        // This parameter tells CPQ that there are configurationMetadata attributes to process.
        // eslint-disable-next-line camelcase
        payload.modelPunchin.parameters._variable_name_punchin = 'true';

        // Add the configurationMetadata attributes/values to the payload
        Object.keys(request.body.configurationMetadata).forEach(attribute => {
          payload.modelPunchin.parameters[attribute] = request.body.configurationMetadata[attribute];
        });
      }

      if (request.body.locale) {
        // eslint-disable-next-line camelcase
        payload.modelPunchin.parameters._bm_session_locale = request.body.locale;
      }

      if (request.body.currency) {
        // eslint-disable-next-line camelcase
        payload.modelPunchin.parameters._bm_session_currency = request.body.currency;
      }
    }
  }
}

module.exports = PayloadAssember;
