// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.

'use strict';

const INCLUDE_ACTION = 'include';
const EXCLUDE_ACTION = 'exclude';

/**
 * This class provides functions to reduce layout properties,
 * which are specified in the request.
 */
class LayoutPropertyReducer {

  /**
   * This function will check the request for a includeLayoutProperties or excludeLayoutProperties
   * property and include/exclude properties appropriately.
   *
   * Note that both of these properties can't be used in one request. For example, if
   * includeLayoutProperties AND excludeLayoutProperties is included, includeLayoutProperties
   * will take precedence.
   *
   * @param {Object} request
   *   The request object that will contains properties to be included/excluded.
   * @param {Object} layoutCache
   *   The layoutCache that will have its properties reduced.
   */
  static reduceLayoutProperties (request, layoutCache) {
    const includeLayoutProperties = request.body.includeLayoutProperties || request.query.includeLayoutProperties;

    if (includeLayoutProperties) {
      this._reduce(layoutCache.responseData.contents.layout, includeLayoutProperties, INCLUDE_ACTION);
    }
    else {
      const excludeLayoutProperties = request.body.excludeLayoutProperties || request.query.excludeLayoutProperties;

      if (excludeLayoutProperties) {
        this._reduce(layoutCache.responseData.contents.layout, excludeLayoutProperties, EXCLUDE_ACTION);
      }
    }
  }

  /**
   * This function will exclude/include a specified list of properties from
   * each layout component within a component hierarchy.
   *
   * @param {Object} layout
   *   The layout that will have its properties reduced.
   * @param {Array} layoutProperties
   *   The list of layout properties that will be included/excluded.
   * @param {String} action
   *   The action to be performed. This can either be 'include' or 'exclude'.
   */
  static _reduce (layout, layoutProperties, action) {
    if (layoutProperties && (action === INCLUDE_ACTION || action === EXCLUDE_ACTION)) {

      const reduceLayout = (item) => {
        Object.keys(item).forEach(property => {
          if ((action === INCLUDE_ACTION && !layoutProperties.includes(property)) ||
              (action === EXCLUDE_ACTION && layoutProperties.includes(property))) {
            delete item[property];
          }
        });

        // Recursively process component hierarchy.
        if (item.components && item.components.items) {
          item.components.items.forEach(reduceLayout);
        }
      };

      layout.items.components.items.forEach(reduceLayout);
    }
  }

}

module.exports = LayoutPropertyReducer;
