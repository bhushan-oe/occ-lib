// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.

'use strict';

/**
 * This class provides a number of static functions that validate requests/responses
 * for all of the configurator actions.
 */
class Validators {

  /**
   * Ensure that the given request body contains productFamily, productLine and model values.
   *
   * @param {Object} request
   *   The request that should contain the productFamily, productLine and model.
   * @param {Object} constants
   *   The constants that are used throughout cpq-configurator-lib.
   * 
   * @returns {Boolean}
   *   True if the productFamily, productLine and model values are in the request body and
   *   are not empty, otherwise false.
   */
  static validateProductInRequest (request) {
    if (!request.body.productFamily || !request.body.productLine || !request.body.model) {
      return false;
    }

    return true;
  }

  /**
   * Ensure that the given request query contains productFamily, productLine and model values.
   *
   * @param {Object} request
   *   The request that should contain the productFamily, productLine and model.
   * @param {Object} constants
   *   The constants that are used throughout cpq-configurator-lib.
   * 
   * @returns {Boolean}
   *   True if the productFamily, productLine and model values are in the request query and
   *   are not empty, otherwise false.
   */
  static validateProductInQuery (request) {
    if (!request.query.productFamily || !request.query.productLine || !request.query.model) {
      return false;
    }

    return true;
  }

  /**
   * Ensure that the given page action request payload contains a cacheInstanceId.
   *
   * @param {Object} request
   *   The request that should contain a valid page action operation and cacheInstanceId.
   * @param {Object} constants
   *   The constants that are used throughout cpq-configurator-lib.
   * 
   * @returns {Boolean}
   *   True if the page action request payload is valid, otherwise false.
   */
  static validateCacheInstanceIdForPageAction (request) {
    // cacheInstanceId must match a current configuration in CPQ.
    if (request.body.op && !request.params.cacheInstanceId) {
      return false;
    }
    return true;
  }

  /**
   * Ensure that the given page action request payload contains a page operator.
   *
   * @param {Object} request
   *   The request that should contain the required page operator.
   *
   * @returns {Boolean}
   *   True if the page action request payload is valid, otherwise false.
   */
  static validatePageOperatorExistsForPageAction (request) {
    // Page operation must be supplied when cachInstanceId has been supplied.
    if (!request.body.op && request.params.cacheInstanceId) {
      return false;
    }
    return true;
  }

  /**
   * Ensure that the given page action request payload contains a supported page operator.
   *
   * @param {Object} request
   *   The request that should contain the required page operator.
   *
   * @returns {Boolean}
   *   True if the page action request payload is valid, otherwise false.
   */
  static validatePageOperatorForPageAction (request, supportedPageActions) {
    // If cacheInstanceId and page operation has been supplied, the
    // page action must exist in the array of valid page action.
    if (request.params.cacheInstanceId && request.body.op && !supportedPageActions.hasOwnProperty(request.body.op)) {
      return false;
    }
    return true;
  }

  /**
   * Checks if a cacheInstanceId exists as a request parameter.
   *
   * @param {Object} request
   *   The request to be checked for a cacheInstanceId.
   * @param {Object} constants
   *   The constants that are used throughout cpq-configurator-lib.
   * 
   * @returns {Boolean}
   *   True when cacheInstanceId exists, otherwise false.
   */
  static validateCacheInstanceIdInRequest (request) {
    if (!request.params.cacheInstanceId) {
      return false;
    }

    return true;
  }

  /**
   * Checks if a arraySetVarName exists as a request parameter.
   *
   * @param {Object} request
   *   The request to be checked for a cacheInstanceId.
   * @param {Object} response
   *   The response that will be updated with error code and error message if arraySetVarName is missing.
   * @param {Object} constants
   *   The constants that are used throughout cpq-configurator-lib.
   * 
   * @returns {Boolean}
   *   True when arraySetVarName exists, otherwise false.
   */
  static validateArraySetVarNameInRequest (request) {
    if (!request.body.arraySetVarName) {
      return false;
    }
    return true;
  }

  /**
   * Checks if a arraySetVarName exists as a request query.
   *
   * @param {Object} request
   *   The request to be checked for a cacheInstanceId.
   * @param {Object} constants
   *   The constants that are used throughout cpq-configurator-lib.
   * 
   * @returns {Boolean}
   *   True when arraySetVarName exists, otherwise false.
   */
  static validateArraySetVarNameInQuery (request) {
    if (!request.query.arraySetVarName) {
      return false;
    }

    return true;
  }

  /**
   * Checks if a configData._state.attributes exists in request payload.
   *
   * @param {Object} request
   *   The request to be checked for configData._state.attributes.
   * @param {Object} constants
   *   The constants that are used throughout cpq-configurator-lib.
   * 
   * @returns {Boolean}
   *   True when configData._state.attributes exists, otherwise false.
   */
  static validateStateAttributesInRequest (request) {
    if (!request.body.configData || !request.body.configData._state || !request.body.configData._state.attributes) {
      return false;
    }
    return true;
  }

  /**
   * Checks if the given actionResponse contains a layoutFlow href value.
   *
   * @param {Object} actionResponse
   *   The object that should contain a layoutFlow value.
   * @param {Object} constants
   *   The constants that are used throughout cpq-configurator-lib.
   *
   * @returns {Boolean}
   *   True when a layoutFlow value exists, otherwise false.
   */
  static validateLayoutFlowInActionResponse (actionResponse) {
    if (!(actionResponse.responseData._flow &&
          actionResponse.responseData._flow.links[0] &&
          actionResponse.responseData._flow.links[0].href)) {

      return false;
    }

    return true;
  }

  /**
   * Checks if a removeIndex exists as a request query parameter.
   *
   * @param {Object} request
   *   The request to be checked for a removeIndex.
   * @param {Object} constants
   *   The constants that are used throughout cpq-configurator-lib.
   * 
   * @returns {Boolean}
   *   True when removeIndex exists, otherwise false.
   */
  static validateRemoveIndexInQuery (request) {
    if (!request.query.hasOwnProperty('removeIndex') || request.query.removeIndex < 0) {
      return false;
    }
    return true;
  }

  /**
   * Checks if an array of expand parameters are valid.
   *
   * @param {Array} expandParameters
   *   An array of expand parameters to be validated.
   * 
   * @param {Array} supportedExpandParameters
   *   An array of expand parameters supported by the configurator.
   *
   * @returns {Boolean}
   *   True if all expand parameters are valid, otherwise false.
   */
  static validateExpandParameters (expandParameters, supportedExpandParameters) {
    const invalidExpandParameters = [];

    expandParameters.forEach(parameter => {
      if (!Object.values(supportedExpandParameters).includes(parameter)) {
        invalidExpandParameters.push(parameter);
      }
    });
    
    return invalidExpandParameters;
  }

  /**
   * Checks if a arraySetVarNames exists as a request query parameter.
   *
   * @param {Object} request
   *   The request to be checked for a removeIndex.
   * @param {Object} constants
   *   The constants that are used throughout cpq-configurator-lib.
   * 
   * @returns {Boolean}
   *   True when arraySetVarNames exists, otherwise false.
   */
  static validateArraySetVarNamesInQuery (request) {
    if (!request.query.hasOwnProperty('arraySetVarNames')) {
      return false;
    }

    return true;
  }

  /**
   * Checks if a configFlow exists as a request parameter.
   *
   * @param {Object} request
   *   The request to be checked for a configFlow.
   * @param {Object} response
   *   The response that will be updated with error code and error message if configFlow is missing.
   * @param {Object} constants
   *   The constants that are used throughout cpq-configurator-lib.
   * 
   * @returns {Boolean}
   *   True when configFlow exists, otherwise false.
   */
  static validateFlowInRequest (request) {
    if (!request.params.flow) {
      return false;
    }
    return true;
  }

  /**
   * Checks if a hierLevel exists as a request query parameter.
   *
   * @param {Object} request
   *   The request to be checked for a removeIndex.
   * @param {Object} constants
   *   The constants that are used throughout cpq-configurator-lib.
   * 
   * @returns {Boolean}
   *   True when hierLevel exists, otherwise false.
   */
  static validateHierLevelInQuery (request) {
    if (!request.query.hasOwnProperty('hierLevel') || !request.query.hierLevel) {
      return false;
    }
    return true;
  }

  /**
   * Checks if a pickListVarName exists as a request parameter.
   *
   * @param {Object} request
   *   The request to be checked for a pickListVarName.
   * @param {Object} constants
   *   The constants that are used throughout cpq-configurator-lib.
   *
   * @returns {Boolean}
   *   True when pickListVarName exists, otherwise false.
   */
  static validatePickListVarNameInRequest (request) {
    if (!request.params.pickListVarName) {
      return false;
    }
    return true;
  }

  /**
   * Check if the request contains an assetKey that the request also contains an op parameter
   * that exists in the given supported operations array.
   * 
   * @param {Object} request
   *   The request that should contain assetKey and op parameters in its body object.
   * @param {Array} supportedOperations
   *   An array of supported ABO operations.
   * 
   * @return {Boolean}
   *   true if an assetKey and supported operation is found in the request, otherwise false.
   */
  static validateIsABOSupported (request, supportedOperations) {
    if (request.body.assetKey && !(request.body.op && supportedOperations.includes(request.body.op))) {
      return false;
    }
    return true;
  }

  /**
   * Check if the given request contains an assetKey.
   * 
   * @param {Object} request
   *   The request that should contain an assetKey in its request body object.
   * 
   * @return {Boolean}
   *   true if an assetKey is found in the request, otherwise false.
   */
  static validateAssetKeyInRequest (request) {
    if (!request.body.assetKey) {
      return false;
    }
    return true;
  }

  /**
   * Check if the given request contains an upgradeName.
   * 
   * @param {Object} request
   *   The request that should contain an upgradeName in its body object.
   * 
   * @return {Boolean}
   *   true if an upgradeName is found in the request, otherwise false.
   */
  static validateUpgradeNameInRequest (request) {
    if (!request.body.upgradeName) {
      return false;
    }
    return true;
  }

}

module.exports = Validators;
