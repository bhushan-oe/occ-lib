// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.

'use strict';
// eslint-disable-next-line node/no-extraneous-require
const nconf = require('nconf');
const TranslatableError = require('@oracle-commerce-cloud/translatable-error');
const defaultRegistry = require('../registry/registry');

/**
 * This class is responsible for building REST resource links for the
 * configurator responses. These links can then be used to expand the
 * responses for more configuration information.
 */
class LinkBuilder {

  /**
   * Constructor
   *
   * @param {Object} config
   *   An object that contains the configuration settings for the application.
   * @param {Boolean} config.useHTTPS 
   *   Whether or not to use the https protocol for outbound requests.
   * @param {String} config.hostname 
   *   External CPQ Configurator server hostname.
   * @param {String} config.port 
   *   External CPQ Configurator server port.
   * @param {Boolean} config.useHTTPSforPreview 
   *   Whether or not to use the https protocol for outbound preview requests.
   * @param {String} config.previewHostname 
   *   External CPQ Configurator preview server hostname.
   * @param {String} config.previewPort 
   *   External CPQ Configurator preview server port.
   * @param {Number} config.timeout 
   *   Request timeout in ms.
   * @param {Object} config.logger 
   *   Used for logging.
   * @param {String} config.username_env_var 
   *   Environment variable name for basic auth username.
   * @param {String} config.password_env_var 
   *   Environment variable name for basic auth password.
   * @param {String} config.CONFIG_BASE_URI
   *   The CPQ 'config' base REST URI.
   * @param {String} config.LAYOUT_BASE_URI
   *   The CPQ 'layoutCache' base REST URI.
   * @param {String} config.PAGE_TEMPLATES_BASE_URI
   *   The CPQ 'pageTemplates' base REST URI.
   * @param {String} config.CONFIG_UI_SETTINGS_BASE_URI
   *   The CPQ 'configUISettings' base REST URI.
   * @param {String} config.CONFIGURATOR_SSE_BASE_CONTEXT_URI
   *   The base URI of the SSE Configurator application.
   * @param {Object} config.registry
   *   The registry that contains the module mappings for the SSE configurator. (optional)
   */
  constructor (config) {
    if (!config) {
      throw new Error('LinkBuilder requires config to be instantiated');
    }

    this.config = config;
    this.logger = config.logger;

    this.registry = config.registry ? config.registry : defaultRegistry;

    this.constants = this.registry.load(
      this.registry.CPQ_CONFIGURATOR_LIB_MODULE_CONSTANTS,
      this.registry.CPQ_CONFIGURATOR_LIB_PACKAGE);
    
    this.validators = this.registry.load(
      this.registry.CPQ_CONFIGURATOR_LIB_MODULE_VALIDATORS,
      this.registry.CPQ_CONFIGURATOR_LIB_PACKAGE);
    
    const Utils = this.registry.load(
      this.registry.CPQ_CONFIGURATOR_LIB_MODULE_UTILS,
      this.registry.CPQ_CONFIGURATOR_LIB_PACKAGE);

    this.utils = new Utils(config);
  }

  /**
   * Build a link object for the layout. The layout property will be appended to the given actionResponse
   * and populated with the link details.
   *
   * @param {Object} request
   *   The request that will be used to build the link.
   * @param {Object} actionResponse
   *   The object that should contain a layoutFlow value. the actionResponse
   *   will also be updated with the link details object.
   */
  buildLayoutLink (request, actionResponse) {

    // Ensure that a layoutFlow exists in the actionResponse before continuing.
    if (!this.validators.validateLayoutFlowInActionResponse(actionResponse)) {
      throw new TranslatableError(
        'Cannot find layout flow.', this.constants.FLOW_REQUIRED, 404);
    }

    const {productFamily, productLine, model} = request.body;
    const {flow, hierLevel} = this.utils.getLayoutFlowDetails(actionResponse);
    const locale = this.utils.getLocale(request);
    const urlProtocolDomainPort = this._getUrlProtocolDomainPort(request);
    const contextUri = this._getContextURI(request);

    const linkUrl =
      `${urlProtocolDomainPort}${contextUri}/layouts/${flow
        }?productFamily=${productFamily
        }&productLine=${productLine
        }&model=${model
        }&locale=${locale
        }&hierLevel=${hierLevel}`;

    actionResponse.responseData.links = actionResponse.responseData.links || [];

    actionResponse.responseData.links.push({
      rel: 'layout',
      href: linkUrl
    });

    if (actionResponse.responseData.layout) {
      actionResponse.responseData.layout.links = [
        {
          rel: 'layout',
          href: linkUrl
        }
      ];
    }

  }

  /**
   * Build a link object for pickListOptions.
   *
   * @param {Object} request
   *   The request that will be used to build the link.
   * @param {Object} configureActionResponse
   *   The object that will be updated with pick list options link information.
   */
  buildPickListOptionsLinks (request, configureActionResponse) {

    const {productFamily, productLine, model} = request.body;
    const cacheInstanceId = configureActionResponse.responseData.cacheInstanceId;
    const urlProtocolDomainPort = this._getUrlProtocolDomainPort(request);
    const contextUri = this._getContextURI(request);

    // This will hold the current request URL that is necessary to build
    // each link, e.g. http://<server>:<port>/ccstorex/custom/v1/configurations.
    let requestUrl = `${urlProtocolDomainPort}${contextUri}`;

    // If the current request is a previous/next operation, '/page' will
    // be in the URL. This needs to be removed as its won't be part of the pickList link URL.
    if (requestUrl.endsWith('/page')) {
      requestUrl = requestUrl.substr(0, requestUrl.length - 5);
    }

    const processPickLists = item => {
      if (item.resourceAttributeData &&
          item.componentTypeCode.lookupCode === 'FORM_CONTROL' &&
          item.inputTypeCode.lookupCode === 'SingleSelectPickList') {

        const pickListVarName = item.resourceAttributeVarName;
        const queryParameters = `?productFamily=${productFamily}&productLine=${productLine}&model=${model}`;
        
        let linkUrl;

        if (requestUrl.endsWith(`/${cacheInstanceId}`)) {
          linkUrl = `${requestUrl}/pick-lists/${pickListVarName}${queryParameters}`;
        }
        else {
          linkUrl = `${requestUrl}/${cacheInstanceId}/pick-lists/${pickListVarName}${queryParameters}`;
        }

        item.resourceAttributeData.links = item.resourceAttributeData.links || [];

        item.resourceAttributeData.links.push({
          rel: 'pickListItems',
          href: linkUrl
        });


        // Update configData picklist links from CPQ to OCC URLs.
        if (configureActionResponse.responseData.configData[item.resourceAttributeVarName]) {
          configureActionResponse.responseData.configData[item.resourceAttributeVarName].links = [
            {
              rel: 'self',
              href: linkUrl
            }
          ];
        }
      }

      if (item.components && item.components.items) {
        item.components.items.forEach(processPickLists);
      }
    };

    configureActionResponse.responseData.layout.items.components.items.forEach(processPickLists);
  }

  /**
   * Build a link object for templates.
   *
   * @param {Object} request
   *   The request that will be used to build the link.
   * @param {Object} configureActionResponse
   *   The object that will be updated with templates link information.
   */
  buildTemplatesLink (request, configureActionResponse) {
    const urlProtocolDomainPort = this._getUrlProtocolDomainPort(request);
    const contextUri = this._getContextURI(request);

    const linkUrl = `${urlProtocolDomainPort}${contextUri}/templates`;

    configureActionResponse.responseData.links = configureActionResponse.responseData.links || [];

    configureActionResponse.responseData.links.push(
      {
        rel: 'templates',
        href: linkUrl
      }
    );

    if (configureActionResponse.responseData.templates) {
      configureActionResponse.responseData.templates.links = [
        {
          rel: 'self',
          href: linkUrl
        }
      ];
    }
  }

  /**
   * Build a link object for ui settings.
   *
   * @param {Object} request
   *   The request that will be used to build the link.
   * @param {Object} configureActionResponse
   *   The object that will be updated with ui settings link information.
   */
  buildUiSettingsLink (request, configureActionResponse) {
    const urlProtocolDomainPort = this._getUrlProtocolDomainPort(request);
    const contextUri = this._getContextURI(request);

    const linkUrl = `${urlProtocolDomainPort}${contextUri}/ui-settings`;

    configureActionResponse.responseData.links = configureActionResponse.responseData.links || [];

    configureActionResponse.responseData.links.push(
      {
        rel: 'uiSettings',
        href: linkUrl
      }
    );

    if (configureActionResponse.responseData.uiSettings) {
      configureActionResponse.responseData.uiSettings.links = [
        {
          rel: 'self',
          href: linkUrl
        }
      ];
    }
  }

  /**
   * This function will retrieve the originalUrl from the request and determine
   * which OCC server (agent or store) to use for building links.
   *
   * @param {Object} request
   *   The request that will contain the originalUrl.
   */
  _getUrlProtocolDomainPort (request) {
    let urlProtocolDomainPort = null;

    if (request.originalUrl.includes('/ccstorex/')) {
      urlProtocolDomainPort = nconf.get('atg.server.url');
    }
    else if (request.originalUrl.includes('/ccagentx/')) {
      urlProtocolDomainPort = nconf.get('atg.server.agent.url');
    }
    else {
      this.logger.error(`URL protocol, domain and port could not be retrieved. Ensure 
        atg.server.url and atg.server.agent.url has been set in runtime-environment.`);

      throw new Error(`${request.originalUrl} does not contain a valid resource`);
    }
    
    return urlProtocolDomainPort;
  }

  /**
   * This utility function will build a context URI for the configurator application.
   * 
   * For example, /ccstorex/custom/v1/configurations ​​or /ccagentx/custom/v1/configurations.
   * 
   * @param {*} request
   *   The request that the originalUrl value will be extracted from.
   */
  _getContextURI (request) {
    // This will be the store or agent context URI, i.e. /ccstorex/custom ​​or /ccagentx/custom.
    let uriArray = request.originalUrl.split('/');

    if (request.originalUrl.includes('http://') || request.originalUrl.includes('https://')) {
      return `/${uriArray[2]}/${uriArray[3]}${this.config.CONFIGURATOR_SSE_BASE_CONTEXT_URI}`;
    }
  
    return `/${uriArray[1]}/${uriArray[2]}${this.config.CONFIGURATOR_SSE_BASE_CONTEXT_URI}`;
  }

}

module.exports = LinkBuilder;
