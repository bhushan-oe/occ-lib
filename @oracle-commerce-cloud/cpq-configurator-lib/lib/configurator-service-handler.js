// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.

'use strict';

const TranslatableError = require('@oracle-commerce-cloud/translatable-error');
const defaultRegistry = require('../registry/registry');

/**
 * This class serves as a handler for all of the OCC -> CPQ configurator actions.
 *
 * It will perform all the necessary request/response validation and manipulation
 * of data before returning to the router/caller.
 */
class ConfiguratorServiceHandler {

  /**
   * Constructor
   *
   * @param {Object} config
   *   An object that contains the configuration settings for the application.
   * @param {Boolean} config.useHTTPS
   *   Whether or not to use the https protocol for outbound requests.
   * @param {String} config.hostname
   *   External CPQ Configurator server hostname.
   * @param {String} config.port
   *   External CPQ Configurator server port.
   * @param {Boolean} config.useHTTPSforPreview
   *   Whether or not to use the https protocol for outbound preview requests.
   * @param {String} config.previewHostname
   *   External CPQ Configurator preview server hostname.
   * @param {String} config.previewPort
   *   External CPQ Configurator preview server port.
   * @param {Number} config.timeout
   *   Request timeout in ms.
   * @param {Object} config.logger
   *   Used for logging.
   * @param {String} config.username_env_var
   *   Environment variable name for basic auth username.
   * @param {String} config.password_env_var
   *   Environment variable name for basic auth password.
   * @param {String} config.CONFIG_BASE_URI
   *   The CPQ 'config' base REST URI.
   * @param {String} config.LAYOUT_BASE_URI
   *   The CPQ 'layoutCache' base REST URI.
   * @param {String} config.PAGE_TEMPLATES_BASE_URI
   *   The CPQ 'pageTemplates' base REST URI.
   * @param {String} config.CONFIG_UI_SETTINGS_BASE_URI
   *   The CPQ 'configUISettings' base REST URI.
   * @param {String} config.CONFIGURATOR_SSE_BASE_CONTEXT_URI
   *   The base URI of the SSE Configurator application.
   * @param {Object} config.registry
   *   The registry that contains the module mappings for the SSE configurator. (optional)
   */
  constructor (config) {
    if (!config) {
      throw new Error('ConfiguratorServiceHandler requires config to be instantiated');
    }

    this.config = config;
    this.logger = config.logger;

    this.registry = config.registry ? config.registry : defaultRegistry;

    this.attributeMapper = this.registry.load(
      this.registry.CPQ_OCC_TRANSFORM_UTILS_LIB_MODULE_CPQ_OCC_LINE_ITEM_MAPPER,
      this.registry.CPQ_DATA_TRANSFORM_UTILS_LIB_PACKAGE);

    this.constants = this.registry.load(
      this.registry.CPQ_CONFIGURATOR_LIB_MODULE_CONSTANTS,
      this.registry.CPQ_CONFIGURATOR_LIB_PACKAGE);

    this.validators = this.registry.load(
      this.registry.CPQ_CONFIGURATOR_LIB_MODULE_VALIDATORS,
      this.registry.CPQ_CONFIGURATOR_LIB_PACKAGE);

    const Utils = this.registry.load(
      this.registry.CPQ_CONFIGURATOR_LIB_MODULE_UTILS,
      this.registry.CPQ_CONFIGURATOR_LIB_PACKAGE);

    this.utils = new Utils(config);

    const LinkBuilder = this.registry.load(
      this.registry.CPQ_CONFIGURATOR_LIB_MODULE_LINK_BUILDER,
      this.registry.CPQ_CONFIGURATOR_LIB_PACKAGE);

    this.linkBuilder = new LinkBuilder(config);

    const CpqRequestDispatcher = this.registry.load(
      this.registry.CPQ_CONFIGURATOR_LIB_MODULE_CPQ_REQUEST_DISPATCHER,
      this.registry.CPQ_CONFIGURATOR_LIB_PACKAGE);

    this.requestDispatcher = new CpqRequestDispatcher(config);
  }

  /**
   * This function acts as a handler for the configure, reconfigure and next/previous page actions.
   *
   * @param {Object} options
   *   This object contains the request payload for performing a configure action.
   *
   * @returns {Promise}
   *   A success/failure response.
   */
  async handleConfigure (options) {
    const {request} = options;

    if (!this.validators.validateProductInRequest(request)) {
      throw new TranslatableError(
        'productFamily, productLine and model must be supplied.', this.constants.INVALID_PRODUCT_INPUT, 400);
    }

    let configureActionResponse = await this.requestDispatcher.doConfigure(request);
    const expandParameters = this.utils.getExpandParameters(request);

    if (expandParameters) {
      const invalidExpandParameters =
        this.validators.validateExpandParameters(expandParameters, this.constants.SUPPORTED_EXPAND_PARAMETERS);

      if (invalidExpandParameters.length > 0) {
        throw new TranslatableError(
          `Request contains invalid expand parameters: ${invalidExpandParameters}.`,
            this.constants.INVALID_EXPAND_PARAMETERS, 309);
      }

      if (expandParameters.includes(this.constants.SUPPORTED_EXPAND_PARAMETERS.all) ||
          expandParameters.includes(this.constants.SUPPORTED_EXPAND_PARAMETERS.layout)) {

        if (!this.validators.validateLayoutFlowInActionResponse(configureActionResponse)) {
          throw new TranslatableError(
            'layoutCache could not be found.', this.constants.LAYOUT_CACHE_NOT_FOUND, 400);
        }

        const layoutCacheResponse =
          await this.requestDispatcher.doLayoutCacheRetrieval(
            request, this.utils.getLayoutFlowDetails(configureActionResponse));

        const arraySetAndPickListIds = this.utils.getArraySetAndPickListIds(
          layoutCacheResponse.responseData.contents.layout.items.components.items);

        let layoutCacheAttributes = this.utils.reduceLayoutCacheContents(layoutCacheResponse, 'attributes');
        const layoutCacheArraySets = this.utils.reduceLayoutCacheContents(layoutCacheResponse, 'arraysets');

        // Process array sets if any exist for the current configuration.

        let arraySetsData = {};

        if (arraySetAndPickListIds.arraySetVariableNames.length) {
          configureActionResponse = await this.requestDispatcher.doLoadData(request,
            configureActionResponse.responseData.cacheInstanceId, arraySetAndPickListIds.arraySetVariableNames);

          arraySetsData =
            this.utils.getArraySetData(layoutCacheArraySets, arraySetAndPickListIds.arraySetVariableNames);

          layoutCacheAttributes = this.utils.addArraySetsToLayoutCacheAttributes(
            layoutCacheAttributes, layoutCacheArraySets, arraySetAndPickListIds.arraySetVariableNames);
        }

        this.utils.addArraySetsVariableNamesToConfiguration(arraySetsData, configureActionResponse);

        // Process pick lists if any exist for the current configuration.

        const pickListOptions = await this.utils.getPickListOptions(
          request, this.requestDispatcher, configureActionResponse.responseData.cacheInstanceId,
            arraySetAndPickListIds.pickListResourceAttributeVarNames);

        this.utils.mergeDataToLayoutComponents(
          layoutCacheResponse, layoutCacheAttributes, arraySetsData, pickListOptions);

        configureActionResponse.responseData.layout = layoutCacheResponse.responseData.contents.layout;
        this.linkBuilder.buildPickListOptionsLinks(request, configureActionResponse);
      }

      if (expandParameters.includes(this.constants.SUPPORTED_EXPAND_PARAMETERS.all) ||
          expandParameters.includes(this.constants.SUPPORTED_EXPAND_PARAMETERS.templates)) {

        const templatesResponse = await this.requestDispatcher.doPageTemplates(request);
        configureActionResponse.responseData.templates = templatesResponse.responseData;
      }

      if (expandParameters.includes(this.constants.SUPPORTED_EXPAND_PARAMETERS.all) ||
          expandParameters.includes(this.constants.SUPPORTED_EXPAND_PARAMETERS.uiSettings)) {

        const uiSettingsResponse = await this.requestDispatcher.doUiSettings(request);
        configureActionResponse.responseData.uiSettings = uiSettingsResponse.responseData;
      }
    }

    configureActionResponse.responseData.layoutFlowVariableName = configureActionResponse.responseData._flow.flow;

    // reset the links collection beforing building SSE endpoint links
    configureActionResponse.responseData.links = [];

    this.linkBuilder.buildLayoutLink(request, configureActionResponse);
    this.linkBuilder.buildTemplatesLink(request, configureActionResponse);
    this.linkBuilder.buildUiSettingsLink(request, configureActionResponse);

    this.utils.removeUnsupportedActions(configureActionResponse.responseData.configData);
    return this.utils.removeCpqSpecificProperties(configureActionResponse, true);
  }

  /**
   * A function that makes a request to an external system to perfom a page (next/previous) configuration action.
   *
   * @param {Object} options
   *   The object that holds all of the configuration/payload data for the page action.
   *
   * @returns {Promise}
   *   A success/failure response.
   */
  async handlePage (options) {
    const {request} = options;

    // Ensure that the required parameters have been supplied in the payload.
    if (!this.validators.validateCacheInstanceIdForPageAction(request)) {
      throw new TranslatableError(
        'cacheInstanceId is required for next/previous action.',
          this.constants.CACHE_INSTANCE_ID_REQUIRED_FOR_PAGE, 400);
    }
    else if (!this.validators.validatePageOperatorExistsForPageAction(request)) {
      throw new TranslatableError(
        'next/previous action op is required for page action.', this.constants.PAGE_OP_REQUIRED, 400);
    }
    else if (!this.validators.validatePageOperatorForPageAction(request, this.constants.PAGE_ACTIONS)) {
      throw new TranslatableError('Page operation is not valid.', this.constants.INVALID_PAGE_OP, 400);
    }

    return this.handleConfigure(options);
  }

  /**
   * A function that makes a request to an external system to update a configuration.

   * @param {Object} options
   *   The object that holds all of the configuration/payload data for the update action.
   *
   * @returns {Promise}
   *   A success/failure response.
   */
  async handleUpdate (options) {
    const {request} = options;

    // Ensure that the required parameters have been supplied in the payload.
    if (!this.validators.validateProductInRequest(request)) {
      throw new TranslatableError(
        'productFamily, productLine and model must be supplied.', this.constants.INVALID_PRODUCT_INPUT, 400);
    }
    else if (!this.validators.validateCacheInstanceIdInRequest(request)) {
      throw new TranslatableError(
        'cacheInstanceId is required for action.',
          this.constants.CACHE_INSTANCE_ID_REQUIRED_FOR_ACTION, 400);
    }
    else if (!this.validators.validateStateAttributesInRequest(request)) {
      throw new TranslatableError(
        'configData._state.attributes must be supplied.',
          this.constants.MISSING_STATE_ATTRIBUTES, 400);
    }

    const updateActionResponse = await this.requestDispatcher.doUpdate(request);
    this.utils.removeUnsupportedActions(updateActionResponse.responseData.configData);
    return this.utils.removeCpqSpecificProperties(updateActionResponse);
  }

  /**
   * A function that makes a request to an external system to perform an interact for a configuration.
   *
   * @param {Object} options
   *   The object that holds all of the configuration/payload data for the update action.
   *
   * @returns {Promise}
   *   A success/failure response.
   */
  async handleInteract (options) {
    const {request} = options;

    // Ensure that the required parameters have been supplied in the payload.
    if (!this.validators.validateProductInRequest(request)) {
      throw new TranslatableError(
        'productFamily, productLine and model must be supplied.', this.constants.INVALID_PRODUCT_INPUT, 400);
    }
    else if (!this.validators.validateCacheInstanceIdInRequest(request)) {
      throw new TranslatableError(
        'cacheInstanceId is required for action.',
          this.constants.CACHE_INSTANCE_ID_REQUIRED_FOR_ACTION, 400);
    }

    const interactActionResponse = await this.requestDispatcher.doInteract(request);
    this.utils.removeUnsupportedActions(interactActionResponse.responseData.configData);
    return this.utils.removeCpqSpecificProperties(interactActionResponse);
  }

  /**
   * A function that makes a request to an external system to add to cart.
   *
   * @param {Object} options
   *   This object contains the request payload for performing an add to cart action.
   *
   * @returns {Promise}
   *   A success/failure response.
   */
  async handleAddToCart (options) {
    const {request} = options;

    // Ensure that the required cachInstanceId, productFamily, productLine and model
    // have been supplied in the payload.
    if (!this.validators.validateProductInRequest(request)) {
      throw new TranslatableError(
        'productFamily, productLine and model must be supplied.', this.constants.INVALID_PRODUCT_INPUT, 400);
    }
    else if (!this.validators.validateCacheInstanceIdInRequest(request)) {
      throw new TranslatableError(
        'cacheInstanceId is required for action.',
          this.constants.CACHE_INSTANCE_ID_REQUIRED_FOR_ACTION, 400);
    }

    const addToCartActionResponse = await this.requestDispatcher.doAddToCart(request);

    // Remove destinationUrl from response as this is only need internally by CPQ.
    delete addToCartActionResponse.responseData.destinationUrl;

    // Remove \n\t characters from integrationPayload so that JSON response is valid.
    const validIntegrationPayload =
      addToCartActionResponse.responseData.integrationPayload.replace('\n\t', '');

    // Map the CPQ attributes to the OCC cart format.
    addToCartActionResponse.responseData.integrationPayload =
      this.attributeMapper.mapCpqToOcc(JSON.parse(validIntegrationPayload));

    return addToCartActionResponse;
  }

  /**
   * A function that makes a request to an external system to add an array set row.
   *
   * @param {Object} options
   *   This object contains the request payload for performing an add array set row action.
   *
   * @returns {Promise}
   *   A success/failure response.
   */
  async handleAddArraySetRow (options) {
    const {request} = options;

    if (!this.validators.validateProductInRequest(request)) {
      throw new TranslatableError(
        'productFamily, productLine and model must be supplied.', this.constants.INVALID_PRODUCT_INPUT, 400);
    }
    else if (!this.validators.validateCacheInstanceIdInRequest(request)) {
      throw new TranslatableError(
        'cacheInstanceId is required for action.',
          this.constants.CACHE_INSTANCE_ID_REQUIRED_FOR_ACTION, 400);
    }
    else if (!this.validators.validateArraySetVarNameInRequest(request)) {
      throw new TranslatableError(
        'arraySetVarName must be supplied.',
          this.constants.ARRAY_SET_VAR_NAME_REQUIRED_FOR_ACTION, 400);
    }

    const {productFamily, productLine, model, arraySetVarName} = request.body;

    return this.requestDispatcher.doArraySetRowAction(
      request,
      productFamily,
      productLine,
      model,
      arraySetVarName,
      this.constants.ADD_ARRAY_SET_ROW_ACTION
    );
  }

  /**
   * A function that makes a request to an external system to delete an array set row.
   *
   * @param {Object} options
   *   This object contains the request payload for performing an add array set row action.
   *
   * @returns {Promise}
   *   A success/failure response.
   */
  async handleDeleteArraySetRow (options) {
    const {request} = options;

    if (!this.validators.validateProductInQuery(request)) {
      throw new TranslatableError(
        'productFamily, productLine and model must be supplied.', this.constants.INVALID_PRODUCT_INPUT, 400);
    }
    else if (!this.validators.validateCacheInstanceIdInRequest(request)) {
      throw new TranslatableError(
        'cacheInstanceId is required for action.', this.constants.CACHE_INSTANCE_ID_REQUIRED_FOR_ACTION, 400);
    }
    else if (!this.validators.validateArraySetVarNameInQuery(request)) {
      throw new TranslatableError(
        'arraySetVarName must be supplied.', this.constants.ARRAY_SET_VAR_NAME_REQUIRED_FOR_ACTION, 400);
    }
    else if (!this.validators.validateRemoveIndexInQuery(request)) {
      throw new TranslatableError(
        'removeIndex must be supplied.', this.constants.REMOVE_INDEX_NOT_FOUND, 400);
    }

    const {productFamily, productLine, model, arraySetVarName} = request.query;

    return this.requestDispatcher.doArraySetRowAction(
      request,
      productFamily,
      productLine,
      model,
      arraySetVarName,
      this.constants.DELETE_ARRAY_SET_ROW_ACTION
    );
  }

  /**
   * A function that makes a request to an external system to get the configData for an array set.
   *
   * @param {Object} options
   *   This object contains the request payload for performing array set get configData.
   *
   * @returns {Promise}
   *   A success/failure response.
   */
  async handleGetArraySetConfigData (options) {
    const {request} = options;

    if (!this.validators.validateProductInQuery(request)) {
      throw new TranslatableError(
        'productFamily, productLine and model must be supplied.', this.constants.INVALID_PRODUCT_INPUT, 400);
    }
    else if (!this.validators.validateCacheInstanceIdInRequest(request)) {
      throw new TranslatableError(
        'cacheInstanceId is required for action.', this.constants.CACHE_INSTANCE_ID_REQUIRED_FOR_ACTION, 400);
    }
    else if (!this.validators.validateArraySetVarNamesInQuery(request)) {
      throw new TranslatableError(
        'arraySetVarNames must be supplied.', this.constants.ARRAY_SET_VAR_NAMES_NOT_FOUND, 400);
    }

    const arraySetVarNames = request.query.arraySetVarNames;
    const cacheInstanceId = request.params.cacheInstanceId;

    const arraySetConfigurationData = await this.requestDispatcher.doLoadData(
      request,
      cacheInstanceId,
      arraySetVarNames.split(',')
    );

    return this.utils.removeCpqSpecificProperties(arraySetConfigurationData);
  }

  /**
   * A function that makes a request to an external system to get layout data for a configurable product.
   *
   * @param {Object} options
   *   This object contains the request payload for performing get layout.
   *
   * @returns {Promise}
   *   A success/failure response.
   */
  async handleGetLayout (options) {
    const {request} = options;

    if (!this.validators.validateProductInQuery(request)) {
      throw new TranslatableError(
        'productFamily, productLine and model must be supplied.', this.constants.INVALID_PRODUCT_INPUT, 400);
    }
    else if (!this.validators.validateFlowInRequest(request)) {
      throw new TranslatableError('flow is required.', this.constants.FLOW_REQUIRED, 400);
    }
    else if (!this.validators.validateHierLevelInQuery(request)) {
      throw new TranslatableError('hierLevel must be supplied.', this.constants.HIER_LEVEL_NOT_FOUND, 400);
    }

    return this.requestDispatcher.doLayoutCacheRetrieval(request);
  }

  /**
   * A function that makes a request to an external system to get options for a pick list.
   *
   * @param {Object} options
   *   This object contains the request payload for performing get layout.
   *
   * @returns {Promise}
   *   A success/failure response.
   */
  async handleGetPickListOptions (options) {
    const {request} = options;

    if (!this.validators.validateProductInQuery(request)) {
      throw new TranslatableError(
        'productFamily, productLine and model must be supplied.', this.constants.INVALID_PRODUCT_INPUT, 400);
    }
    else if (!this.validators.validateCacheInstanceIdInRequest(request)) {
      throw new TranslatableError(
        'cacheInstanceId is required for action.', this.constants.CACHE_INSTANCE_ID_REQUIRED_FOR_ACTION, 400);
    }
    else if (!this.validators.validatePickListVarNameInRequest(request)) {
      throw new TranslatableError(
        'pickListVarName is required.', this.constants.PICK_LIST_VAR_NAME_REQUIRED, 400);
    }

    const pickListOptions = await this.requestDispatcher.doPickListGetOptions(
      request,
      request.params.cacheInstanceId,
      request.params.pickListVarName
    );

    return this.utils.removeCpqSpecificProperties(pickListOptions);
  }


  /**
   * A function that makes a request to an external system to get settings used to
   * control how a configurator UI is displayed.
   *
   * Example request payload:
   *
   * @param {Object} options
   *   This object contains the request payload for performing get layout.
   *
   * @returns {Promise}
   *   A success/failure response.
   */
  async handleGetUiSettings (options) {
    const {request} = options;

    const uiSettings = await this.requestDispatcher.doUiSettings(request);
    return this.utils.removeCpqSpecificProperties(uiSettings);
  }

  /**
   * A function that makes a request to an external system to get templates used to
   * control how BOM table and recommened parts table are displayed.
   *
   * Example request payload:
   *
   * @param {Object} options
   *   This object contains the request payload for performing get layout.
   *
   * @returns {Promise}
   *   A success/failure response.
   */
  async handleGetTemplates (options) {
    const {request} = options;

    const templates = await this.requestDispatcher.doPageTemplates(request);
    return this.utils.removeCpqSpecificProperties(templates);
  }

  //----------------------------------------
  // ABO Handlers
  //----------------------------------------

  /**
   * A function that makes a request to an external system to modify an asset.
   *
   * @param {Object} options
   *   This object contains the request payload for performing a modify action.
   */
  async handleModify (options) {
    const {request} = options;

    if (!this.validators.validateIsABOSupported(request, this.constants.SUPPORTED_ASSET_BASED_OPERATIONS)) {
      throw new TranslatableError(
        'Invalid ABO op parameter.', this.constants.INVALID_ABO_OP_Parameter, 400);
    }
    if (!this.validators.validateAssetKeyInRequest(request)) {
      throw new TranslatableError(
        'assetKey is a required parameter.', this.constants.MISSING_ASSET_KEY, 400);
    }

    return this.handleConfigure(options);
  }

  /**
   * A function that makes a request to an external system to upgrade an asset.
   *
   * @param {Object} options
   *   This object contains the request payload for performing a upgrade action.
   */
  async handleUpgrade (options) {
    const {request} = options

    if (!this.validators.validateIsABOSupported(request, this.constants.SUPPORTED_ASSET_BASED_OPERATIONS)) {
      throw new TranslatableError(
        'Invalid ABO op parameter.', this.constants.INVALID_ABO_OP_Parameter, 400);
    }
    if (!this.validators.validateAssetKeyInRequest(request)) {
      throw new TranslatableError(
        'assetKey is a required parameter.', this.constants.MISSING_ASSET_KEY, 400);
    }
    if (!this.validators.validateUpgradeNameInRequest(request)) {
      throw new TranslatableError(
        'upgradeName is a required parameter.', this.constants.MISSING_UPGRADE_NAME, 400);
    }

    return this.handleConfigure(options);
  }

}

module.exports = ConfiguratorServiceHandler;
