// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.

'use strict';

module.exports = {
  // The locale request header name.
  LOCALE_HEADER_NAME: 'X-CCAsset-Language',

  // The request header name that indicates OCC is in preview mode.
  PREVIEW_HEADER_NAME: 'x-ccaudience',

  // Locale to be used when request payload or header doesn't contain a locale.
  DEFAULT_LOCALE: 'us',

  // CPQ action endpoints.
  CONFIGURE_ACTION: '/actions/_configure',
  UPDATE_ACTION: '/actions/_update',
  ADD_TO_CART_ACTION: '/actions/_integration_addToCart',
  RECONFIGURE_ACTION: '/actions/_reconfigureClient',
  LOAD_DATA_ACTION: '/actions/_loadData',
  INTERACT_ACTION: '/actions/_interact',
  ADD_ARRAY_SET_ROW_ACTION: '/actions/_add',
  DELETE_ARRAY_SET_ROW_ACTION: '/actions/_delete',
  PAGE_ACTIONS: {
    previous: '/actions/_previous',
    next: '/actions/_next'
  },
  GETOPTIONS_ACTION: '/actions/getOptions',

  RECOMMENDED_SPARE_TEMPLATES_ACTION: '/recommended_spare_templates',

  // ABO Operations
  OP_MODIFY: "MODIFY",
  OP_UPGRADE: "UPGRADE",
  SUPPORTED_ASSET_BASED_OPERATIONS: ['MODIFY', 'UPGRADE'],

  // In certain payloads/responses CPQ array set variable names
  // are prefixed by a constant identifier
  ARRAY_SET_PREFIX: '_set',

  // Actions that are supported by the OCC-CPQ configurator integration.
  // These will be the actions visible to the shopper.
  SUPPORTED_ACTIONS: ['_update', '_next', '_previous', '_integration_addToCart'],

  // The 'expand' parameters that are supported by the OCC-CPQ configurator integration.
  // These will be the actions visible to the shopper. These reference the available
  // expand parameters that will instruct the handler to expand the corresponding linked
  // objects in the response payload.
  SUPPORTED_EXPAND_PARAMETERS: {
    all: 'all',
    layout: 'layout',
    uiSettings: 'uiSettings',
    templates: 'templates'
  },

  // Localization resource key constants.
  INTERNAL_SERVER_ERROR: 'internalServerError',
  INVALID_PRODUCT_INPUT: 'invalidProductInput',
  CACHE_INSTANCE_ID_REQUIRED_FOR_PAGE: 'cacheInstanceIdRequiredForPage',
  PAGE_OP_REQUIRED: 'pageOpRequired',
  INVALID_PAGE_OP: 'invalidPageOp',
  CACHE_INSTANCE_ID_REQUIRED_FOR_ACTION: 'cacheInstanceIdRequiredForAction',
  ARRAY_SET_VAR_NAME_REQUIRED_FOR_ACTION: 'arraySetVarNameRequiredForAction',
  MISSING_STATE_ATTRIBUTES: 'missingStateAttributes',
  LAYOUT_CACHE_NOT_FOUND: 'layoutCacheNotFound',
  REMOVE_INDEX_NOT_FOUND: 'removeIndexNotFound',
  INVALID_EXPAND_PARAMETERS: 'invalidExpandParameters',
  ARRAY_SET_VAR_NAMES_NOT_FOUND: 'arraySetVarNamesNotFound',
  FLOW_REQUIRED: 'flowRequired',
  HIER_LEVEL_NOT_FOUND: 'hierLevelNotFound',
  PICK_LIST_VAR_NAME_REQUIRED: 'pickListVarNameRequired',
  UI_SETTINGS_NOT_FOUND: 'uiSettingsNotFound',
  TEMPLATES_NOT_FOUND: 'templatesNotFound',
  INVALID_ABO_OP_PARAMETER: 'invalidAboOpParameter',
  MISSING_ASSET_KEY: 'missingAssetKey',
  MISSING_UPGRADE_NAME: 'missingUpgradeName',
  MISSING_ABO_OP_PARAMETER: 'missingAboOpParameter',

  // Node internal server error response status.
  NODE_INTERNAL_SERVER_ERROR: 'ERR_HTTP_INVALID_STATUS_CODE'
};
