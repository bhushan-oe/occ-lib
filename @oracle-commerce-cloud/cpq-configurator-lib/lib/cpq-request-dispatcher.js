// Copyright (C) 1994, 2019, Oracle and/or its affiliates. All rights reserved.

'use strict';

const TranslatableError = require('@oracle-commerce-cloud/translatable-error');
const defaultRegistry = require('../registry/registry');

const ARRAY_KEY = 'array_key';
const SET_PREFIX = '_set';
const HIER_LEVEL_PARAMETER = 'hierLevel';
const PICK_LISTS_RESOURCE = '_pickLists';

/**
 * This class contains functions for dispatching configurator requests to CPQ.
 */
class CpqRequestDispatcher {

  /**
   * Constructor
   * 
   * @param {Object} config
   *   An object that contains the configuration settings for the application.
   * @param {Boolean} config.useHTTPS 
   *   Whether or not to use the https protocol for outbound requests.
   * @param {String} config.hostname 
   *   External CPQ Configurator server hostname.
   * @param {String} config.port 
   *   External CPQ Configurator server port.
   * @param {Boolean} config.useHTTPSforPreview 
   *   Whether or not to use the https protocol for outbound preview requests.
   * @param {String} config.previewHostname 
   *   External CPQ Configurator preview server hostname.
   * @param {String} config.previewPort 
   *   External CPQ Configurator preview server port.
   * @param {Number} config.timeout 
   *   Request timeout in ms.
   * @param {Object} config.logger
   *   Used for logging.
   * @param {String} config.username_env_var 
   *   Environment variable name for basic auth username.
   * @param {String} config.password_env_var
   *   Environment variable name for basic auth password.
   * @param {String} config.CONFIG_BASE_URI
   *   The CPQ 'config' base REST URI.
   * @param {String} config.LAYOUT_BASE_URI
   *   The CPQ 'layoutCache' base REST URI.
   * @param {String} config.PAGE_TEMPLATES_BASE_URI
   *   The CPQ 'pageTemplates' base REST URI.
   * @param {String} config.CONFIG_UI_SETTINGS_BASE_URI
   *   The CPQ 'configUISettings' base REST URI.
   * @param {String} config.CONFIGURATOR_SSE_BASE_CONTEXT_URI
   *   The base URI of the SSE Configurator application.
   * @param {Object} config.registry
   *   The registry that contains the module mappings for the SSE configurator. (optional)
   */
  constructor (config) {
    if (!config) {
      throw new Error('CpqRequestDispatcher requires config to be instantiated');
    }

    // The CPQ config base URI. When invoking an action, the productFamily, productLine and
    // model and action endpoint will be appended to this. For example:
    //
    // E.g. http://<cpq_server>/rest/v8/configproductFamily1.productLine1.model1/actions/_configure
    this.CONFIG_BASE_URI = config.CONFIG_BASE_URI;

    // The CPQ layoutCache base URI. When invoking an action, the productFamily, productLine and
    // model and action endpoint will be appended to this. For example:
    //
    // E.g. http://<cpq_server>/rest/v8/layoutCache/productFamily1/productLine1/model1/model1LayoutFlow/en
    this.LAYOUT_BASE_URI = config.LAYOUT_BASE_URI;

    this.PAGE_TEMPLATES_BASE_URI = config.PAGE_TEMPLATES_BASE_URI;
    this.CONFIG_UI_SETTINGS_BASE_URI = config.CONFIG_UI_SETTINGS_BASE_URI;

    this.config = config;

    this.registry = config.registry ? config.registry : defaultRegistry;

    this.constants = this.registry.load(
      this.registry.CPQ_CONFIGURATOR_LIB_MODULE_CONSTANTS,
      this.registry.CPQ_CONFIGURATOR_LIB_PACKAGE);

    const Utils = this.registry.load(
      this.registry.CPQ_CONFIGURATOR_LIB_MODULE_UTILS,
      this.registry.CPQ_CONFIGURATOR_LIB_PACKAGE);

    this.utils = new Utils(config);

    this.layoutPropertyReducer = this.registry.load(
      this.registry.CPQ_CONFIGURATOR_LIB_MODULE_LAYOUT_PROPERTY_REDUCER,
      this.registry.CPQ_CONFIGURATOR_LIB_PACKAGE);

    const PayloadAssembler = this.registry.load(
      this.registry.CPQ_CONFIGURATOR_LIB_MODULE_PAYLOAD_ASSEMBLER,
      this.registry.CPQ_CONFIGURATOR_LIB_PACKAGE);

    this.payloadAssembler = new PayloadAssembler(config);

    const ConfiguratorRestHandler = this.registry.load(
      this.registry.CPQ_CONFIGURATOR_REST_MODULE_CONFIGURATOR_REST_HANDLER,
      this.registry.CPQ_CONFIGURATOR_REST_PACKAGE);

    this.configuratorRestHandler = new ConfiguratorRestHandler({
      useHTTPS: config.useHTTPS,
      hostname: config.hostname,
      port: config.port,
      timeout: config.timeout,
      logger: config.logger,
      username: config.username_env_var,
      password: config.password_env_var
    });

    this.previewConfiguratorRestHandler = new ConfiguratorRestHandler({
      useHTTPS: config.useHTTPSforPreview,
      hostname: config.previewHostname,
      port: config.previewPort,
      timeout: config.timeout,
      logger: config.logger,
      username: config.username_env_var,
      password: config.password_env_var
    });
  }

  /**
   * Build a request payload and invoke the CPQ _configure/_reconfigure/_next/_previous endpoint.
   *
   * @param {Object} request
   *   The request object for the action being performed.
   *
   * @returns {Promise}
   *   A resolved configure/reconfigure or next/previous action response Promise, otherwise a rejected Promise.
   */
  async doConfigure (request) {

    // The current configurator action being performed, i.e. configure, reconfigure, next or previous.
    let action;

    // When the action is an initial configure or reconfigure operation, include extra required properties.
    if (!request.body.op) {
      if (request.body.configId) {
        action = this.constants.RECONFIGURE_ACTION;
      }
      else {
        action = this.constants.CONFIGURE_ACTION;
      }
    }
    else if (request.body.op) {
      if (this.constants.PAGE_ACTIONS[request.body.op]) {
        action = this.constants.PAGE_ACTIONS[request.body.op];
      }
      else if (this.constants.SUPPORTED_ASSET_BASED_OPERATIONS.includes(request.body.op)) {
        action = this.constants.CONFIGURE_ACTION;
      }
    }

    const payload = this.payloadAssembler.assemblePayloadForAction(request, action);

    try {
      // Invoke the CPQ _configure, _reconfigure or _previous/_next endpoint.
      return await this._getRestHandler(request).post(this._buildActionRequest(request, action, payload));
    }
    catch (err) {
      this._rejectAction(err);
    }
  }

  /**
   * Build the layoutCache request and invoke the CPQ layoutCache endpoint.
   *
   * @param {Object} request
   *   The request object for the action being performed.
   * @param {Object} layoutFlowDetails
   *   This object will hold the details associated with the flow, i.e. layout flow name and hierLevel values.
   *
   * @returns {Promise}
   *   A resolved layoutCache response Promise, otherwise a rejected Promise.
   */
  async doLayoutCacheRetrieval (request, layoutFlowDetails) {
    const flow = layoutFlowDetails ? layoutFlowDetails.flow : request.params.flow;
    const hierLevel = layoutFlowDetails ? layoutFlowDetails.hierLevel : request.query.hierLevel;

    const productFamily = request.body.productFamily || request.query.productFamily;
    const productLine = request.body.productLine || request.query.productLine;
    const model = request.body.model || request.query.model;

    const locale = this.utils.getLocale(request, this.constants);

    const layoutCacheRequest = {
      uri: `${
        this.LAYOUT_BASE_URI
      }/${productFamily}/${productLine}/${model}/${flow}/${locale}`
    };

    layoutCacheRequest.queryString = `?${HIER_LEVEL_PARAMETER}=${hierLevel}`;

    try {
      // Invoke the CPQ layoutCache endpoint.
      const layoutCache = await this._getRestHandler(request).get(layoutCacheRequest);

      // Reduce the layout properties if the request contains an
      // includeLayoutProperties or includeLayoutProperties list.
      this.layoutPropertyReducer.reduceLayoutProperties(request, layoutCache);

      return layoutCache;
    }
    catch (err) {
      this._rejectAction({
        message: 'layoutCache could not be found.',
        statusCode: 404,
        resourceKey: this.constants.LAYOUT_CACHE_NOT_FOUND}
      );
    }
  }

  /**
   * Build a request payload and invoke the CPQ _update endpoint.
   *
   * @param {Object} request
   *   The request object for the action being performed.
   *
   * @returns {Promise}
   *   A resolved _update action response Promise, otherwise a rejected Promise.
   */
  async doUpdate (request) {
    const action = this.constants.UPDATE_ACTION;
    const payload = this.payloadAssembler.assemblePayloadForAction(request, action);

    try {
      // Invoke the CPQ _update endpoint.
      return await this._getRestHandler(request).post(this._buildActionRequest(request, action, payload));
    }
    catch (err) {
      this._rejectAction(err);
    }
  }

  /**
   * Build a request payload and invoke the CPQ _interact endpoint.
   *
   * @param {Object} request
   *   The request object for the action being performed.
   *
   * @returns {Promise}
   *   A resolved _interact action response Promise, otherwise a rejected Promise.
   */
  async doInteract (request) {
    const action = this.constants.INTERACT_ACTION;
    const payload = this.payloadAssembler.assemblePayloadForAction(request, action);

    try {
      // Invoke the CPQ _interact endpoint.
      return await this._getRestHandler(request).post(this._buildActionRequest(request, action, payload));
    }
    catch (err) {
      this._rejectAction(err);
    }
  }

  /**
   * Build the loadData request and invoke the CPQ _loadData endpoint.
   *
   * @param {Object} request
   *   The request object for the action being performed.
   * @param {Object} cacheInstanceId
   *   The cacheInstanceId for the configuration being loaded.
   * @param {Object} arraySetVarNames
   *   The arraySet variable names that should be included in the configuration.
   *
   * @returns {Promise}
   *   A resolved loadData response Promise, otherwise a rejected Promise.
   */
  async doLoadData (request, cacheInstanceId, arraySetVarNames) {
    const action = this.constants.LOAD_DATA_ACTION;
    const payload = this.payloadAssembler.assemblePayloadForAction(request, action);
    payload.cacheInstanceId = cacheInstanceId;

    for (let i = 0, len = arraySetVarNames.length; i < len; i++) {
      const arraySetVariableName = arraySetVarNames[i];

      // to ignore (as they are not "real" array sets)? For now assuming that the
      // presence of "array_key" in the variable name means it should not be passed
      // in the loadData payload
      if (!arraySetVariableName.includes(ARRAY_KEY)) {
        payload.criteria.childDefs.push({
          name: `${SET_PREFIX}${arraySetVariableName}`,
          queryDef: {
            offset: 0,
            state: true,
            totalResults: true
          }
        });
      }
    }

    try {
      // Invoke the CPQ _loadData endpoint.
      return await this._getRestHandler(request).post(this._buildActionRequest(request, action, payload));
    }
    catch (err) {
      this._rejectAction(err);
    }
  }

  /**
   * Retrieves the options associated with a pick list.
   *
   * @param {Object} request
   *   The request object for the action being performed.
   * @param {Object} cacheInstanceId
   *   The cacheInstanceId for the configuration being loaded.
   * @param {Object} pickListVarName
   *   The pick list variable name. (optional)
   *
   * @returns {Promise}
   *   A resolved get options response Promise, otherwise a rejected Promise.
   */
  async doPickListGetOptions (request, cacheInstanceId, pickListVarName) {
    const productFamily = request.body.productFamily || request.query.productFamily;
    const productLine = request.body.productLine || request.query.productLine;
    const model = request.body.model || request.query.model;

    pickListVarName = pickListVarName ? '/' + pickListVarName : '';

    const getOptionsRequest = {
      uri: `${this.CONFIG_BASE_URI
        }${productFamily
        }.${productLine
        }.${model}/${PICK_LISTS_RESOURCE}${pickListVarName
        }${this.constants.GETOPTIONS_ACTION}`,
      payload: {
        cacheInstanceId
      }
    };

    try {
      // Invoke the CPQ getOptions endpoint.
      return await this._getRestHandler(request).post(getOptionsRequest);
    }
    catch (err) {
      this._rejectAction(err);
    }
  }

  /**
   * Build a _integration_addToCart request payload and invoke the CPQ _integration_addToCart endpoint.
   *
   * @param {Object} request
   *   The request object for the action being performed.
   *
   * @returns {Promise}
   *   A resolved _integration_addToCart action response Promise, otherwise a rejected Promise.
   */
  async doAddToCart (request) {
    const payload = {
      cacheInstanceId: request.params.cacheInstanceId
    };

    try {
      // Invoke the CPQ add to cart endpoint.
      return await this._getRestHandler(request).post(
        this._buildActionRequest(request, this.constants.ADD_TO_CART_ACTION, payload)
      );
    }
    catch (err) {
      this._rejectAction(err);
    }
  }

  /**
   * Build a _setmyArraySet/actions/add|delete request payload and invoke the CPQ endpoint.
   *
   * @param {Object} request
   *   The request object for the action being performed.
   * @param {String} productFamily
   *   The product family of the configuration being worked on.
   * @param {String} productLine
   *   The product line of the configuration being worked on.
   * @param {String} model
   *   The model of the configuration being worked on.
   * @param {String} arraySetVarName
   *   The variable name of the array set to be updated.
   * @param {String} action
   *   The action to be perfomed on the array set.
   *
   * @returns {Promise}
   *   A resolved _setmyArraySet/actions/add|delete action response Promise, otherwise a rejected Promise.
   */
  async doArraySetRowAction (
    request,
    productFamily,
    productLine,
    model,
    arraySetVarName,
    action) {

    const arraySetRowRequest = {
      uri: `${
        this.CONFIG_BASE_URI
      }${productFamily}.${productLine}.${model}/${SET_PREFIX}${arraySetVarName}${action}`,
      payload: {
        cacheInstanceId: request.params.cacheInstanceId
      }
    };

    if (action === this.constants.DELETE_ARRAY_SET_ROW_ACTION) {
      arraySetRowRequest.payload.removeIndex = parseInt(request.query.removeIndex, 10);
    }

    try {
      // Invoke the CPQ add array set row endpoint.
      return await this._getRestHandler(request).post(arraySetRowRequest);
    }
    catch (err) {
      this._rejectAction(err);
    }
  }

  /**
   * Build a uiSettings request payload and invoke the CPQ configUISettings endpoint.
   *
   * @param {Object} request
   *   The request object for the action being performed.
   *
   * @returns {Promise}
   *   A resolved UI settings response Promise, otherwise a rejected Promise.
   */
  async doUiSettings (request) {

    const uiSettingsRequest = {
      uri: this.CONFIG_UI_SETTINGS_BASE_URI,
      queryString: ''
    };

    try {
      // Invoke the CPQ configUISettings endpoint.
      return await this._getRestHandler(request).get(uiSettingsRequest);
    }
    catch (err) {
      this._rejectAction({
        message: 'uiSettings could not be found.',
        statusCode: 404, 
        resourceKey: this.constants.UI_SETTINGS_NOT_FOUND}
      );
    }
  }

  /**
   * Build a pageTemplates request payload and invoke the CPQ pageTemplates endpoint.
   *
   * @param {Object} request
   *   The request object for the action being performed.
   *
   * @returns {Promise}
   *   A resolved page templates response Promise, otherwise a rejected Promise.
   */
  async doPageTemplates (request) {

    const pageTemplatesRequest = {
      uri: `${this.PAGE_TEMPLATES_BASE_URI}${this.constants.RECOMMENDED_SPARE_TEMPLATES_ACTION}`,
      queryString: ''
    };

    try {
      // Invoke the CPQ getOptions endpoint.
      return await this._getRestHandler(request).get(pageTemplatesRequest);
    }
    catch (err) {
      this._rejectAction({
        message: 'templates could not be found.',
        statusCode: 404, 
        resourceKey: this.constants.TEMPLATES_NOT_FOUND}
      );
    }
  }

  // ------------------------------------------------------------
  // PRIVATE FUNCTIONS
  // ------------------------------------------------------------

  /**
   * Assemble an action request to be passed to the CPQ configurator API.
   *
   * @private
   *
   * @param {Object} request
   *   The request containing the productFamily, productLine and model.
   * @param {Object} action
   *   The action to be performed in the current request.
   * @param {Object} payload
   *   The appropriate payload to be passed for the action.
   *
   * @returns {Object}
   *   An valid action request.
   */
  _buildActionRequest (request, action, payload) {
    const productFamily = request.body.productFamily || request.query.productFamily;
    const productLine = request.body.productLine || request.query.productLine;
    const model = request.body.model || request.query.model;

    return {
      uri: `${this.CONFIG_BASE_URI}${productFamily}.${productLine}.${model}${action}`,
      payload
    };
  }

  /**
   * Function that updates a response with a given error and returns that error in a Promise.reject.
   *
   * @private
   *
   * @param {Object} response
   *   The response that will be updated with the given error.
   * @param {Object} error
   *   An error that will be added to the response.
   *
   * @returns {Promise}
   *   A rejected Promise with the given error.
   */
  _rejectAction (error) {
    let statusCode;
    let resourceKey;
    let message;

    if (error.responseData && error.responseCode) {

      // The SSE server will swallow the error message when the status code is 404 or 500.
      // Return a 400 (bad request) status code in these cases so that error message will be retained.
      statusCode = error.responseCode === 500 || error.responseCode === 404 ? 400 : error.responseCode;

      if (error.responseData.error) {
        message = error.responseData.error;
      }
      else if (error.responseData['o:errorDetails'] &&
               error.responseData['o:errorDetails'][0] &&
               error.responseData['o:errorDetails'][0].title) {

        message = error.responseData['o:errorDetails'][0].title;
      }
      else if (error.responseData.title) {
        message = error.responseData.title;
      }
      else if (error.responseData.message) {
        message = error.responseData.message;
      }
    }
    else {
      message = error.message;
      statusCode = error.statusCode;
      resourceKey = error.resourceKey;
    }

    throw new TranslatableError(message, resourceKey, statusCode);
  }

  /**
   * Retrieves the rest handler to be used to perform CPQ configurator actions.
   *
   * @param {Object} request
   *   The request object that will be checked for the existence of a preview header.
   *
   * @returns {Object}
   *   A ConfigurationRestHandler that has been configured for a production CPQ server
   *   or one that has been configured for a CPQ preview/staging/test server.
   */
  _getRestHandler (request) {
    return request.rawHeaders && request.rawHeaders.includes(this.constants.PREVIEW_HEADER_NAME) ?
      this.previewConfiguratorRestHandler :
      this.configuratorRestHandler;
  }

}

module.exports = CpqRequestDispatcher;
