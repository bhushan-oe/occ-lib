const ExternalValidationUtils = require('../app/external-validation-utils').ExternalValidationUtils;
const testData = require('./test-data');
let utils = new ExternalValidationUtils();
utils.config = {
  "useHTTPS": false,
  "OIC_Hostname": "kkm00bvz.in.oracle.com",
  "OIC_port": "7003",
  "timeout": 50000,
  "username_env_var": "weblogic",
  "password_env_var": "welcome2",
  "GET_CONFIG_URI": "/ic/api/integration/v1/flows/rest/CPQ_GETCONFIG/4.0/",
  "GET_CONFIGBOM_URI": "/ic/api/integration/v1/flows/rest/OCC_CPQ_GETCONFIGB/1.0/"
}

describe("External Validation utils", function () {

  it("create http request configurations for OIC (getConfiguration)", function () {
    let oicConfigurations = {};
    oicConfigurations.useHTTPS = utils.config.useHTTPS;
    oicConfigurations.hostname = utils.config.OIC_Hostname;
    oicConfigurations.port = utils.config.OIC_port;
    oicConfigurations.timeout = utils.config.timeout;
    oicConfigurations.username = utils.config.username_env_var;
    oicConfigurations.password = utils.config.password_env_var;
    oicConfigurations.path_getConfigBOM = utils.config.GET_CONFIGBOM_URI;
    oicConfigurations.path_getConfig = utils.config.GET_CONFIG_URI;
    const basicAuthHeader = "Basic " + Buffer.from(oicConfigurations.username + ':' + oicConfigurations.password).toString("base64");

    let expectedResponse = {
      protocol: 'http:',
      hostname: 'kkm00bvz.in.oracle.com',
      path: '/ic/api/integration/v1/flows/rest/CPQ_GETCONFIG/4.0/',
      port: '7003',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': basicAuthHeader
      }
    }
    let response = utils.getHttpRequestConfigurations(oicConfigurations, oicConfigurations.path_getConfig);
    expect(response).toEqual(expectedResponse);
  });

  it("get heirarchichal structure for OIC response of getConfigurations", function () {
    spyOn(utils, 'flatStructureToHierarchy').and.callThrough();
    let result = utils.getHierarchicalStructure(testData.getConfigurationsResponse);

    testData.getConfigurationsResponse.forEach(function (item) {
      expect(Object.keys(item)).toContain('configuratorItems');
      item.configuratorItems.forEach(function (i) {
        expect(i.statusCode).not.toEqual(5103);
        expect(utils.flatStructureToHierarchy).toHaveBeenCalledWith(i);
      });
    });
    expect(result).toEqual(testData.expectedHeirarchichalStructure);
  });

  it("Convert flat structure from OIC to heirarchy", function () {
    spyOn(utils, "constructChildItemsStucture").and.callThrough();
    let parentToChildrenMap = {};
    let configuratorItem = testData.getConfigurationsResponse[0].configuratorItems[0];
    let result = utils.flatStructureToHierarchy(configuratorItem);
    let childItems = configuratorItem.childItems;
    expect(configuratorItem.id).not.toBe(null);
    let rootItem = configuratorItem.id;
    expect(rootItem).toEqual("BOM_laptopRoot");
    for (let childItemIdx in childItems) {
      let childItem = childItems[childItemIdx];
      let parentId = childItem.parentId;
      if (!parentId) {
        parentId = rootItemId;
      }
      if (!parentToChildrenMap[parentId]) {
        parentToChildrenMap[parentId] = [];
      }
      parentToChildrenMap[parentId].push(childItem);
    }
    expect(parentToChildrenMap).toEqual(testData.expectedParentToChildrenMap);
    expect(utils.constructChildItemsStucture).toHaveBeenCalledWith(configuratorItem, parentToChildrenMap);
    expect(result).toEqual(testData.configuratorItemsHeirarchy);
  });

  it("construct child items structure recursively", function () {
    spyOn(utils, "constructChildItemsStucture").and.callThrough();
    let configuratorItem = testData.getConfigurationsResponse[0].configuratorItems[0];
    utils.constructChildItemsStucture(configuratorItem, testData.expectedParentToChildrenMap);
    let childItems = testData.expectedParentToChildrenMap[configuratorItem.id];
    expect(childItems).not.toBe(null);
    configuratorItem.childItems = [];
    for (var item in childItems) {
      configuratorItem.childItems.push(childItems[item]);
      expect(utils.constructChildItemsStucture).toHaveBeenCalledWith(childItems[item], testData.expectedParentToChildrenMap);
    }
  });

})