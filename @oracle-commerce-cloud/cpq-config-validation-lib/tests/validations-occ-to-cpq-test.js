const constants = require('../app/constants');
const testData = require('./test-data');
const ValidationOccToCPQ = require('../validations/validations-occ-to-cpq').ValidationOccToCPQ;
let validationUtils = new ValidationOccToCPQ();

describe("Validation of data, occ against cpq", function () {
  it("create configurationId item object to map", function () {
    let commerceItemsRes = testData.expectedHeirarchichalStructure;
    let configIdMap = validationUtils.createConfiguratorIdToItemObjectMap(commerceItemsRes);
    expect(configIdMap).toEqual(testData.configIdMap);
  });
  it("Validate the commerce items", function () {
    spyOn(validationUtils, 'createConfiguratorIdToItemObjectMap').and.callThrough();
    spyOn(validationUtils, 'validateSingleCommerceItem').and.callThrough();
    spyOn(validationUtils, 'validateExternalPricesAndRecurringCharges').and.callThrough();
    spyOn(validationUtils, 'validateABOProperties').and.callThrough();
    let commerceItemsReq = testData.webhookRequestPayload_cpqItems.order.shoppingCart.items;
    let commerceItemsRes = testData.expectedHeirarchichalStructure;
    validationUtils.validateCommerceItems(commerceItemsReq, commerceItemsRes);
    expect(validationUtils.createConfiguratorIdToItemObjectMap).toHaveBeenCalledWith(commerceItemsRes);
    let configIdMap = validationUtils.createConfiguratorIdToItemObjectMap(commerceItemsRes);
    for (let itemIdx in commerceItemsReq) {
      let commerceItemId = commerceItemsReq[itemIdx].id || commerceItemsReq[itemIdx].commerceItemId || commerceItemsReq[itemIdx].commerceId;
      let matchingItemInResponse = configIdMap[commerceItemsReq[itemIdx].configuratorId];
      let errorTracker = [];
      expect(commerceItemId).toEqual("ci4000801");
      expect(matchingItemInResponse).toBeTruthy();
      expect(matchingItemInResponse.statusCode).not.toEqual(constants.ITEM_FAILURE);
      matchingItemInResponse.commerceItemId = commerceItemId;
      expect(validationUtils.validateSingleCommerceItem).toHaveBeenCalledWith(commerceItemsReq[itemIdx], matchingItemInResponse);
      errorTracker = validationUtils.validateSingleCommerceItem(commerceItemsReq[itemIdx], matchingItemInResponse);
      checkValidationInvocations(validationUtils, commerceItemsReq[itemIdx], matchingItemInResponse);
      expect(errorTracker.length).toEqual(0);
      expect(matchingItemInResponse.statusCode).toEqual(constants.ITEM_SUCCESS);
    }
    //Since we have 10 items, these methods should be called atleast 10 times
    expect(validationUtils.validateExternalPricesAndRecurringCharges.calls.count()).toBeGreaterThanOrEqual(10);
    expect(validationUtils.validateABOProperties.calls.count()).toBeGreaterThanOrEqual(10);
  });
  it("validate a single commerceItem", function(){
    let commerceItemReq = testData.webhookRequestPayload_cpqItems.order.shoppingCart.items[0];
    let commerceItemRes = testData.expectedHeirarchichalStructure[0];
    let errorTracker = [];
    spyOn(validationUtils, 'validateCommerceItemProperties').and.callThrough();
    spyOn(validationUtils, 'validateChildItems').and.callThrough();
    errorTracker = validationUtils.validateSingleCommerceItem(commerceItemReq, commerceItemRes);
    expect(commerceItemReq.catRefId).toBeTruthy();
    expect(commerceItemRes.catRefId).toBeTruthy();
    expect(commerceItemReq.catRefId).toEqual(commerceItemRes.catRefId);
    expect(commerceItemReq.childItems).toBeTruthy();
    expect(commerceItemRes.childItems).toBeTruthy();
    expect(commerceItemReq.childItems.length).toEqual(commerceItemRes.childItems.length);
    expect(validationUtils.validateCommerceItemProperties).toHaveBeenCalledWith(commerceItemReq, commerceItemRes);
    errorTracker.push.apply(errorTracker, validationUtils.validateCommerceItemProperties(commerceItemReq, commerceItemRes));
    console.log(errorTracker);
    expect(errorTracker.length).toEqual(0);
    expect(commerceItemReq.childItems).toBeTruthy();
    expect(commerceItemRes.childItems).toBeTruthy();
    expect(validationUtils.validateChildItems).toHaveBeenCalledWith(commerceItemReq.childItems, commerceItemRes.childItems);
    expect(errorTracker.length).toEqual(0);
  });
  it("validate commerce item properties", function(){
    let errorTracker = [];
    let commerceItemReq = testData.webhookRequestPayload_cpqItems.order.shoppingCart.items[0];
    let commerceItemRes = testData.expectedHeirarchichalStructure[0];
    spyOn(validationUtils, 'validateExternalPricesAndRecurringCharges').and.callThrough();
    spyOn(validationUtils, 'validateABOProperties').and.callThrough();
    errorTracker = validationUtils.validateCommerceItemProperties(commerceItemReq, commerceItemRes);
    expect(validationUtils.validateExternalPricesAndRecurringCharges).toHaveBeenCalledWith(commerceItemReq, commerceItemRes, errorTracker);
    expect(validationUtils.validateABOProperties).toHaveBeenCalledWith(commerceItemReq, commerceItemRes, errorTracker);
    expect(errorTracker.length).toEqual(0);
  });
  it("Validate ABO properties", function(){
    let errorTracker = [];
    let commerceItemReq = testData.webhookRequestPayload_cpqItems.order.shoppingCart.items[0];
    let commerceItemRes = testData.expectedHeirarchichalStructure[0];
    validationUtils.validateABOProperties(commerceItemReq, commerceItemRes, errorTracker);
    expect(commerceItemReq.assetKey).toBeFalsy();
    expect(commerceItemRes.actionCode).toEqual(commerceItemReq.actionCode);
    expect(commerceItemReq.activationDate).toBeFalsy();
    expect(commerceItemRes.activationDate).toBeFalsy();
    expect(commerceItemReq.deactivationDate).toBeFalsy();
    expect(commerceItemRes.deactivationDate).toBeFalsy();
    expect(errorTracker.length).toEqual(0);
  });
  it("validate External Prices And Recurring Charges", function() {
    let errorTracker = [];
    let commerceItemReq = testData.webhookRequestPayload_cpqItems.order.shoppingCart.items[0];
    let commerceItemRes = testData.expectedHeirarchichalStructure[0];
    validationUtils.validateExternalPricesAndRecurringCharges(commerceItemReq, commerceItemRes, errorTracker);
    expect(commerceItemReq.externalPriceQuantity).toEqual(-1);
    expect(commerceItemReq.externalPrice).toEqual(commerceItemRes.externalPrice);
    expect(commerceItemReq.externalRecurringCharge).not.toEqual(commerceItemRes.externalRecurringCharge);
    expect(commerceItemReq.externalRecurringChargeFrequency).not.toEqual(commerceItemRes.externalRecurringChargeFrequency);
    expect(commerceItemReq.externalRecurringChargeDuration).not.toEqual(commerceItemRes.externalRecurringChargeDuration);
    expect(errorTracker.length).toEqual(0);
  });

  it("Validate Finding All CommerceItems With Same catRefId", function() {
    let commerceItemsRes = testData.expectedHeirarchichalStructure[0].childItems[1].childItems;
    let catRefId = "sku40008";

    let result = validationUtils.findCommerceItemsWithCatRefId(commerceItemsRes, catRefId);

    let itemsArray = [];
    for (let itemIdx in commerceItemsRes) {
      if(commerceItemsRes[itemIdx].catRefId == catRefId) {
        itemsArray.push(commerceItemsRes[itemIdx]);
      }
    }

    expect(result).toEqual(itemsArray);
  });

  it("Validate Finding All CommerceItems With catRefId when catRefId doesn't exist in the response object", function () {
    let commerceItemsRes = testData.expectedHeirarchichalStructure[0].childItems[1].childItems;
    let catRefId = "sku40001234";

    let result = validationUtils.findCommerceItemsWithCatRefId(commerceItemsRes, catRefId);

    expect(result).toEqual([]);
  });

  //--------------------------
  // Helper Methods
  //--------------------------

  /**
  * Gets called recursively and verifies that for every child item, external prices, recurring charges and abo properties are validated.
  * Spy on the required methods is done in calling method.
  * @param {object} validationUtils - required. Updated class with spy applied on methods.
  * @param {object} requestItem - required. Item in the request payload.
  **/
  function checkValidationInvocations(validationUtils, requestItem) {
    expect(validationUtils.validateExternalPricesAndRecurringCharges).toHaveBeenCalledWith(requestItem, jasmine.any(Object), jasmine.any(Object));
    expect(validationUtils.validateABOProperties).toHaveBeenCalledWith(requestItem, jasmine.any(Object), jasmine.any(Object));
    if(requestItem.childItems && requestItem.childItems.length > 0) {
      for (let itemIdx in requestItem.childItems) {
        checkValidationInvocations(validationUtils, requestItem.childItems[itemIdx]);
      }
    }    
  }
});
