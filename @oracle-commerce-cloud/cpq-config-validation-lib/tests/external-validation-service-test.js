const constants = require('../app/constants');
const testData = require('./test-data');
const ExternalValidationService = require('../app/external-validation-service').ExternalValidationService;
let validationService = new ExternalValidationService({
  "useHTTPS": false,
  "OIC_Hostname": "kkm00bvz.in.oracle.com",
  "OIC_port": "7003",
  "timeout": 50000,
  "username_env_var": "weblogic",
  "password_env_var": "welcome2",
  "GET_CONFIG_URI": "/ic/api/integration/v1/flows/rest/CPQ_GETCONFIG/4.0/",
  "GET_CONFIGBOM_URI": "/ic/api/integration/v1/flows/rest/OCC_CPQ_GETCONFIGB/1.0/"
});

const url = "/ccstorex/custom/v1/validateCPQConfigurations";
describe("External Validation Service", function () {
  it("Segregate the order items  with regular item with external prices", function () {
    spyOn(validationService, 'buildResponse').and.callThrough();
    let httpResponse = {};
    let callBack = function(){};
    validationService.filteringOrderItems(testData.webhookRequestPayload_regularItems, url, httpResponse, callBack);
    let items = testData.webhookRequestPayload_regularItems.order.shoppingCart.items;
    let configurationArray = [];
    let configurationBOMArray = [];
    items.forEach(function(item){
      expect(item.asset).toBeFalsy();
      expect(item.configuratorId).toBeFalsy();
    });
    expect(configurationArray.length).toEqual(0);
    expect(configurationBOMArray.length).toEqual(0);
    items.forEach(function (item) {
      item.statusCode = constants.ITEM_SUCCESS;
      item.messages = []
    });
    expect(validationService.buildResponse).toHaveBeenCalledWith(items);
  });
  it("Segregate the order items  with CPQ items", function (done) {
    spyOn(validationService.utils, 'getHttpRequestConfigurations').and.callThrough();
    spyOn(validationService.utils, "callIntegrationAPI").and.returnValue(new Promise(function (resolve, reject) {
      resolve(testData.getConfigurationsResponse);
    }));
    spyOn(validationService.utils, 'getHierarchicalStructure').and.callThrough();
    spyOn(validationService.validationUtils, 'validateCommerceItems').and.callThrough();
    let httpResponse = {};
    let callBack = function () { }; 
    validationService.filteringOrderItems(testData.webhookRequestPayload_cpqItems, url, httpResponse, callBack);
    let items = testData.webhookRequestPayload_cpqItems.order.shoppingCart.items;
    let configurationArray = [];
    items.forEach(function (item) {
      expect(item.asset).toBeFalsy();
      expect(item.configuratorId).toBeTruthy();
      expect(item.configuratorId).toEqual("36768637");
      configurationArray.push(item.configuratorId);
    });
    expect(configurationArray.length).toBeGreaterThan(0);
    expect(validationService.utils.getHttpRequestConfigurations).toHaveBeenCalledWith(validationService, validationService.path_getConfig);
    let configRequest = validationService.utils.getHttpRequestConfigurations(validationService, validationService.path_getConfig);
    let configurationPayload = {
      "locale": testData.webhookRequestPayload_cpqItems.profile.locale,
      "currencyCode": testData.webhookRequestPayload_cpqItems.currencyCode,
      "operation": "externalPricing",
      "orderId": testData.webhookRequestPayload_cpqItems.order.orderId,
      "configuratorIds": configurationArray
    }
    expect(configurationPayload).toEqual(testData.configurationPayload);
    let params = {
      "httpsRequestOptions": configRequest,
      "requestPayload": configurationPayload
    };
    validationService.utils.callIntegrationAPI(params).then(function (result) {
      expect(result).toEqual(testData.getConfigurationsResponse);     
    }).catch((err) => {
      fail(err);
    }).then(() => {
      done();
      expect(validationService.utils.getHierarchicalStructure).toHaveBeenCalled();
      expect(validationService.validationUtils.validateCommerceItems).toHaveBeenCalled();
    }, done);
  });
  it("Segregate the order items  with CPQ items and regular items", function (done) {
    spyOn(validationService.utils, 'getHttpRequestConfigurations').and.callThrough();
    spyOn(validationService.utils, "callIntegrationAPI").and.returnValue(new Promise(function (resolve, reject) {
      resolve(testData.getConfigurationsResponse);
    }));
    spyOn(validationService.utils, 'getHierarchicalStructure').and.callThrough();
    spyOn(validationService.validationUtils, 'validateCommerceItems').and.callThrough();
    let httpResponse = {};
    let callBack = function () { };
    validationService.filteringOrderItems(testData.webhookRequestPayload_regular_cpq_items, url, httpResponse, callBack);
    let items = testData.webhookRequestPayload_regular_cpq_items.order.shoppingCart.items;
    let configurationArray = [];
    expect(items[0].asset).toBeFalsy();
    expect(items[0].configuratorId).toBeTruthy();
    configurationArray.push(items[0].configuratorId);
    expect(items[1].asset).toBeFalsy();
    expect(items[1].configuratorId).toBeFalsy();
    expect(configurationArray.length).toBeGreaterThan(0);
    expect(validationService.utils.getHttpRequestConfigurations).toHaveBeenCalledWith(validationService, validationService.path_getConfig);
    let configRequest = validationService.utils.getHttpRequestConfigurations(validationService, validationService.path_getConfig);
    let configurationPayload = {
      "locale": testData.webhookRequestPayload_cpqItems.profile.locale,
      "currencyCode": testData.webhookRequestPayload_cpqItems.currencyCode,
      "operation": "externalPricing",
      "orderId": testData.webhookRequestPayload_cpqItems.order.orderId,
      "configuratorIds": configurationArray
    }
    expect(configurationPayload).toEqual(testData.configurationPayload);
    let params = {
      "httpsRequestOptions": configRequest,
      "requestPayload": configurationPayload
    };
    validationService.utils.callIntegrationAPI(params).then(function (result) {
      expect(result).toEqual(testData.getConfigurationsResponse);
    }).catch((err) => {
      fail(err);
    }).then(() => {
      done();
      expect(validationService.utils.getHierarchicalStructure).toHaveBeenCalled();
      expect(validationService.validationUtils.validateCommerceItems).toHaveBeenCalled();
    }, done);
  });
});