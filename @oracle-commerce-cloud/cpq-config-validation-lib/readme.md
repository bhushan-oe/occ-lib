# cpq-config-validation-lib
> A server side extension used to provide Oracle Commerce Cloud with validation of the configurable data.

## Introduction
This is a library designed to be used with server side extension. It provides the ES6 classes with the default functionality which can be further extended. The library is developed to perform the validation of the data coming from Oracle Cloud Commerce against an external system.

## Prerequisites
The prerequisites required are
* Oracle Commerce Cloud (Minimum version: 19B) 
* A CRM system.
* A CPQ system.
* Oracle Integration Cloud (OIC).
* Set up the external pricing manager webhook to point to the SSE URL

## Install
The library should be copied to the node_modules directory of the server side extension.

## Usage

The ExternalValidationService class provides the Validation functionality. The main method filteringOrderItems should be invoked with following parameters. 

httpRequest body - The HTTP request body nothing but OrderRequest, 
originalUrl - The url of the endpoint invoked, 
httpResponse - The HTTP response object - not populated. callback used instead to populate repsonse. 
callback - callback method that gets invoked to pass the response(error or result)

## Warning
The source code of this library should not be changed
