// Copyright © 1994, 2019, Oracle and/or its affiliates. All rights reserved.

'use strict';
const constants = require("./../app/constants")

/**
 * Class for validating the data
 * sent from CPQ with the one received from OCC
 */

class ValidationOccToCPQ {

  /**
  * This method verifies the external recurring charge relates fields for ABO orders
  * @param {object} commerceItemReq - required. The request from the OCC
  * @param {object} commerceItemRes - required. The response received from the CPQ
  * @param {object} errorTracker - required. The error stack
  **/
  validateExternalPricesAndRecurringCharges(commerceItemReq, commerceItemRes, errorTracker) {
    let commerceItemId = commerceItemReq.id || commerceItemReq.commerceItemId;
    if (commerceItemReq.externalPriceQuantity != constants.EXTERNAL_PRICE_QUANTITY_RESPONSE) {
      errorTracker.push(commerceItemId + constants.EXTERNAL_PRICE_QUANTITY_FAILED);
    }
    if (commerceItemReq.externalPrice != commerceItemRes.externalPrice) {
      errorTracker.push(commerceItemId + constants.EXTERNAL_PRICE_VALIDATION_FAILED);
    }

    if (commerceItemReq.externalRecurringCharge != commerceItemRes.externalRecurringCharge) {
      errorTracker.push(commerceItemId + constants.EXTERNAL_RECURRING_CHARGE_PRICE_VALIDATION_FAILED);
    }

    if (commerceItemReq.externalRecurringChargeFrequency != commerceItemRes.externalRecurringChargeFrequency) {
      errorTracker.push(commerceItemId + constants.EXTERNAL_RECURRING_CHARGE_FREQUENCY_VALIDATION_FAILED);
    }

    if (commerceItemReq.externalRecurringChargeDuration != commerceItemRes.externalRecurringChargeDuration) {
      errorTracker.push(commerceItemId + constants.EXTERNAL_RECURRING_CHARGE_DURATION_VALIDATION_FAILED);
    }
  }

  /**
  * This method verifies the ABO properties
  * @param {object} commerceItemReq - required. The request from the OCC
  * @param {object} commerceItemRes - required. The response received from the CPQ
  * @param {object} errorTracker - required. The error stack
  **/
  validateABOProperties(commerceItemReq, commerceItemRes, errorTracker) {
    let commerceItemId = commerceItemReq.id || commerceItemReq.commerceItemId;

    //when assetable is true, the assetKey would be sent from OCC otherwise the assetKey would be null
    if (commerceItemReq.assetKey) {
      //check only if asset key is available from OCC which is only when the commerceItem is assetable
      if (commerceItemReq.assetKey != commerceItemRes.assetKey) {
        errorTracker.push(commerceItemId + constants.ASSET_KEY_VALIDATION_FAILED);
      }
    }

    if ((commerceItemReq.actionCode && !commerceItemRes.actionCode)
      || (!commerceItemReq.actionCode && commerceItemRes.actionCode)
      || (commerceItemReq.actionCode && commerceItemRes.actionCode && commerceItemReq.actionCode.toLowerCase() != commerceItemRes.actionCode.toLowerCase())) {
      errorTracker.push(commerceItemId + constants.ACTION_CODE_VALIDATION_FAILED);
    }

    if ((commerceItemReq.activationDate && !commerceItemRes.activationDate)
      || (!commerceItemReq.activationDate && commerceItemRes.activationDate)) {
      errorTracker.push(commerceItemId + constants.ACTIVATION_DATE_VALIDATION_FAILED);
    }
    else if (commerceItemReq.activationDate && commerceItemRes.activationDate) {
      let reqActivationDate = new Date(commerceItemReq.activationDate);
      let resActivationDate = new Date(commerceItemRes.activationDate);
      if (reqActivationDate.valueOf() != resActivationDate.valueOf()) {
        errorTracker.push(commerceItemId + constants.ACTIVATION_DATE_VALIDATION_FAILED);
      }
    }

    if ((commerceItemReq.deactivationDate && !commerceItemRes.deactivationDate)
      || (!commerceItemReq.deactivationDate && commerceItemRes.deactivationDate)) {
      errorTracker.push(commerceItemId + constants.DEACTIVATION_DATE_VALIDATION_FAILED);
    }
    else if (commerceItemReq.deactivationDate && commerceItemRes.deactivationDate) {
      let reqDeactivationDate = new Date(commerceItemReq.deactivationDate);
      let resDeactivationDate = new Date(commerceItemRes.deactivationDate);
      if (reqDeactivationDate.valueOf() != resDeactivationDate.valueOf()) {
        errorTracker.push(commerceItemId + constants.DEACTIVATION_DATE_VALIDATION_FAILED);
      }
    }
  }

  /**
  * This method checks if the child items are available for validation
  * @param {object} commerceItemReq - required. The request from the OCC
  * @param {object} commerceItemRes - required. The response received from the CPQ
  * @param {object} errorTracker - required. The error stack
  **/
  validateCommerceItemProperties(commerceItemReq, commerceItemRes) {
    let errorList = [];
    this.validateExternalPricesAndRecurringCharges(commerceItemReq, commerceItemRes, errorList)
    this.validateABOProperties(commerceItemReq, commerceItemRes, errorList);
    return errorList;
  }

  /**
  * This method is a utility function fetch the commerceItems corresponding to same catRefId
  * @param {object} commerceItems 
  * @param {object} catRefId
  **/
  findCommerceItemsWithCatRefId(commerceItems, catRefId) {
    return commerceItems.filter(item => item.catRefId === catRefId);;
  }

  /**
  * This method validates the childItems, if any for a commerceItem
  * @param {object} commerceItemReq - required. The request from the OCC
  * @param {object} commerceItemRes - required. The response received from the CPQ
  * @param {object} errorTracker - required. The error stack
  **/
  validateChildItems(commerceItemsReq, commerceItemsRes) {
    let responseItems = {};
    for (let itemIdx in commerceItemsReq) {
      if(!responseItems.hasOwnProperty(commerceItemsReq[itemIdx].catRefId)) {
        responseItems[commerceItemsReq[itemIdx].catRefId] = this.findCommerceItemsWithCatRefId(commerceItemsRes, commerceItemsReq[itemIdx].catRefId);
      }
    }
    for (let itemIdx in commerceItemsReq) {
      // A list of validation errors for this particular commerceItem.
      let errors = [];
      let items = responseItems[commerceItemsReq[itemIdx].catRefId];
      let commerceItemId = commerceItemsReq[itemIdx].id || commerceItemsReq[itemIdx].commerceItemId;
      if(items.length === 0) {
        errors.push(commerceItemId + constants.CATREFID_VALIDATION);
      }
      for (let index in items) {
        if (!items[index].validated) {
          // If one item of the catRefId fails validation, try with another item.
          errors = [];

          if (commerceItemsReq[itemIdx].quantity != items[index].quantity) {
            errors.push(commerceItemId + constants.QUANTITY_VALIDATION_FAILED);
          }

          if ((commerceItemsReq[itemIdx].childItems && !items[index].childItems)
            || (!commerceItemsReq[itemIdx].childItems && items[index].childItems)
            || (commerceItemsReq[itemIdx].childItems && items[index].childItems && commerceItemsReq[itemIdx].childItems.length != items[index].childItems.length)) {
            errors.push(commerceItemId + constants.NUMBER_OF_CHILDITEMS_VALIDATION);
          }
          errors.push.apply(errors, this.validateCommerceItemProperties(commerceItemsReq[itemIdx], items[index]));
          if (errors.length == 0) {
            items[index].validated = true;
            if (commerceItemsReq[itemIdx].childItems && items[index].childItems) {
              errors.push.apply(errors, this.validateChildItems(commerceItemsReq[itemIdx].childItems, items[index].childItems));
            }
            break;
          }
        }
      }
      // If the item validation had no issues, error array will be empty. Otherwise, errors related to last item from a set of items belonging to same catRefId,
      // will be present.
      if (errors && errors.length > 0) {
        return errors;
      }
    }
  }

  /**
  * This method validates the commerecItem level properties
  * @param {object} commerceItemReq - required. The request from the OCC
  * @param {object} commerceItemRes - required. The response received from the CPQ
  **/
  validateSingleCommerceItem(commerceItemReq, commerceItemRes) {
    let errorTracker = [];
    let commerceItemId = commerceItemReq.id || commerceItemReq.commerceItemId;
    if ((commerceItemReq.catRefId && !commerceItemRes.catRefId)
      || (!commerceItemReq.catRefId && commerceItemRes.catRefId)
      || (commerceItemReq.catRefId.toLowerCase() != commerceItemRes.catRefId.toLowerCase())) {
      errorTracker.push(commerceItemId + constants.CATREFID_VALIDATION);
    }

    if ((commerceItemReq.childItems && !commerceItemRes.childItems)
      || (!commerceItemReq.childItems && commerceItemRes.childItems)
      || (commerceItemReq.childItems.length != commerceItemRes.childItems.length)) {
      errorTracker.push(commerceItemId + constants.NUMBER_OF_CHILDITEMS_VALIDATION);
    }

    errorTracker.push.apply(errorTracker, this.validateCommerceItemProperties(commerceItemReq, commerceItemRes));
    if (errorTracker.length == 0 && commerceItemReq.childItems && commerceItemRes.childItems) {
      errorTracker.push.apply(errorTracker, this.validateChildItems(commerceItemReq.childItems, commerceItemRes.childItems));
    }
    return errorTracker;
  }

  /**
  * This is a utility function to create a config map
  * which stores the configuratorId as the key and 
  * commerceItem details as the value
  * @param {object} commerceItemRes
  **/
  createConfiguratorIdToItemObjectMap(commerceItemsRes) {
    let configIdMap = {};
    for (let itemIdx in commerceItemsRes) {
      configIdMap[commerceItemsRes[itemIdx].configuratorId] = commerceItemsRes[itemIdx];
    }
    return configIdMap;
  }

  /**
  * This method validates the commerceItemReq with the commerceItemRes
  * and sets the statusCode at the item level
  * @param {object} commerceItemReq - required. The request from the OCC
  * @param {object} commerceItemRes - required. The response received from the CPQ
  **/
  validateCommerceItems(commerceItemsReq, commerceItemsRes) {

    let configIdMap = this.createConfiguratorIdToItemObjectMap(commerceItemsRes);
    let self = this;
    let validatedData = [];
    for (let itemIdx in commerceItemsReq) {
      let commerceItemId = commerceItemsReq[itemIdx].id || commerceItemsReq[itemIdx].commerceItemId || commerceItemsReq[itemIdx].commerceId;
      let matchingItemInResponse = configIdMap[commerceItemsReq[itemIdx].configuratorId];
      let errorTracker = [];
      if (!matchingItemInResponse) {
        matchingItemInResponse = {
          "catRefId": commerceItemsReq[itemIdx].catRefId,
          "commerceItemId": commerceItemId
        }
      }
      else if (matchingItemInResponse.statusCode == constants.ITEM_FAILURE) {
        matchingItemInResponse.commerceItemId = commerceItemId;
        matchingItemInResponse.catRefId = commerceItemsReq[itemIdx].catRefId;
        delete matchingItemInResponse.configuratorId;
        validatedData.push(matchingItemInResponse);
        continue;
      }
      else {
        matchingItemInResponse.commerceItemId = commerceItemId;
        errorTracker = self.validateSingleCommerceItem(commerceItemsReq[itemIdx], matchingItemInResponse);
      }
      if (errorTracker.length != 0) {
        matchingItemInResponse.statusCode = constants.ITEM_FAILURE;
      }
      else {
        matchingItemInResponse.statusCode = constants.ITEM_SUCCESS;
      }
      matchingItemInResponse.messages = errorTracker;
      validatedData.push(matchingItemInResponse);
    }
    return validatedData;
  }
}

module.exports.ValidationOccToCPQ = ValidationOccToCPQ;