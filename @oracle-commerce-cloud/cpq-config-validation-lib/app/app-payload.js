// Copyright © 1994, 2019, Oracle and/or its affiliates. All rights reserved.
'use strict';

module.exports = (requestBody, originalUrl) => {

  let items;

  if (originalUrl.includes("ccstorex")) {
    items = requestBody.order.shoppingCart.items;
  } else if (originalUrl.includes("ccagentx")) {
    items = requestBody.shoppingCart.items;
  }
  return items;
};