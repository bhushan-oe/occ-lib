// Copyright © 1994, 2019, Oracle and/or its affiliates. All rights reserved.

'use strict';

const logger = require('@oracle-commerce-cloud/logging-wrapper').logger;
const constants = require("./constants");
const payload = require("./app-payload");
const ExternalValidationUtils = require("./external-validation-utils").ExternalValidationUtils;
const ValidationOccToCPQ = require("./../validations/validations-occ-to-cpq").ValidationOccToCPQ;
/**
 * Class for validating the data from OCC with CPQ
 *
 */
class ExternalValidationService {

  /**
   * Creates ExternalValidationService Instance.
   */
  constructor(config) {
    if (!config) {
      throw new Error('No configuration passed.');
    }
    this.validationUtils = new ValidationOccToCPQ(),
      this.utils = new ExternalValidationUtils(),
      this.useHTTPS = config.USE_HTTPS,
      this.hostname = config.OIC_HOSTNAME,
      this.port = config.OIC_PORT,
      this.timeout = config.timeout,
      this.username = process.env[config.username_env_var],
      this.password = process.env[config.password_env_var],
      this.path_getConfigBOM = config.GET_CONFIGBOM_URI,
      this.path_getConfig = config.GET_CONFIG_URI

  }
  // Public methods

  /**
   * This function processes the incoming request body, segregate the data
   * into arrays for getConfig or getConfigBom calls
   * @param  {} requestBody request body coming from the webhook
   * @param  {} httpResponse response object that would be created after processing the data
   * @param  {} callback function that sends back the response to OCC
   */
  async filteringOrderItems(requestBody, originalUrl, httpResponse, callback) {

    // var is used as this must be available inside the if condition.
    var response = {};
    var self = this;
    try {
      var configurationArray = [];
      var configurationBomApiRecords = [];
      var configBomResponses = [];
      var items = payload(requestBody, originalUrl);
      items.forEach(function (item) {
        if (!item.asset && !item.configuratorId) {
          //regular products requiring external price validation, won't make CPQ calls
        }
        else if ((!item.asset && item.configuratorId) || (item.asset && (item.actionCode !== constants.TERMINATE && item.actionCode !== constants.SUSPEND))) {
          //regular configurable product and assets with actionCode other than Terminate or suspend, add in the getConfigurations array
          configurationArray.push(item.configuratorId);
        }
        else {
          //get configBOM calls for each of these items
          configurationBomApiRecords.push(item.configuratorId);
        }
      });
      if (configurationArray.length == 0 && configurationBomApiRecords.length == 0) {
        //regular products requiring external price validation, won't make CPQ calls
        items.forEach(function (item) {
          item.statusCode = constants.ITEM_SUCCESS;
          item.messages = []
        });
        response = this.buildResponse(items);
      }
      else {
        if (configurationArray.length > 0) {
          //make the getConfigurations call
          let configRequest = this.utils.getHttpRequestConfigurations(this, this.path_getConfig);
          let configurationPayload = {
            "locale": requestBody.profile.locale,
            "currencyCode": requestBody.currencyCode,
            "operation": "externalPricing",
            "orderId": requestBody.order.orderId,
            "configuratorIds": configurationArray
          }
          let params = {
            "httpsRequestOptions": configRequest,
            "requestPayload": configurationPayload
          };
          var configResponse = this.utils.callIntegrationAPI(params);
        }
        if (configurationBomApiRecords.length > 0) {
          //make the getConfigBOM call
          let configRequest = this.utils.getHttpRequestConfigurations(this, this.path_getConfigBOM);
          configBomResponses = configurationBomApiRecords.map(async (configuratorId) => {
            let configurationPayload = {
              "configuratorId": configuratorId
            }
            let params = {
              "httpsRequestOptions": configRequest,
              "requestPayload": configurationPayload
            };
            let result = this.utils.callIntegrationAPI(params);
            return result;
          });
        }
        if (configResponse) {
          //merging the promises into one array to be resolved later
          configBomResponses.push(configResponse);
        }
        let responseAll = await Promise.all(configBomResponses);
        logger.info("Raw data from CPQ: " + responseAll);
        let hierarchicalResponseArray = this.utils.getHierarchicalStructure(responseAll);
        logger.info("Hierarchical data from CPQ: " + JSON.stringify(hierarchicalResponseArray));
        //validating the OCC data against the external response data
        let validatedData = this.validationUtils.validateCommerceItems(items, hierarchicalResponseArray);
        response = this.buildResponse(validatedData);
      }
      callback(response, requestBody, httpResponse);

    } catch (error) {
      logger.info("ICS endpoint error Received is ", error);
      callback(error, requestBody, httpResponse);
      return;
    }
  }

  /**
  * Builds the response for the SSE and send it back.
  *
  * @param {object} validatedData - the data returned after the validation
  * @returns {object} res response payload for the SSE
  **/
  buildResponse(validatedData) {
    let res = {};
    res.externalPrices = [];
    validatedData.forEach(function (item) {
      let temp = {
        "statusCode": item.statusCode,
        "catRefId": item.catRefId,
        "commerceItemId": item.commerceItemId,
        "messages": item.messages
      }
      res.externalPrices.push(temp);
    });
    let validAtOrder = validatedData.some(function (item) {
      return item.statusCode != constants.ITEM_SUCCESS;
    });
    if (validAtOrder) {
      //one of the sub item has an error
      res.responseCode = constants.ORDER_FAILURE;
    }
    else {
      //all the item are validated
      res.responseCode = constants.ORDER_SUCCESS;
    }
    return res;
  }
}

module.exports.ExternalValidationService = ExternalValidationService;