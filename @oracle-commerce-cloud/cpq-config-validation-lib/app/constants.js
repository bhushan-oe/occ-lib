// Copyright © 1994, 2019, Oracle and/or its affiliates. All rights reserved.

'use strict';

module.exports = {
  // HTTP methods and erros
  HTTP: "http:",
  HTTPS: "https:",
  HTTP_METHOD_POST: "POST",
  HTTP_METHOD_GET: "GET",
  HTTP_INTERNAL_SERVER_ERROR: "500",
  HTTP_UNAUTHORIZED: "401",
  HTTP_SUCCESS: "200",
  HTTP_BAD_REQUEST: "400",

  //action codes
  TERMINATE : "TERMINATE",
  SUSPEND : "SUSPEND",

// Content types
  CONTENT_TYPE: "Content-Type",
  APPLICATION_JSON: "application/json",

  ORDER_SUCCESS : 5001,
  ORDER_FAILURE : 5002,
  ITEM_SUCCESS : 5104,
  ITEM_FAILURE : 5103,

  EXTERNAL_PRICE_QUANTITY_RESPONSE: -1,
  EXTERNAL_PRICE_VALIDATION_FAILED: ": externalPrice validation failed",
  EXTERNAL_PRICE_QUANTITY_FAILED: ": externalPriceQuantity validation failed",
  EXTERNAL_RECURRING_CHARGE_PRICE_VALIDATION_FAILED: ": externalRecurringCharge validation failed",
  EXTERNAL_RECURRING_CHARGE_FREQUENCY_VALIDATION_FAILED: ": externalRecurringChargeFrequency validation failed",
  EXTERNAL_RECURRING_CHARGE_DURATION_VALIDATION_FAILED: ": externalRecurringChargeDuration validation failed",
  ASSET_KEY_VALIDATION_FAILED: ": assetKey validation failed",
  ACTION_CODE_VALIDATION_FAILED: ": actionCode validation failed",
  ACTIVATION_DATE_VALIDATION_FAILED: ": activationDate validation failed",
  DEACTIVATION_DATE_VALIDATION_FAILED: ": deactivationDate validation failed",
  QUANTITY_VALIDATION_FAILED: ": quantity validation failed",
  NUMBER_OF_CHILDITEMS_VALIDATION: ": number of childItems received in response is different form what has been sent in request.",
  CATREFID_VALIDATION: ": catRefId validation failed",
  ERROR_CALLING_OIC: "Error when calling OIC endpoint: ",
  INVOKING_THE_OIC_API: "Invoking the OIC API",

};