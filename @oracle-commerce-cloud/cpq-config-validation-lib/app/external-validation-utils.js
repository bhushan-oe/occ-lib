// Copyright © 1994, 2019, Oracle and/or its affiliates. All rights reserved.

const constants = require('./constants');
const logger = require("@oracle-commerce-cloud/logging-wrapper").logger;
const http = require('http');
const https = require('https');
/**
 *
 * This class helps make the 
 * OIC calls for getConfig and getConfigBOM
 * Also, transforms the results from OIC
 */

class ExternalValidationUtils {

  /**
 * Invokes the OIC endpoints (getConfiguration & getConfigBOM)
 * 
 * @param {Object} params - required. The param object contain following information
 * httpsRequestOptions - This contains the object returned by the getHttpRequestConfigurations 
 *                       method.
 * @return {object} The response js object returned by OIC.
 */
  async callIntegrationAPI(params) {
    return new Promise(function (resolve, reject) {
      let response = "";
      let payload = "";
      let requestObject = (params.httpsRequestOptions.protocol === constants.HTTP) ? http : https;
      const httpsRequestOptions = {
        hostname: params.httpsRequestOptions.hostname,
        path: params.httpsRequestOptions.path,
        port: params.httpsRequestOptions.port,
        method: constants.HTTP_METHOD_POST,
        headers: params.httpsRequestOptions.headers
      };
      let request = requestObject.request(httpsRequestOptions, function (res) {
        //collect the response data
        res.on("data", function (chunk) {
          response += chunk;
        });
        res.on("end", function () {
          logger.info('Response received from ' + httpsRequestOptions.path);
          if (res.statusCode >= 200 && res.statusCode <= 299) {
            payload = response;
            resolve(payload);
          } else {
            reject('Transaction Failed: Response code ' + res.statusCode + ' : ' + res.statusMessage + ' received from OIC');
          }
        });
        res.on("error", function () {
          reject(constants.ERROR_CALLING_OIC + error.message);
        });
      });
      request.on("error", function (error) {
        reject(constants.ERROR_CALLING_OIC + error.message);
      });
      request.write(JSON.stringify(params.requestPayload));
      logger.info(constants.INVOKING_THE_OIC_API);
      request.end();
    });
  }

  /**
  * Get the http request configurations for creating OIC http request.
  *
  * @param {object} Configurations - required. config for the OIC.
  * @returns {object} http configurations
  **/
  getHttpRequestConfigurations(config, path) {
    const basicAuthHeader = "Basic " + Buffer.from(config.username + ':' + config.password).toString("base64");//construct url object from string
    const httpsRequestOptions = {
      hostname: config.hostname,
      port: config.port,
      path: path,
      protocol: config.useHTTPS ? constants.HTTPS : constants.HTTP,
      method: constants.HTTP_METHOD_POST,
      headers: {
        "Content-Type": "application/json",
        "Authorization": basicAuthHeader
      }
    }
    return httpsRequestOptions;
  }

  /**
  * transformation of the flat structure from OIC to the hierarchical structure in OCC
  *
  * @param {object} response - response from OIC
  * @returns {object} hierarchicalStructure configurations
  **/
  getHierarchicalStructure(response) {
    let self = this;
    let hierarchicalStructure = [];
    let configItem;
    response.forEach(function (item) {
      if (typeof item == "string") {
        configItem = JSON.parse(item);
      }
      else {
        configItem = item;
      }
      if (configItem.hasOwnProperty('configuratorItem')) {
        hierarchicalStructure.push(self.flatStructureToHierarchy(configItem.configuratorItem));
      }
      else if (configItem.hasOwnProperty('configuratorItems')) {
        configItem.configuratorItems.forEach(function (configuratorItem) {
          if (configuratorItem.statusCode == constants.ITEM_FAILURE) {
            //pushing the failed case as it is to the array
            hierarchicalStructure.push(configuratorItem);
          }
          else {
            //generating a hierarchical structure for success
            hierarchicalStructure.push(self.flatStructureToHierarchy(configuratorItem));
          }
        });
      }
    });
    return hierarchicalStructure;
  }

  /**
  * Utility function that constructs the hierarchy
  *
  * @param {object} configuratorItem - single commerceItem to be converted to hierarchy
  * @returns {object} configuratorItem hierachichal structure
  **/
  flatStructureToHierarchy(configuratorItem) {
    var parentToChildrenMap = {};
    var childItems = configuratorItem.childItems;

    var rootItemId = "_rootItem";
    if (configuratorItem.id) {
      rootItemId = configuratorItem.id;
    }

    for (var childItemIdx in childItems) {
      var childItem = childItems[childItemIdx];
      var parentId = childItem.parentId;
      if (!parentId) {
        parentId = rootItemId;
      }
      if (!parentToChildrenMap[parentId]) {
        parentToChildrenMap[parentId] = [];
      }
      parentToChildrenMap[parentId].push(childItem);
    }

    //Should contain only one item
    this.constructChildItemsStucture(configuratorItem, parentToChildrenMap);
    return configuratorItem;
  }

  /**
  * Recursive function to create the parent-child relationship
  *
  * @param {object} commerceItem - single commerceItem to be converted to hierarchy
  * @param {object} parentToChildrenMap - single commerceItem to be converted to hierarchy
  **/
  constructChildItemsStucture(commerceItem, parentToChildrenMap) {
    var childItems = parentToChildrenMap[commerceItem.id];
    if (childItems) {
      commerceItem.childItems = [];
      for (var item in childItems) {
        commerceItem.childItems.push(childItems[item]);
        this.constructChildItemsStucture(childItems[item], parentToChildrenMap);
      }
    }
  }
}

module.exports.ExternalValidationUtils = ExternalValidationUtils;