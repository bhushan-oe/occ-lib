# webhook-signature-validator
The library provides utility methods to validate webhook signature from server side extensions.

## Introduction
This is a library designed to be used with server side extension. It provides utility methods to validate webhook signature from server side extensions.


**getWebhookSecretKey** - Invokes the admin function webhook endpoint based on the URL provided in params.
**validateWebhookPayloadSignature** - Validte webhook signature.

## Prerequisites
The prerequisites required are
* Oracle Commerce Cloud (Minimum version: 19B)

## Install
The library should be copied to the node_modules directory of the server side extension.

## Usage
```javascript
const WebhookSignatureValidator = require("@oracle-commerce-cloud/webhook-signature-validator").WebhookSignatureValidator;
let webhookSignatureValidator = new WebhookSignatureValidator();

webhookSignatureValidator.validateWebhookPayloadSignature(param);

```
