// Copyright © 1994, 2019, Oracle and/or its affiliates. All rights reserved.
module.exports = {
  APPLICATION_FORM_URLENCODED:"application/x-www-form-urlencoded",
  HTTP_METHOD_GET:"GET",
  APPLICATION_JSON:"application/json",
  SITES_ENDPOINT:"/ccadmin/v1/sites",
  SITE_SETTINGS_ENDPOINT:"/ccadmin/v1/sitesettings/",
  BASE64:"base64",
  INVALID_SIGNATURE:"Access denied, Webhook signature is invalid."

};