// Copyright © 1994, 2019, Oracle and/or its affiliates. All rights reserved.
const constants = require('./constants');
const config = require("../config/config.json");
const crypto = require('crypto');
const OccRestUtils = require("@oracle-commerce-cloud/occ-rest-utils").OccRestUtils;
const logger = require("@oracle-commerce-cloud/logging-wrapper").logger;

/**
 *  Provides methods to get webhook secret key and
 *  handles the validation of webhook signature
 */
class WebhookSignatureValidator {

    constructor(){
        this.occutil = new OccRestUtils();
    }
  
  /**
   * Validates the webhook signature, which is received in the HTTP request.
   *
   * @param {object} httpRequest-required, The HTTP request.
   * @param {object} webhookConfig-required, This object contains webhook configurations
   * like webhook endpoint name.
   *
   * @throw  Error. If the secret key. raw body or signature of the webhook do not match or invalid.
   * It throws an error Access denied, Secret key, raw body or signature is missing or
   * Webhook signature is invalid.
   **/
 async validateWebhookPayloadSignature(httpRequest, webhookConfig) {
    logger.debug("validate webhook signature request received");
    await this.getWebhookSecretKey(webhookConfig);
    let secretKey = WebhookSignatureValidator.webhookSecretKey;
    let rawBody = httpRequest.rawBody;
    let webhookSignature = httpRequest.headers[config.webhookSignature];
    if (secretKey == null || rawBody === undefined || webhookSignature === undefined) {
      logger.warn("Access denied, Secret key, rawBody or signature is missing.");
      throw new Error(constants.INVALID_SIGNATURE);
    }
    let decodedSecretKey = Buffer.from(secretKey, constants.BASE64);
    let calculatedSignature = crypto.createHmac(config.webhookHMACHash, decodedSecretKey)
      .update(rawBody)
      .digest(constants.BASE64);
    if (calculatedSignature != webhookSignature) {
      logger.warn("Access denied, Webhook signature is invalid.");
      throw new Error(constants.INVALID_SIGNATURE);
    }
  }


  /** 
   * Fetches the webhook secret key to validate the incoming webhook request and
   * It stores secret key in webhookSecretKey property of WebhookSignatureValidator class.
   *
   * @param {object} webhookConfig-required, This object contains webhook configurations
   * like webhook endpoint name.
   */
 async getWebhookSecretKey(webhookConfig) {
  logger.debug("load webhook secret key request received.");
  this.accessToken = await this.occutil.loginAdmin();
    let params = {
      "contentType":constants.APPLICATION_FORM_URLENCODED,
      "URLpath":webhookConfig.webhookEndpoint+"?includeSecretKey=true",
      "httpMethod":constants.HTTP_METHOD_GET,
      "oAuthToken":this.accessToken
    };

    let webhookSettingsResponse = await this.occutil.callAdminApi(params);
    WebhookSignatureValidator.webhookSecretKey = webhookSettingsResponse.secretKey;
 } 
 

}

module.exports.WebhookSignatureValidator = WebhookSignatureValidator;