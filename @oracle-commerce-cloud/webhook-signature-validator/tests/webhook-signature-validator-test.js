// Copyright © 1994, 2019, Oracle and/or its affiliates. All rights reserved.
const WebhookSignatureValidator = require("../lib/webhook-signature-validator").WebhookSignatureValidator;
const constants = require('../lib/constants');
const config = require("../config/config.json");
const OccRestUtils = require("@oracle-commerce-cloud/occ-rest-utils").OccRestUtils;
const crypto = require('crypto');


describe('WebhookSignatureValidator flows ', () => {


  it('Valid access token', (done) => {
    let webhookConfig = {
      "webhookEndpoint": "/ccadmin/v1/functionWebhooks/production-cancelOrderUpdate"
    }
    let webhookSignatureValidator = new WebhookSignatureValidator();
    spyOn(OccRestUtils.prototype, "loginAdmin").and.returnValue("accessToken123");
    spyOn(OccRestUtils.prototype, "callAdminApi").and.returnValue({
      "secretKey": "secretKey123"
    });
    webhookSignatureValidator.getWebhookSecretKey(webhookConfig).then(function (result) {
      expect(webhookSignatureValidator.accessToken).toEqual("accessToken123");
    }).catch((err) => {
      fail(err);
    }).then(() => done(), done);
  });

  it('Invalid access token', (done) => {
    let webhookConfig = {
      "webhookEndpoint": "/ccadmin/v1/functionWebhooks/production-cancelOrderUpdate"
    }
    let webhookSignatureValidator = new WebhookSignatureValidator();
    spyOn(OccRestUtils.prototype, "loginAdmin").and.returnValue(null);
    spyOn(OccRestUtils.prototype, "callAdminApi").and.returnValue({
      "secretKey": "secretKey123"
    });
    webhookSignatureValidator.getWebhookSecretKey(webhookConfig).then(function (result) {
      expect(webhookSignatureValidator.accessToken).not.toEqual("accessToken123");
    }).catch((err) => {
      fail(err);
    }).then(() => done(), done);
  });

  it('Valid webhook secret key', (done) => {
    let webhookConfig = {
      "webhookEndpoint": "/ccadmin/v1/functionWebhooks/production-cancelOrderUpdate"
    }
    let webhookSignatureValidator = new WebhookSignatureValidator();
    spyOn(OccRestUtils.prototype, "loginAdmin").and.returnValue("accessToken123");
    spyOn(OccRestUtils.prototype, "callAdminApi").and.returnValue({
      "secretKey": "secretKey123"
    });
    webhookSignatureValidator.getWebhookSecretKey(webhookConfig).then(function (result) {
      expect(WebhookSignatureValidator.webhookSecretKey).toEqual("secretKey123");
    }).catch((err) => {
      fail(err);
    }).then(() => done(), done);
  });

  it('Invalid webhook secret key', (done) => {
    let webhookConfig = {
      "webhookEndpoint": "/ccadmin/v1/functionWebhooks/production-cancelOrderUpdate"
    }
    let webhookSignatureValidator = new WebhookSignatureValidator();
    spyOn(OccRestUtils.prototype, "loginAdmin").and.returnValue("accessToken123");
    spyOn(OccRestUtils.prototype, "callAdminApi").and.returnValue({
      "secretKey": null
    });
    webhookSignatureValidator.getWebhookSecretKey(webhookConfig).then(function (result) {
      expect(WebhookSignatureValidator.webhookSecretKey).not.toEqual("secretKey123");
    }).catch((err) => {
      fail(err);
    }).then(() => done(), done);
  });

  it('Validate webhook secret key  ', (done) => {
    let encodedKey = Buffer.from("secretKey123").toString(constants.BASE64);
    let decodedSecretKey = Buffer.from(encodedKey, constants.BASE64);
    let calculatedSignature = crypto.createHmac(config.webhookHMACHash, decodedSecretKey)
      .update("rawBody123")
      .digest(constants.BASE64);
    let httpRequest = {
      "rawBody": "rawBody123",
      "headers": {
        "x-oracle-cc-webhook-signature": calculatedSignature
      }
    };
    let webhookConfig = {
      "webhookEndpoint": "/ccadmin/v1/functionWebhooks/production-cancelOrderUpdate"
    }
    let webhookSignatureValidator = new WebhookSignatureValidator();
    spyOn(OccRestUtils.prototype, "loginAdmin").and.returnValue("accessToken123");
    spyOn(OccRestUtils.prototype, "callAdminApi").and.returnValue({
      "secretKey": encodedKey
    });
    webhookSignatureValidator.validateWebhookPayloadSignature(httpRequest, webhookConfig).then(function (result) {
      expect(WebhookSignatureValidator.webhookSecretKey).toEqual(encodedKey);
    }).catch((err) => {
      fail(err);
    }).then(() => done(), done);
  });


  it('Invalidate webhook secret key  ', (done) => {
    let encodedKey = Buffer.from("secretKey123").toString(constants.BASE64);
    let encodedKey2 = Buffer.from("secretKey456").toString(constants.BASE64);
    let decodedSecretKey = Buffer.from(encodedKey, constants.BASE64);
    let calculatedSignature = crypto.createHmac(config.webhookHMACHash, decodedSecretKey)
      .update("rawBody123")
      .digest(constants.BASE64);
    let httpRequest = {
      "rawBody": "rawBody123",
      "headers": {
        "x-oracle-cc-webhook-signature": calculatedSignature
      }
    };
    let webhookConfig = {
      "webhookEndpoint": "/ccadmin/v1/functionWebhooks/production-cancelOrderUpdate"
    }
    let webhookSignatureValidator = new WebhookSignatureValidator();
    spyOn(OccRestUtils.prototype, "loginAdmin").and.returnValue("accessToken123");
    spyOn(OccRestUtils.prototype, "callAdminApi").and.returnValue({
      "secretKey": encodedKey
    });
    webhookSignatureValidator.validateWebhookPayloadSignature(httpRequest, webhookConfig).then(function (result) {
      expect(WebhookSignatureValidator.webhookSecretKey).not.toEqual(encodedKey2);
    }).catch((err) => {
      fail(err);
    }).then(() => done(), done);
  });


  it('Exception for invalid webhook secret key  ', () => {
    let httpRequest = {
      "rawBody": "rawBody123",
      "headers": {
        "x-oracle-cc-webhook-signature": null
      }
    };
    let webhookConfig = {
      "webhookEndpoint": "/ccadmin/v1/functionWebhooks/production-cancelOrderUpdate"
    }
    let webhookSignatureValidator = new WebhookSignatureValidator();
    spyOn(OccRestUtils.prototype, "loginAdmin").and.returnValue("accessToken123");
    spyOn(OccRestUtils.prototype, "callAdminApi").and.returnValue({
      "secretKey": null
    });
   
      expect(()=>{webhookSignatureValidator.validateWebhookPayloadSignature(httpRequest, webhookConfig)
      .then( (result)=>{})
      .catch( (err)=>{
        expect(err).toEqual("Access denied, Secret key, rawBody or signature is missing.");
      })});
    
   
  });


  it('Validate raw body  ', (done) => {
    let encodedKey = Buffer.from("secretKey123").toString(constants.BASE64);
    let decodedSecretKey = Buffer.from(encodedKey, constants.BASE64);
    let calculatedSignature = crypto.createHmac(config.webhookHMACHash, decodedSecretKey)
      .update("rawBody123")
      .digest(constants.BASE64);
    let httpRequest = {
      "rawBody": "rawBody123",
      "headers": {
        "x-oracle-cc-webhook-signature": calculatedSignature
      }
    };
    let webhookConfig = {
      "webhookEndpoint": "/ccadmin/v1/functionWebhooks/production-cancelOrderUpdate"
    }
    let webhookSignatureValidator = new WebhookSignatureValidator();
    spyOn(OccRestUtils.prototype, "loginAdmin").and.returnValue("accessToken123");
    spyOn(OccRestUtils.prototype, "callAdminApi").and.returnValue({
      "secretKey": encodedKey
    });
    webhookSignatureValidator.validateWebhookPayloadSignature(httpRequest, webhookConfig).then(function (result) {
      expect(httpRequest.rawBody).toEqual("rawBody123");
    }).catch((err) => {
      fail(err);
    }).then(() => done(), done);


  });

  it('Invalidate raw body  ', (done) => {
    let encodedKey = Buffer.from("secretKey123").toString(constants.BASE64);
    let decodedSecretKey = Buffer.from(encodedKey, constants.BASE64);
    let calculatedSignature = crypto.createHmac(config.webhookHMACHash, decodedSecretKey)
      .update("rawBody123")
      .digest(constants.BASE64);
    let httpRequest = {
      "rawBody": "rawBody123",
      "headers": {
        "x-oracle-cc-webhook-signature": calculatedSignature
      }
    };
    let webhookConfig = {
      "webhookEndpoint": "/ccadmin/v1/functionWebhooks/production-cancelOrderUpdate"
    }
    let webhookSignatureValidator = new WebhookSignatureValidator();
    spyOn(OccRestUtils.prototype, "loginAdmin").and.returnValue("accessToken123");
    spyOn(OccRestUtils.prototype, "callAdminApi").and.returnValue({
      "secretKey": encodedKey
    });
    webhookSignatureValidator.validateWebhookPayloadSignature(httpRequest, webhookConfig).then(function (result) {
      expect(httpRequest.rawBody).not.toEqual("rawBody145");
    }).catch((err) => {
      fail(err);
    }).then(() => done(), done);
  });



  it('Exception for invalid raw body  ', () => {
    let encodedKey = Buffer.from("secretKey123").toString(constants.BASE64);
    let decodedSecretKey = Buffer.from(encodedKey, constants.BASE64);
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000;
    let calculatedSignature = crypto.createHmac(config.webhookHMACHash, decodedSecretKey)
      .update("rawBody123")
      .digest(constants.BASE64);
    let httpRequest = {
      "rawBody": null,
      "headers": {
        "x-oracle-cc-webhook-signature": calculatedSignature
      }
    };
    let webhookConfig = {
      "webhookEndpoint": "/ccadmin/v1/functionWebhooks/production-cancelOrderUpdate"
    }
    let webhookSignatureValidator = new WebhookSignatureValidator();
    spyOn(OccRestUtils.prototype, "loginAdmin").and.returnValue("accessToken123");
    spyOn(OccRestUtils.prototype, "callAdminApi").and.returnValue({
      "secretKey": null
    });
    expect(()=>{webhookSignatureValidator.validateWebhookPayloadSignature(httpRequest, webhookConfig)
      .then( (result)=>{})
      .catch( (err)=>{
        expect(err).toEqual("Access denied, Secret key, rawBody or signature is missing.");
      })});
    
  });



  it('Validate webhook signature  ', (done) => {
    let encodedKey = Buffer.from("secretKey123").toString(constants.BASE64);
    let decodedSecretKey = Buffer.from(encodedKey, constants.BASE64);
    let calculatedSignature = crypto.createHmac(config.webhookHMACHash, decodedSecretKey)
      .update("rawBody123")
      .digest(constants.BASE64);
    let httpRequest = {
      "rawBody": "rawBody123",
      "headers": {
        "x-oracle-cc-webhook-signature": calculatedSignature
      }
    };
    let webhookConfig = {
      "webhookEndpoint": "/ccadmin/v1/functionWebhooks/production-cancelOrderUpdate"
    }
    let webhookSignatureValidator = new WebhookSignatureValidator();
    spyOn(OccRestUtils.prototype, "loginAdmin").and.returnValue("accessToken123");
    spyOn(OccRestUtils.prototype, "callAdminApi").and.returnValue({
      "secretKey": encodedKey
    });
    webhookSignatureValidator.validateWebhookPayloadSignature(httpRequest, webhookConfig).then(function (result) {
      expect(httpRequest.headers['x-oracle-cc-webhook-signature']).toEqual(calculatedSignature);
    }).catch((err) => {
      fail(err);
    }).then(() => done(), done);
  });

  it('Invalidate webhook signature  ', (done) => {
    let encodedKey = Buffer.from("secretKey123").toString(constants.BASE64);
    let decodedSecretKey = Buffer.from(encodedKey, constants.BASE64);
    let calculatedSignature = crypto.createHmac(config.webhookHMACHash, decodedSecretKey)
      .update("rawBody123")
      .digest(constants.BASE64);
    let httpRequest = {
      "rawBody": "rawBody123",
      "headers": {
        "x-oracle-cc-webhook-signature": calculatedSignature
      }
    };
    let webhookConfig = {
      "webhookEndpoint": "/ccadmin/v1/functionWebhooks/production-cancelOrderUpdate"
    }
    let webhookSignatureValidator = new WebhookSignatureValidator();
    spyOn(OccRestUtils.prototype, "loginAdmin").and.returnValue("accessToken123");
    spyOn(OccRestUtils.prototype, "callAdminApi").and.returnValue({
      "secretKey": encodedKey
    });
    webhookSignatureValidator.validateWebhookPayloadSignature(httpRequest, webhookConfig).then(function (result) {
      expect(httpRequest.headers['x-oracle-cc-webhook-signature']).not.toEqual("fiisfiasfia");
    }).catch((err) => {
      fail(err);
    }).then(() => done(), done);


  });




  it('Exception for invalid webhook signature  ', () => {
    let encodedKey = Buffer.from("secretKey123").toString(constants.BASE64);
    let encodedKey2 = Buffer.from("secretKey456").toString(constants.BASE64);
    let decodedSecretKey = Buffer.from(encodedKey, constants.BASE64);
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000;
    let calculatedSignature = crypto.createHmac(config.webhookHMACHash, decodedSecretKey)
      .update("rawBody123")
      .digest(constants.BASE64);
    let httpRequest = {
      "rawBody": "rawBody123",
      "headers": {
        "x-oracle-cc-webhook-signature": calculatedSignature
      }
    };
    let webhookConfig = {
      "webhookEndpoint": "/ccadmin/v1/functionWebhooks/production-cancelOrderUpdate"
    }
    let webhookSignatureValidator = new WebhookSignatureValidator();
    spyOn(OccRestUtils.prototype, "loginAdmin").and.returnValue("accessToken123");
    spyOn(OccRestUtils.prototype, "callAdminApi").and.returnValue({
      "secretKey": encodedKey2
    });
    expect(()=>{webhookSignatureValidator.validateWebhookPayloadSignature(httpRequest, webhookConfig)
      .then( (result)=>{})
      .catch( (err)=>{
        expect(err).toEqual("Access denied, Secret key, rawBody or signature is missing.");
      })});
    
  });




  it('Exception for invalid x-oracle-cc-webhook-signature ', () => {
    let encodedKey = Buffer.from("secretKey123").toString(constants.BASE64);
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000;
    let httpRequest = {
      "rawBody": "rawBody123",
      "headers": {
        "x-oracle-cc-webhook-signature": null
      }
    };
    let webhookConfig = {
      "webhookEndpoint": "/ccadmin/v1/functionWebhooks/production-cancelOrderUpdate"
    }
    let webhookSignatureValidator = new WebhookSignatureValidator();
    spyOn(OccRestUtils.prototype, "loginAdmin").and.returnValue("accessToken123");
    spyOn(OccRestUtils.prototype, "callAdminApi").and.returnValue({
      "secretKey": encodedKey
    });
    expect(()=>{webhookSignatureValidator.validateWebhookPayloadSignature(httpRequest, webhookConfig)
      .then( (result)=>{})
      .catch( (err)=>{
        expect(err).toEqual("Access denied, Secret key, rawBody or signature is missing.");
      })});
    
  });

});