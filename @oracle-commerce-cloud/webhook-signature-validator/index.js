const WebhookSignatureValidator = require('./lib/webhook-signature-validator').WebhookSignatureValidator;

module.exports = {
  WebhookSignatureValidator
}
