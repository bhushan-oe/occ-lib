# translatable-error
> An extension to Error with additional fields.

## Introduction
This Error extension provides a resourceKey property and placeholders for use with an internationalization library.

## Prerequisites
The prerequisites required are
* Oracle Commerce Cloud

## Install
The library should be copied to the node_modules directory of the server side extension.

## Usage
```javascript
const TranslatableError = require('@oracle-commerce-cloud/translatable-error');
throw new TranslatableError('A contact is required before performing the operation.', 'contactRequired');
```
