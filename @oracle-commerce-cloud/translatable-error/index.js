'use strict';

/**
 * Error wrapper for translatable errors.
 *
 * @class TranslatableError
 * @extends {Error}
 */
class TranslatableError extends Error {

  /**
   * Creates an instance of TranslatableError.
   *
   * @param {string} message The error message.
   * @param {string} resourceKey A key in a resource bundle.
   * @param {string} status The HTTP status code of the error.
   * @param {Object} placeholders Placeholders replacements.
   * @memberof TranslatableError
   */
  constructor (message, resourceKey, status, placeholders) {
    super(message);
    this.resourceKey = resourceKey;
    this.status = status;
    this.placeholders = placeholders;
    Error.captureStackTrace(this, TranslatableError);
  }
}

module.exports = TranslatableError;
