'use strict';

const winston = require('winston');


try {
  module.exports.logger = require('../../../../../../lib/logging')();
  
} catch (e) {
  module.exports.logger = winston;
}
