# logging-wrapper
> The library which wraps the logging utility provided by the custom server for server side extensions.

## Introduction
This is a library designed to be used with server side extension. It is a wrapper to the logging utility provided by the custom server for server side extensions.

## Prerequisites
The prerequisites required are
* Oracle Commerce Cloud (Minimum version: 18C)

## Install
The library should be copied to the node_modules directory of the server side extension.

## Usage
```javascript
const logger = require("@oracle-commerce-cloud/logging-wrapper").logger;

logger.debug("Debug...");
logger.info("Info...");
```
