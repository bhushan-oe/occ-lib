# occ-rest-utils
> The library provides utility methods to call Oracle Commerce Cloud endpoints from server side extensions.

## Introduction
This is a library designed to be used with server side extension. It provides following utility methods to call Oracle Commerce Cloud endpoints from server side extensions.

**callApi** - Invokes the endpoint based on the URL provided in params
**callAdminApi** - Invokes the admin endpoint
**callStoreApi** - Invokes the store endpoint
**loginAdmin** - Provides the access token to invoke admin endpoint

## Prerequisites
The prerequisites required are
* Oracle Commerce Cloud (Minimum version: 18C)

## Install
The library should be copied to the node_modules directory of the server side extension.

## Usage
```javascript
const OccRestUtils = require("@oracle-commerce-cloud/occ-rest-utils").OccRestUtils;
occutil = new OccRestUtils();

occutil.loginAdmin();
occutil.callAdminApi(params);
```
