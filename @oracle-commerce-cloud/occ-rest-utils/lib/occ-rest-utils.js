'use strict';

const url = require('url');
const nconf = require('nconf');
const http = require('http');
const https = require('https');
const constants = require('./constants');
const querystring = require('querystring');
const logger = require('@oracle-commerce-cloud/logging-wrapper').logger;

/**
 *
 * The Class with utility methods for handling the OCCS endpoint calls
 *
 */
class OccRestUtils {

  /**
   * Handles the API call to OCCS endpoint.
   *
   * @param {object} params - required. The param object contain following information
   *  httpMethod - The HTTPS method like GET, POST or PUT
   *  contentType - The "Content-Type" for the endpoint. Ex: "application/json"
   *  hostURL - The URL object with OCCS host details
   *  URLpath - The URLpath string to the endpoint (after the hostname)
   *  qs - query parameters to the endpoint
   *  ignoreError - If true, any API error response will be ignored and error payload will be returned.
   *  requestPayload - The request JSON
   *
   * @return {object} The response JSON returned by the endpoint
   * @throws an error when the URL path is invalid or error is returned by endpoint
   */
  callApi(params) {
    return new Promise(function(fulfill, reject) {
      let apiResponse = '';
      //default to GET
      if (!params.httpMethod) {
        params.httpMethod = constants.HTTP_METHOD_GET;
      }
      if (!params.contentType) {
        params.contentType = constants.APPLICATION_JSON;
      }
      let httpRequestOptions = {
        hostname: params.hostURL.hostname,
        path: params.URLpath,
        port: params.hostURL.port,
        method: params.httpMethod,
        headers: {
          [constants.CONTENT_TYPE]: params.contentType,
          [constants.AUTHORIZATION]: [constants.BEARER] + params.oAuthToken
        }
      };
      if (params.headers) {
        let headers = Object.assign(httpRequestOptions.headers, params.headers);
        httpRequestOptions.headers = headers;
      }

      // Add query params
      if (params.qs) {
        httpRequestOptions.path = httpRequestOptions.path + '?' + querystring.stringify(params.qs);
      }

      logger.info('Invoking ' + params.URLpath +' endpoint');
      let requestObject;
      if (params.hostURL.protocol === 'http:') {
        requestObject = http;
        if (!httpRequestOptions.port) {
          httpRequestOptions.port = 80;
        }
      } else {
        requestObject = https;
        if (!httpRequestOptions.port) {
          httpRequestOptions.port = 443;
        }
      }

      let OCCRequest = requestObject.request(httpRequestOptions, function(res) {
        //collect the response data
        res.on('data', function(chunk) {
          apiResponse += chunk;
        });
        res.on('end', function() {
          try {
            if (res.statusCode === 204){
              fulfill();
            } else {
              apiResponse = JSON.parse(apiResponse);
              if (params.ignoreError || res.statusCode === 200) {
                fulfill(apiResponse);
              } else {
                logger.error('Error response returned by OCC:' + JSON.stringify(apiResponse));
                reject(apiResponse.message);
              }
            }
          } catch (error) {
            logger.error('Error parsing OCCS endpoint response: ' + error.message);
            reject(error.message);
          }
        });
        res.on('error', function(error) {
          logger.error('Error when calling OCCS endpoint: ' + error.message);
          reject(error.message);
        });
      });
      OCCRequest.on('error', function(error) {
        logger.error('Error when calling OCCS endpoint: ' + error.message);
        reject(error.message);
      });
      if (params.requestPayload) {
        OCCRequest.write(params.requestPayload);
      }
      OCCRequest.end();
    });
  }

  /**
   * Handles the API call to OCCS admin endpoint.
   *
   * @param {object} params - required. The param object contain following information
   *  httpMethod - The HTTPS method like GET, POST or PUT
   *  contentType - The "Content-Type" for the endpoint. Ex: "application/json"
   *  URLpath - The URLpath string to the endpoint
   *  qs - query parameters to the endpoint
   *  ignoreError - If true, any API error response will be ignored and error payload will be returned.
   *  requestPayload - The request JSON
   *
   * @return {object} The response JSON returned by the endpoint.
   * @throws an error when the URL path is invalid or error is returned by endpoint
   *
   */
  callAdminApi(params) {
    params.hostURL = url.parse(nconf.stores.runtime.store[[constants.ADMIN_URL]], true, true);
    return this.callApi(params);
  }

  /**
   * Handles the API call to OCCS store endpoint.
   *
   * @param {object} params - required. The param object contain following information
   *  httpMethod - The HTTPS method like GET, POST or PUT
   *  contentType - The "Content-Type" for the endpoint. Ex: "application/json"
   *  URLpath - The URLpath string to the endpoint
   *  qs - query parameters to the endpoint
   *  ignoreError - If true, any API error response will be ignored and error payload will be returned.
   *  requestPayload - The request JSON
   *
   * @return {object} The response JSON returned by the endpoint.
   * @throws an error when the URL path is invalid or error is returned by endpoint
   *
   */
  callStoreApi(params) {
    params.hostURL = url.parse(nconf.stores.runtime.store[[constants.STOREFRONT_URL]], true, true);
    return this.callApi(params);
  }

  /**
   * Invokes the login admin endpoint("/ccadmin/v1/login") and retuns the oAuth token
   *
   * @return {object} the oAuth token which can be used to invoke the authenticated admin endpoints
   *
   */
  async loginAdmin() {
    let loginResponse = await this.callAdminApi({
      oAuthToken: nconf.stores.runtime.store[[constants.CREDENTIALS]][
        [constants.TOKEN]
      ],
      URLpath: constants.LOGIN_ENDPOINT,
      httpMethod: constants.HTTP_METHOD_POST,
      contentType: constants.APPLICATION_FORM_URLENCODED,
      requestPayload: querystring.stringify({
        [constants.GRANT_TYPE]: constants.CLIENT_CREDENTIALS_GRANT_TYPE
      })
    });
    return loginResponse[[constants.ACCESS_TOKEN]];
  }
}

module.exports.OccRestUtils = OccRestUtils;
