'use strict';

module.exports = {
  // OCCS Endpoint URLs
  LOGIN_ENDPOINT: '/ccadmin/v1/login',

  // OCCS Endpoint Headers and constants
  CLIENT_CREDENTIALS_GRANT_TYPE: 'client_credentials',
  AUTHORIZATION: 'Authorization',
  BEARER: 'Bearer ',
  GRANT_TYPE: 'grant_type',
  ACCESS_TOKEN: 'access_token',
  SITE_ID: 'x-ccsite',

  // SSE constants
  ADMIN_URL: 'atg.server.admin.url',
  STOREFRONT_URL: 'atg.server.url',
  CREDENTIALS: 'atg.application.credentials',
  TOKEN: 'atg.application.token',

  // HTTP methods and erros
  HTTP_METHOD_POST: 'POST',
  HTTP_METHOD_GET: 'GET',

  // Content types
  CONTENT_TYPE: 'Content-Type',
  APPLICATION_FORM_URLENCODED: 'application/x-www-form-urlencoded',
  APPLICATION_JSON: 'application/json',
};
