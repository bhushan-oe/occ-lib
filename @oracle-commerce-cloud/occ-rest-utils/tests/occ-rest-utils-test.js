'use strict';

const nconf = require('nconf');
const OccRestUtils = require('../lib/occ-rest-utils').OccRestUtils;
let occutils = new OccRestUtils();

let params = {
  hostURL: {
    protocol: 'http:'
  },
  URLpath: '/test/url/path'
};

describe('OccRestUtils store and admin API flows: ', function() {
  beforeEach(function() {
    this.stores = nconf.stores;
    nconf.stores = {
      runtime: {
        store: {
          'atg.server.admin.url': 'http://example.com',
          'atg.server.url': 'http://example.com',
          'atg.application.token': 'token123'
        }
      }
    };
  });
  afterEach(function() {
    nconf.stores = this.stores;
  });
  it('Store API', function(done) {
    spyOn(OccRestUtils.prototype, 'callApi').and.returnValue(new Promise(function(resolve, reject) {
      try {
        resolve('test Data');
      } catch (e) {
        reject('Test Data');
      }
    }));
    occutils.callStoreApi(params).then(function(result) {
      expect(result).toEqual('test Data');
    }).catch((err) => {
      fail(err);
    }).then(() => done(), done);
  });

  it('Admin API', function(done) {
    spyOn(OccRestUtils.prototype, 'callApi').and.returnValue(new Promise(function(resolve, reject) {
      try {
        resolve('test Data');
      } catch (e) {
        reject('Test Data');
      }
    }));
    occutils.callAdminApi(params).then(function(result) {
      expect(result).toEqual('test Data');
    }).catch((err) => {
      fail(err);
    }).then(() => done(), done);
  });

});
